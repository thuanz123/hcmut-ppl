# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .MCParser import MCParser
else:
    from MCParser import MCParser

# This class defines a complete generic visitor for a parse tree produced by MCParser.

class MCVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by MCParser#program.
    def visitProgram(self, ctx:MCParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#manyDecl.
    def visitManyDecl(self, ctx:MCParser.ManyDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#decl.
    def visitDecl(self, ctx:MCParser.DeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#variableDecl.
    def visitVariableDecl(self, ctx:MCParser.VariableDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#primitiveType.
    def visitPrimitiveType(self, ctx:MCParser.PrimitiveTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#variable.
    def visitVariable(self, ctx:MCParser.VariableContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#functionDecl.
    def visitFunctionDecl(self, ctx:MCParser.FunctionDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#functionType.
    def visitFunctionType(self, ctx:MCParser.FunctionTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#arrayPointerType.
    def visitArrayPointerType(self, ctx:MCParser.ArrayPointerTypeContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#parameterList.
    def visitParameterList(self, ctx:MCParser.ParameterListContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#parameterDecl.
    def visitParameterDecl(self, ctx:MCParser.ParameterDeclContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#blockStatement.
    def visitBlockStatement(self, ctx:MCParser.BlockStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#blockBody.
    def visitBlockBody(self, ctx:MCParser.BlockBodyContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#statement.
    def visitStatement(self, ctx:MCParser.StatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#ifStatement.
    def visitIfStatement(self, ctx:MCParser.IfStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#doWhileStatement.
    def visitDoWhileStatement(self, ctx:MCParser.DoWhileStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#forStatement.
    def visitForStatement(self, ctx:MCParser.ForStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#breakStatement.
    def visitBreakStatement(self, ctx:MCParser.BreakStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#continueStatement.
    def visitContinueStatement(self, ctx:MCParser.ContinueStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#returnStatement.
    def visitReturnStatement(self, ctx:MCParser.ReturnStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expressionStatement.
    def visitExpressionStatement(self, ctx:MCParser.ExpressionStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression.
    def visitExpression(self, ctx:MCParser.ExpressionContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression1.
    def visitExpression1(self, ctx:MCParser.Expression1Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression2.
    def visitExpression2(self, ctx:MCParser.Expression2Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression3.
    def visitExpression3(self, ctx:MCParser.Expression3Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression4.
    def visitExpression4(self, ctx:MCParser.Expression4Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression5.
    def visitExpression5(self, ctx:MCParser.Expression5Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression6.
    def visitExpression6(self, ctx:MCParser.Expression6Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression7.
    def visitExpression7(self, ctx:MCParser.Expression7Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression8.
    def visitExpression8(self, ctx:MCParser.Expression8Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expression9.
    def visitExpression9(self, ctx:MCParser.Expression9Context):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#literal.
    def visitLiteral(self, ctx:MCParser.LiteralContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#funcall.
    def visitFuncall(self, ctx:MCParser.FuncallContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by MCParser#expressionList.
    def visitExpressionList(self, ctx:MCParser.ExpressionListContext):
        return self.visitChildren(ctx)



del MCParser