# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


from lexererr import *



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\63")
        buf.write("\u0160\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\3\2\6\2m\n\2\r\2\16\2n\3\3\3\3\3\3\5\3")
        buf.write("t\n\3\3\3\5\3w\n\3\3\3\3\3\3\3\5\3|\n\3\3\3\3\3\3\3\5")
        buf.write("\3\u0081\n\3\3\4\3\4\5\4\u0085\n\4\3\4\3\4\3\5\3\5\5\5")
        buf.write("\u008b\n\5\3\6\3\6\7\6\u008f\n\6\f\6\16\6\u0092\13\6\3")
        buf.write("\6\3\6\3\6\3\7\3\7\5\7\u0099\n\7\3\b\3\b\3\b\3\t\3\t\3")
        buf.write("\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13")
        buf.write("\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3")
        buf.write("\r\3\r\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20")
        buf.write("\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\22\3\22")
        buf.write("\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34")
        buf.write("\3\35\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3")
        buf.write(" \3!\3!\3!\3\"\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'")
        buf.write("\3(\3(\3)\3)\3*\3*\3+\3+\3,\3,\3-\3-\3.\3.\3/\3/\7/\u0128")
        buf.write("\n/\f/\16/\u012b\13/\3\60\6\60\u012e\n\60\r\60\16\60\u012f")
        buf.write("\3\60\3\60\3\61\3\61\3\61\3\61\7\61\u0138\n\61\f\61\16")
        buf.write("\61\u013b\13\61\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3\62")
        buf.write("\3\62\7\62\u0146\n\62\f\62\16\62\u0149\13\62\3\62\3\62")
        buf.write("\3\63\3\63\7\63\u014f\n\63\f\63\16\63\u0152\13\63\3\63")
        buf.write("\3\63\5\63\u0156\n\63\3\64\3\64\7\64\u015a\n\64\f\64\16")
        buf.write("\64\u015d\13\64\3\65\3\65\3\u0139\2\66\3\3\5\4\7\2\t\5")
        buf.write("\13\6\r\2\17\2\21\7\23\b\25\t\27\n\31\13\33\f\35\r\37")
        buf.write("\16!\17#\20%\21\'\22)\23+\24-\25/\26\61\27\63\30\65\31")
        buf.write("\67\329\33;\34=\35?\36A\37C E!G\"I#K$M%O&Q\'S(U)W*Y+[")
        buf.write(",]-_.a/c\60e\61g\62i\63\3\2\13\3\2\62;\4\2GGgg\3\2//\6")
        buf.write("\2\f\f\17\17$$^^\n\2$$))^^ddhhppttvv\5\2C\\aac|\6\2\62")
        buf.write(";C\\aac|\5\2\13\f\17\17\"\"\4\2\f\f\17\17\2\u016d\2\3")
        buf.write("\3\2\2\2\2\5\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\21\3\2")
        buf.write("\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2")
        buf.write("\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2")
        buf.write("#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2")
        buf.write("\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65")
        buf.write("\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2")
        buf.write("\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2")
        buf.write("\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2")
        buf.write("\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3")
        buf.write("\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e")
        buf.write("\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\3l\3\2\2\2\5\u0080\3\2")
        buf.write("\2\2\7\u0082\3\2\2\2\t\u008a\3\2\2\2\13\u008c\3\2\2\2")
        buf.write("\r\u0098\3\2\2\2\17\u009a\3\2\2\2\21\u009d\3\2\2\2\23")
        buf.write("\u00a5\3\2\2\2\25\u00a9\3\2\2\2\27\u00af\3\2\2\2\31\u00b6")
        buf.write("\3\2\2\2\33\u00bb\3\2\2\2\35\u00be\3\2\2\2\37\u00c3\3")
        buf.write("\2\2\2!\u00c7\3\2\2\2#\u00ca\3\2\2\2%\u00d0\3\2\2\2\'")
        buf.write("\u00d6\3\2\2\2)\u00df\3\2\2\2+\u00e6\3\2\2\2-\u00eb\3")
        buf.write("\2\2\2/\u00f1\3\2\2\2\61\u00f3\3\2\2\2\63\u00f5\3\2\2")
        buf.write("\2\65\u00f7\3\2\2\2\67\u00f9\3\2\2\29\u00fb\3\2\2\2;\u00fe")
        buf.write("\3\2\2\2=\u0101\3\2\2\2?\u0104\3\2\2\2A\u0107\3\2\2\2")
        buf.write("C\u010a\3\2\2\2E\u010d\3\2\2\2G\u010f\3\2\2\2I\u0111\3")
        buf.write("\2\2\2K\u0113\3\2\2\2M\u0115\3\2\2\2O\u0117\3\2\2\2Q\u0119")
        buf.write("\3\2\2\2S\u011b\3\2\2\2U\u011d\3\2\2\2W\u011f\3\2\2\2")
        buf.write("Y\u0121\3\2\2\2[\u0123\3\2\2\2]\u0125\3\2\2\2_\u012d\3")
        buf.write("\2\2\2a\u0133\3\2\2\2c\u0141\3\2\2\2e\u014c\3\2\2\2g\u0157")
        buf.write("\3\2\2\2i\u015e\3\2\2\2km\t\2\2\2lk\3\2\2\2mn\3\2\2\2")
        buf.write("nl\3\2\2\2no\3\2\2\2o\4\3\2\2\2pq\5\3\2\2qs\7\60\2\2r")
        buf.write("t\5\3\2\2sr\3\2\2\2st\3\2\2\2tv\3\2\2\2uw\5\7\4\2vu\3")
        buf.write("\2\2\2vw\3\2\2\2w\u0081\3\2\2\2xy\7\60\2\2y{\5\3\2\2z")
        buf.write("|\5\7\4\2{z\3\2\2\2{|\3\2\2\2|\u0081\3\2\2\2}~\5\3\2\2")
        buf.write("~\177\5\7\4\2\177\u0081\3\2\2\2\u0080p\3\2\2\2\u0080x")
        buf.write("\3\2\2\2\u0080}\3\2\2\2\u0081\6\3\2\2\2\u0082\u0084\t")
        buf.write("\3\2\2\u0083\u0085\t\4\2\2\u0084\u0083\3\2\2\2\u0084\u0085")
        buf.write("\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087\5\3\2\2\u0087")
        buf.write("\b\3\2\2\2\u0088\u008b\5+\26\2\u0089\u008b\5-\27\2\u008a")
        buf.write("\u0088\3\2\2\2\u008a\u0089\3\2\2\2\u008b\n\3\2\2\2\u008c")
        buf.write("\u0090\7$\2\2\u008d\u008f\5\r\7\2\u008e\u008d\3\2\2\2")
        buf.write("\u008f\u0092\3\2\2\2\u0090\u008e\3\2\2\2\u0090\u0091\3")
        buf.write("\2\2\2\u0091\u0093\3\2\2\2\u0092\u0090\3\2\2\2\u0093\u0094")
        buf.write("\7$\2\2\u0094\u0095\b\6\2\2\u0095\f\3\2\2\2\u0096\u0099")
        buf.write("\n\5\2\2\u0097\u0099\5\17\b\2\u0098\u0096\3\2\2\2\u0098")
        buf.write("\u0097\3\2\2\2\u0099\16\3\2\2\2\u009a\u009b\7^\2\2\u009b")
        buf.write("\u009c\t\6\2\2\u009c\20\3\2\2\2\u009d\u009e\7d\2\2\u009e")
        buf.write("\u009f\7q\2\2\u009f\u00a0\7q\2\2\u00a0\u00a1\7n\2\2\u00a1")
        buf.write("\u00a2\7g\2\2\u00a2\u00a3\7c\2\2\u00a3\u00a4\7p\2\2\u00a4")
        buf.write("\22\3\2\2\2\u00a5\u00a6\7k\2\2\u00a6\u00a7\7p\2\2\u00a7")
        buf.write("\u00a8\7v\2\2\u00a8\24\3\2\2\2\u00a9\u00aa\7h\2\2\u00aa")
        buf.write("\u00ab\7n\2\2\u00ab\u00ac\7q\2\2\u00ac\u00ad\7c\2\2\u00ad")
        buf.write("\u00ae\7v\2\2\u00ae\26\3\2\2\2\u00af\u00b0\7u\2\2\u00b0")
        buf.write("\u00b1\7v\2\2\u00b1\u00b2\7t\2\2\u00b2\u00b3\7k\2\2\u00b3")
        buf.write("\u00b4\7p\2\2\u00b4\u00b5\7i\2\2\u00b5\30\3\2\2\2\u00b6")
        buf.write("\u00b7\7x\2\2\u00b7\u00b8\7q\2\2\u00b8\u00b9\7k\2\2\u00b9")
        buf.write("\u00ba\7f\2\2\u00ba\32\3\2\2\2\u00bb\u00bc\7k\2\2\u00bc")
        buf.write("\u00bd\7h\2\2\u00bd\34\3\2\2\2\u00be\u00bf\7g\2\2\u00bf")
        buf.write("\u00c0\7n\2\2\u00c0\u00c1\7u\2\2\u00c1\u00c2\7g\2\2\u00c2")
        buf.write("\36\3\2\2\2\u00c3\u00c4\7h\2\2\u00c4\u00c5\7q\2\2\u00c5")
        buf.write("\u00c6\7t\2\2\u00c6 \3\2\2\2\u00c7\u00c8\7f\2\2\u00c8")
        buf.write("\u00c9\7q\2\2\u00c9\"\3\2\2\2\u00ca\u00cb\7y\2\2\u00cb")
        buf.write("\u00cc\7j\2\2\u00cc\u00cd\7k\2\2\u00cd\u00ce\7n\2\2\u00ce")
        buf.write("\u00cf\7g\2\2\u00cf$\3\2\2\2\u00d0\u00d1\7d\2\2\u00d1")
        buf.write("\u00d2\7t\2\2\u00d2\u00d3\7g\2\2\u00d3\u00d4\7c\2\2\u00d4")
        buf.write("\u00d5\7m\2\2\u00d5&\3\2\2\2\u00d6\u00d7\7e\2\2\u00d7")
        buf.write("\u00d8\7q\2\2\u00d8\u00d9\7p\2\2\u00d9\u00da\7v\2\2\u00da")
        buf.write("\u00db\7k\2\2\u00db\u00dc\7p\2\2\u00dc\u00dd\7w\2\2\u00dd")
        buf.write("\u00de\7g\2\2\u00de(\3\2\2\2\u00df\u00e0\7t\2\2\u00e0")
        buf.write("\u00e1\7g\2\2\u00e1\u00e2\7v\2\2\u00e2\u00e3\7w\2\2\u00e3")
        buf.write("\u00e4\7t\2\2\u00e4\u00e5\7p\2\2\u00e5*\3\2\2\2\u00e6")
        buf.write("\u00e7\7v\2\2\u00e7\u00e8\7t\2\2\u00e8\u00e9\7w\2\2\u00e9")
        buf.write("\u00ea\7g\2\2\u00ea,\3\2\2\2\u00eb\u00ec\7h\2\2\u00ec")
        buf.write("\u00ed\7c\2\2\u00ed\u00ee\7n\2\2\u00ee\u00ef\7u\2\2\u00ef")
        buf.write("\u00f0\7g\2\2\u00f0.\3\2\2\2\u00f1\u00f2\7-\2\2\u00f2")
        buf.write("\60\3\2\2\2\u00f3\u00f4\7/\2\2\u00f4\62\3\2\2\2\u00f5")
        buf.write("\u00f6\7,\2\2\u00f6\64\3\2\2\2\u00f7\u00f8\7\61\2\2\u00f8")
        buf.write("\66\3\2\2\2\u00f9\u00fa\7\'\2\2\u00fa8\3\2\2\2\u00fb\u00fc")
        buf.write("\7(\2\2\u00fc\u00fd\7(\2\2\u00fd:\3\2\2\2\u00fe\u00ff")
        buf.write("\7~\2\2\u00ff\u0100\7~\2\2\u0100<\3\2\2\2\u0101\u0102")
        buf.write("\7?\2\2\u0102\u0103\7?\2\2\u0103>\3\2\2\2\u0104\u0105")
        buf.write("\7#\2\2\u0105\u0106\7?\2\2\u0106@\3\2\2\2\u0107\u0108")
        buf.write("\7>\2\2\u0108\u0109\7?\2\2\u0109B\3\2\2\2\u010a\u010b")
        buf.write("\7@\2\2\u010b\u010c\7?\2\2\u010cD\3\2\2\2\u010d\u010e")
        buf.write("\7#\2\2\u010eF\3\2\2\2\u010f\u0110\7>\2\2\u0110H\3\2\2")
        buf.write("\2\u0111\u0112\7@\2\2\u0112J\3\2\2\2\u0113\u0114\7?\2")
        buf.write("\2\u0114L\3\2\2\2\u0115\u0116\7]\2\2\u0116N\3\2\2\2\u0117")
        buf.write("\u0118\7_\2\2\u0118P\3\2\2\2\u0119\u011a\7*\2\2\u011a")
        buf.write("R\3\2\2\2\u011b\u011c\7+\2\2\u011cT\3\2\2\2\u011d\u011e")
        buf.write("\7}\2\2\u011eV\3\2\2\2\u011f\u0120\7\177\2\2\u0120X\3")
        buf.write("\2\2\2\u0121\u0122\7=\2\2\u0122Z\3\2\2\2\u0123\u0124\7")
        buf.write(".\2\2\u0124\\\3\2\2\2\u0125\u0129\t\7\2\2\u0126\u0128")
        buf.write("\t\b\2\2\u0127\u0126\3\2\2\2\u0128\u012b\3\2\2\2\u0129")
        buf.write("\u0127\3\2\2\2\u0129\u012a\3\2\2\2\u012a^\3\2\2\2\u012b")
        buf.write("\u0129\3\2\2\2\u012c\u012e\t\t\2\2\u012d\u012c\3\2\2\2")
        buf.write("\u012e\u012f\3\2\2\2\u012f\u012d\3\2\2\2\u012f\u0130\3")
        buf.write("\2\2\2\u0130\u0131\3\2\2\2\u0131\u0132\b\60\3\2\u0132")
        buf.write("`\3\2\2\2\u0133\u0134\7\61\2\2\u0134\u0135\7,\2\2\u0135")
        buf.write("\u0139\3\2\2\2\u0136\u0138\13\2\2\2\u0137\u0136\3\2\2")
        buf.write("\2\u0138\u013b\3\2\2\2\u0139\u013a\3\2\2\2\u0139\u0137")
        buf.write("\3\2\2\2\u013a\u013c\3\2\2\2\u013b\u0139\3\2\2\2\u013c")
        buf.write("\u013d\7,\2\2\u013d\u013e\7\61\2\2\u013e\u013f\3\2\2\2")
        buf.write("\u013f\u0140\b\61\3\2\u0140b\3\2\2\2\u0141\u0142\7\61")
        buf.write("\2\2\u0142\u0143\7\61\2\2\u0143\u0147\3\2\2\2\u0144\u0146")
        buf.write("\n\n\2\2\u0145\u0144\3\2\2\2\u0146\u0149\3\2\2\2\u0147")
        buf.write("\u0145\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u014a\3\2\2\2")
        buf.write("\u0149\u0147\3\2\2\2\u014a\u014b\b\62\3\2\u014bd\3\2\2")
        buf.write("\2\u014c\u0150\7$\2\2\u014d\u014f\5\r\7\2\u014e\u014d")
        buf.write("\3\2\2\2\u014f\u0152\3\2\2\2\u0150\u014e\3\2\2\2\u0150")
        buf.write("\u0151\3\2\2\2\u0151\u0153\3\2\2\2\u0152\u0150\3\2\2\2")
        buf.write("\u0153\u0155\7^\2\2\u0154\u0156\n\6\2\2\u0155\u0154\3")
        buf.write("\2\2\2\u0155\u0156\3\2\2\2\u0156f\3\2\2\2\u0157\u015b")
        buf.write("\7$\2\2\u0158\u015a\5\r\7\2\u0159\u0158\3\2\2\2\u015a")
        buf.write("\u015d\3\2\2\2\u015b\u0159\3\2\2\2\u015b\u015c\3\2\2\2")
        buf.write("\u015ch\3\2\2\2\u015d\u015b\3\2\2\2\u015e\u015f\13\2\2")
        buf.write("\2\u015fj\3\2\2\2\23\2nsv{\u0080\u0084\u008a\u0090\u0098")
        buf.write("\u0129\u012f\u0139\u0147\u0150\u0155\u015b\4\3\6\2\b\2")
        buf.write("\2")
        return buf.getvalue()


class MCLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    INTLIT = 1
    FLOATLIT = 2
    BOOLEANLIT = 3
    STRINGLIT = 4
    BOOLEANTYPE = 5
    INTTYPE = 6
    FLOATTYPE = 7
    STRINGTYPE = 8
    VOIDTYPE = 9
    IF = 10
    ELSE = 11
    FOR = 12
    DO = 13
    WHILE = 14
    BREAK = 15
    CONTINUE = 16
    RETURN = 17
    TRUE = 18
    FALSE = 19
    ADD = 20
    SUB = 21
    MUL = 22
    DIV = 23
    MOD = 24
    AND = 25
    OR = 26
    EQUAL = 27
    NOTEQUAL = 28
    LE = 29
    GE = 30
    NOT = 31
    LT = 32
    GT = 33
    ASSIGN = 34
    LSB = 35
    RSB = 36
    LB = 37
    RB = 38
    LP = 39
    RP = 40
    SEMI = 41
    COMMA = 42
    ID = 43
    WS = 44
    BLOCK_COMMENT = 45
    Line_COMMENT = 46
    ILLEGAL_ESCAPE = 47
    UNCLOSE_STRING = 48
    ERROR_CHAR = 49

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'boolean'", "'int'", "'float'", "'string'", "'void'", "'if'", 
            "'else'", "'for'", "'do'", "'while'", "'break'", "'continue'", 
            "'return'", "'true'", "'false'", "'+'", "'-'", "'*'", "'/'", 
            "'%'", "'&&'", "'||'", "'=='", "'!='", "'<='", "'>='", "'!'", 
            "'<'", "'>'", "'='", "'['", "']'", "'('", "')'", "'{'", "'}'", 
            "';'", "','" ]

    symbolicNames = [ "<INVALID>",
            "INTLIT", "FLOATLIT", "BOOLEANLIT", "STRINGLIT", "BOOLEANTYPE", 
            "INTTYPE", "FLOATTYPE", "STRINGTYPE", "VOIDTYPE", "IF", "ELSE", 
            "FOR", "DO", "WHILE", "BREAK", "CONTINUE", "RETURN", "TRUE", 
            "FALSE", "ADD", "SUB", "MUL", "DIV", "MOD", "AND", "OR", "EQUAL", 
            "NOTEQUAL", "LE", "GE", "NOT", "LT", "GT", "ASSIGN", "LSB", 
            "RSB", "LB", "RB", "LP", "RP", "SEMI", "COMMA", "ID", "WS", 
            "BLOCK_COMMENT", "Line_COMMENT", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", 
            "ERROR_CHAR" ]

    ruleNames = [ "INTLIT", "FLOATLIT", "FLOAT_EP", "BOOLEANLIT", "STRINGLIT", 
                  "STRINGCHAR", "ESCAPE", "BOOLEANTYPE", "INTTYPE", "FLOATTYPE", 
                  "STRINGTYPE", "VOIDTYPE", "IF", "ELSE", "FOR", "DO", "WHILE", 
                  "BREAK", "CONTINUE", "RETURN", "TRUE", "FALSE", "ADD", 
                  "SUB", "MUL", "DIV", "MOD", "AND", "OR", "EQUAL", "NOTEQUAL", 
                  "LE", "GE", "NOT", "LT", "GT", "ASSIGN", "LSB", "RSB", 
                  "LB", "RB", "LP", "RP", "SEMI", "COMMA", "ID", "WS", "BLOCK_COMMENT", 
                  "Line_COMMENT", "ILLEGAL_ESCAPE", "UNCLOSE_STRING", "ERROR_CHAR" ]

    grammarFileName = "MC.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


    def emit(self):
        tk = self.type
        if tk == self.UNCLOSE_STRING:       
            result = super().emit();
            raise UncloseString(result.text[1:]);
        elif tk == self.ILLEGAL_ESCAPE:
            result = super().emit();
            raise IllegalEscape(result.text[1:]);
        elif tk == self.ERROR_CHAR:
            result = super().emit();
            raise ErrorToken(result.text); 
        else:
            return super().emit();


    def action(self, localctx:RuleContext, ruleIndex:int, actionIndex:int):
        if self._actions is None:
            actions = dict()
            actions[4] = self.STRINGLIT_action 
            self._actions = actions
        action = self._actions.get(ruleIndex, None)
        if action is not None:
            action(localctx, actionIndex)
        else:
            raise Exception("No registered action for:" + str(ruleIndex))


    def STRINGLIT_action(self, localctx:RuleContext , actionIndex:int):
        if actionIndex == 0:
            self.text = self.text[1:-1]
     


