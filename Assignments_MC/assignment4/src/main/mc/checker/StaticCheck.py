
"""
 * @author nhphung
 
"""
from AST import * 
from Visitor import *
from Utils import Utils
from StaticError import *

from functools import reduce

def flatten(lst):
    return [y for x in lst for y in x if type(y) is Symbol]

class MType:
    def __init__(self,partype,rettype):
        self.partype = partype
        self.rettype = rettype

    def __repr__(self):
        return "MTtype("+str(self.partype)+","+str(self.rettype)+")"

class Symbol:
    def __init__(self,name,mtype,value = None,isInvoked = False):
        self.name = name
        self.mtype = mtype
        self.value = value
        self.isInvoked = isInvoked

    def __repr__(self):
        return "Symbol("+str(self.name)+","+str(self.mtype)+","+str(self.value)+","+str(self.isInvoked)+")"

class StaticChecker(BaseVisitor,Utils):
              
    def __init__(self,ast):
        #print(ast)
        #print(ast)
        #print()
        self.ast = ast
    
    def check(self):
        return self.visit(self.ast,StaticChecker.global_envi)

    global_envi = [
        Symbol("getInt",MType([],IntType()), isInvoked=True),
        Symbol("putInt",MType([IntType()],VoidType()), isInvoked=True),
        Symbol("putIntLn",MType([IntType()],VoidType()), isInvoked=True),
        Symbol("getFloat",MType([],FloatType()), isInvoked=True),
        Symbol("putFloat",MType([FloatType()],VoidType()), isInvoked=True),
        Symbol("putFloatLn",MType([FloatType()],VoidType()), isInvoked=True),
        Symbol("putBool",MType([BoolType()],VoidType()), isInvoked=True),
        Symbol("putBoolLn",MType([BoolType()],VoidType()), isInvoked=True),
        Symbol("putString",MType([StringType()],VoidType()), isInvoked=True),
        Symbol("putStringLn",MType([StringType()],VoidType()), isInvoked=True),
        Symbol("putLn",MType([],VoidType()), isInvoked=True)
    ]

    def visitProgram(self,ast, c): 
        env = reduce(lambda env, decl: env + [self.visit(decl, (env, False))],ast.decl, c) 

        res = self.lookup('main', env, lambda x: x.name)

        if res is None or type(res.mtype) is not MType:
            raise NoEntryPoint()
    
        [self.visit(decl,(env, True)) for decl in ast.decl if type(decl) is FuncDecl]

        invoked_check = self.lookup(False, env, lambda x: x.isInvoked if x.name != 'main' else True)

        if invoked_check is not None:
            raise UnreachableFunction(invoked_check.name)
                            
    def visitFuncDecl(self, ast, c):
        if c[1]:
            paramLst = reduce(lambda env, param: env + [self.visit(param, [env])],ast.param,[]) 

            local_env = [paramLst]+[c[0]]+[False]+[ast.name.name]+[ast.returnType]
            res = False

            for mem in ast.body.member:
                if type(mem) is VarDecl:
                    local_env = [local_env[0] + [self.visit(mem, local_env)]]+local_env[1:]
                else:
                    res = (self.visit(mem, local_env) is True) or res

            if not res and type(ast.returnType) is not VoidType:
                raise FunctionNotReturn(ast.name.name) 
        else:
            if self.lookup(ast.name.name,c[0],lambda x: x.name) is not None:
                raise Redeclared(Function(), ast.name.name)
            
            try: 
                paramLst = reduce(lambda env, param: env + [self.visit(param, [env])],ast.param,[])
            except Redeclared as e:
                raise Redeclared(Parameter(), e.n)
        
            return Symbol(ast.name.name, MType([param.mtype for param in paramLst], ast.returnType))

    def visitVarDecl(self, ast, c):
        if self.lookup(ast.variable,c[0],lambda x: x.name) is not None:
            raise Redeclared(Variable(), ast.variable)
        
        return Symbol(ast.variable, ast.varType, isInvoked=True)

    def visitBlock(self, ast, c):
        env = [[]]+c
        res = False

        for mem in ast.member:
            if type(mem) is VarDecl:
                env = [env[0] + [self.visit(mem, env)]]+env[1:]
            else:
                res = (self.visit(mem, env) is True) or res                 

        return res

    def visitIf(self, ast, c):
        exprType = self.visit(ast.expr,c)
        
        if type(exprType) is not BoolType:
            raise TypeMismatchInStatement(ast)

        thenStmtRes = self.visit(ast.thenStmt, c) is True
        
        if ast.elseStmt is not None:
            elseStmtRes = self.visit(ast.elseStmt, c) is True
            
            return thenStmtRes and elseStmtRes
        else:
            return False

    def visitFor(self, ast, c):
        expr1Type = self.visit(ast.expr1, c)
        expr2Type = self.visit(ast.expr2, c)
        expr3Type = self.visit(ast.expr3, c)
        
        if type(expr1Type) is not IntType or type(expr2Type) is not BoolType or type(expr3Type) is not IntType:
            raise TypeMismatchInStatement(ast)

        if not c[-3]:
            c_clone = c[:]
            c_clone[-3] = True

        self.visit(ast.loop, c if c[-3] else c_clone)

        return False

    def visitDowhile(self, ast, c):
        expType = self.visit(ast.exp, c)
        
        if type(expType) is not BoolType:
            raise TypeMismatchInStatement(ast)

        if not c[-3]:
            c_clone = c[:]
            c_clone[-3] = True

        return reduce(lambda res, stmt: res or (self.visit(stmt,c if c[-3] else c_clone) is True),ast.sl,False)

    def visitReturn(self, ast, c):  
        exprType = self.visit(ast.expr, c) if ast.expr is not None else None
        
        if type(c[-1]) is VoidType and exprType is None:
            return False
        elif type(c[-1]) is FloatType and type(exprType) in [IntType, FloatType]:
            return True
        elif type(c[-1]) is ArrayPointerType and type(exprType) in [ArrayType, ArrayPointerType] and type(c[-1].eleType) is type(exprType.eleType):
            return True
        elif type(c[-1]) is type(exprType) and type(c[-1]) not in [VoidType,ArrayPointerType]:
            return True
        else:
            raise TypeMismatchInStatement(ast)

    def visitBreak(self, ast, c):
        if not c[-3]:
            raise BreakNotInLoop
        
        return False
    
    def visitContinue(self, ast, c):
        if not c[-3]:
            raise ContinueNotInLoop
            
        return False
    
    def visitBinaryOp(self, ast, c):
        if ast.op == '=' and type(ast.left) not in [Id,ArrayCell]:
            raise NotLeftValue(ast.left)
        
        leftType = self.visit(ast.left, c)
        rightType = self.visit(ast.right, c)

        if ast.op in ['&&', '||', '==', '!=', '='] and type(leftType) is BoolType and type(rightType) is BoolType:
            return leftType
        elif ast.op in ['+', '-', '*', '/', '%', '='] and type(leftType) is IntType and type(rightType) is IntType:
            return leftType
        elif ast.op in ['<', '<=', '>', '>=', '==', '!='] and type(leftType) is IntType and type(rightType) is IntType:
            return BoolType()
        elif ast.op in ['+', '-', '*', '/'] and (type(leftType),type(rightType)) in [(FloatType,IntType),(IntType,FloatType),(FloatType,FloatType)]:
            return FloatType()
        elif ast.op in ['<', '<=', '>', '>='] and (type(leftType),type(rightType)) in [(FloatType,IntType),(IntType,FloatType),(FloatType,FloatType)]:
            return BoolType()
        elif ast.op == '=' and type(leftType) is FloatType and type(rightType) in [IntType,FloatType]:
            return leftType
        elif ast.op == '=' and type(leftType) is StringType and type(rightType) is StringType:
            return leftType
        else:
            raise TypeMismatchInExpression(ast)
    
    def visitUnaryOp(self, ast, c):
        operandType = self.visit(ast.body, c)

        if ast.op == '-' and type(operandType) in [IntType, FloatType]:
            return operandType
        elif ast.op == '!' and type(operandType) is BoolType:
            return operandType
        else:
            raise TypeMismatchInExpression(ast)

    def visitArrayCell(self, ast, c):
        arrType = self.visit(ast.arr, c)
        idxType = self.visit(ast.idx, c)
        
        if type(arrType) not in [ArrayPointerType, ArrayType] or type(idxType) is not IntType:
            raise TypeMismatchInExpression(ast)

        return arrType.eleType
        
    def visitCallExpr(self, ast, c):
        params = [self.visit(x,c) for x in ast.param]
        res = self.lookup(ast.method.name, flatten(filter(lambda x: type(x) is list,c[:-3])), lambda x: x.name)
        
        if res is None:
            raise Undeclared(Function(),ast.method.name)
        elif type(res.mtype) is not MType or len(res.mtype.partype) != len(params):
            raise TypeMismatchInExpression(ast)
        else:
            if not res.isInvoked and res.name != c[-2]:
                res.isInvoked = True
            
            for i in range(len(params)):
                if type(res.mtype.partype[i]) is FloatType and type(params[i]) in [FloatType, IntType]:
                    continue
                elif type(res.mtype.partype[i]) is ArrayPointerType and type(params[i]) in [ArrayType, ArrayPointerType] and type(res.mtype.partype[i].eleType) is type(params[i].eleType):
                    continue
                elif type(res.mtype.partype[i]) is type(params[i]) and type(params[i]) is not ArrayPointerType:
                    continue
                else:
                    raise TypeMismatchInExpression(ast)

            return res.mtype.rettype
            
    def visitId(self, ast, c):
        res = self.lookup(ast.name, flatten(filter(lambda x: type(x) is list,c)), lambda x: x.name)

        if res is None: 
            raise Undeclared(Identifier(), ast.name)
        
        return res.mtype

    def visitFloatLiteral(self, ast, c):
        return FloatType()

    def visitIntLiteral(self, ast, c):
        return IntType()

    def visitBooleanLiteral(self, ast, c):
        return BoolType()
    
    def visitStringLiteral(self, ast, c):
        return StringType()

