/**
 * Student name: Nguyen Hoang Thuan
 * Student ID: 1752054
**/


grammar MC;

@lexer::header {
from lexererr import *
}

@lexer::members {
def emit(self):
    tk = self.type
    if tk == self.UNCLOSE_STRING:       
        result = super().emit();
        raise UncloseString(result.text[1:]);
    elif tk == self.ILLEGAL_ESCAPE:
        result = super().emit();
        raise IllegalEscape(result.text[1:]);
    elif tk == self.ERROR_CHAR:
        result = super().emit();
        raise ErrorToken(result.text); 
    else:
        return super().emit();
}

options{
	language=Python3;
}

program : (declaration)* EOF ;

declaration : variableDecl | functionDecl ;

variableDecl : primitiveType variable(COMMA variable)* SEMI ;

primitiveType : BOOLEANTYPE | INTTYPE | FLOATTYPE | STRINGTYPE ;

variable : ID | ID LSB INTLIT RSB ;

functionDecl : functionType ID LB parameterList RB blockStatement ;

functionType : primitiveType | arrayPointerType | VOIDTYPE ; 

arrayPointerType : primitiveType LSB RSB ;

parameterList : (parameterDecl (COMMA parameterDecl)*)? ;

parameterDecl : primitiveType ID (LSB RSB)? ;

blockStatement : LP blockBody RP ;

blockBody : (variableDecl | statement)* ;

statement : ifStatement 
          | doWhileStatement
	  | forStatement 
          | breakStatement 
          | continueStatement 
	  | returnStatement 
          | expressionStatement 
          | blockStatement ;

ifStatement: IF LB expression RB statement (ELSE statement)?;

doWhileStatement : DO statement+ WHILE expression SEMI ;

forStatement: FOR LB expression SEMI expression SEMI expression RB statement;

breakStatement : BREAK SEMI ;

continueStatement : CONTINUE SEMI ;

returnStatement : RETURN expression? SEMI ;

expressionStatement : expression SEMI ; 

expression  : expression1 ASSIGN expression | expression1 ;

expression1 : expression1 OR expression2 | expression2 ;

expression2 : expression2 AND expression3 | expression3 ;

expression3 : expression4 (EQUAL|NOTEQUAL) expression4 | expression4 ;

expression4 : expression5 (LT|LE|GT|GE) expression5 | expression5 ;
         
expression5 : expression5 (ADD|SUB) expression6 | expression6 ;
          
expression6 : expression6 (DIV|MUL|MOD) expression7 | expression7 ;
          
expression7 : (SUB|NOT) expression7 | expression8 ;
           
expression8 : expression9 LSB expression RSB | expression9 ;
           
expression9 : LB expression RB
            | literal
            | funcall
            | ID ;
           
literal : INTLIT | FLOATLIT | STRINGLIT | BOOLEANLIT ;

funcall : ID LB expressionList RB ;

expressionList : (expression(COMMA expression)*)? ;

INTLIT : [0-9]+ ;

FLOATLIT : INTLIT '.' INTLIT? FLOAT_EP?
         | '.' INTLIT FLOAT_EP?
         | INTLIT FLOAT_EP ;

fragment FLOAT_EP : [eE][-]? INTLIT ;

BOOLEANLIT : TRUE | FALSE ;

STRINGLIT : '"' STRINGCHAR* '"' 
{self.text = self.text[1:-1]};

fragment STRINGCHAR : ~["\\\n\r] | ESCAPE ;

fragment ESCAPE : '\\' [btnfr"'\\] ;

BOOLEANTYPE : 'boolean' ;

INTTYPE : 'int' ;

FLOATTYPE : 'float' ;

STRINGTYPE : 'string' ;  
 
VOIDTYPE : 'void' ;

IF : 'if' ;

ELSE : 'else' ;

FOR : 'for' ;

DO : 'do' ;

WHILE : 'while' ;

BREAK : 'break' ;

CONTINUE : 'continue' ;

RETURN : 'return' ;

TRUE : 'true' ;

FALSE : 'false' ;

ADD : '+' ;

SUB : '-' ;

MUL : '*' ;

DIV : '/' ;

MOD : '%' ;

AND : '&&' ;

OR : '||' ;

EQUAL : '==' ;

NOTEQUAL : '!=' ;

LE : '<=' ;

GE : '>=' ;

NOT : '!' ;

LT : '<' ; 

GT : '>' ;

ASSIGN : '=' ;

LSB : '[' ;

RSB : ']' ;

LB : '(' ;

RB : ')' ;

LP : '{' ;

RP : '}' ;

SEMI : ';' ;

COMMA : ',' ;

ID : [a-zA-Z_][a-zA-Z0-9_]* ;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines

BLOCK_COMMENT : '/*' .*? '*/' -> skip ;

Line_COMMENT : '//' ~[\r\n]* -> skip ;

ILLEGAL_ESCAPE :  '"' STRINGCHAR* '\\' (~[btnfr"'\\])? ;

UNCLOSE_STRING : '"' STRINGCHAR* ;

ERROR_CHAR : . ;