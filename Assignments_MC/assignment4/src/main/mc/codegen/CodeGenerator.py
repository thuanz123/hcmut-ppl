'''
 *   @author Nguyen Hua Phung
 *   @version 1.0
 *   23/10/2015
 *   This file provides a simple version of code generator
 *
'''
from Utils import *
from StaticCheck import *
from StaticError import *
from Emitter import Emitter
from Frame import Frame
from abc import ABC, abstractmethod

from functools import reduce

class CodeGenerator(Utils):
    def __init__(self):
        self.libName = "io"

    def init(self):
        return [Symbol("getInt", MType([], IntType()), CName(self.libName)),
                Symbol("putInt", MType([IntType()], VoidType()), CName(self.libName)),
                Symbol("putIntLn", MType([IntType()], VoidType()), CName(self.libName)),
                Symbol("getFloat", MType([], FloatType()), CName(self.libName)),
                Symbol("putFloat", MType([FloatType()], VoidType()), CName(self.libName)),
                Symbol("putFloatLn", MType([FloatType()], VoidType()), CName(self.libName)),
                Symbol("putBool",MType([BoolType()],VoidType()),CName(self.libName)),
                Symbol("putBoolLn",MType([BoolType()],VoidType()),CName(self.libName)),
                Symbol("putString",MType([StringType()],VoidType()),CName(self.libName)),
                Symbol("putStringLn",MType([StringType()],VoidType()),CName(self.libName)),
                Symbol("putLn",MType([],VoidType()),CName(self.libName))
                ]

    def gen(self, ast, dir_):
        #ast: AST
        #dir_: String

        gl = self.init()
        gc = CodeGenVisitor(ast, gl, dir_)
        gc.visit(ast, None)

class ClassType(Type):
    def __init__(self, cname):
        #cname: String
        self.cname = cname

    def __str__(self):
        return "ClassType"

    def accept(self, v, param):
        return v.visitClassType(self, param)

class SubBody():
    def __init__(self, frame, sym):
        #frame: Frame
        #sym: List[Symbol]

        self.frame = frame
        self.sym = sym

class Access():
    def __init__(self, frame, sym, isLeft, isFirst, neededDup=False):
        #frame: Frame
        #sym: List[Symbol]
        #isLeft: Boolean
        #isFirst: Boolean
        #neededDup: Boolean

        self.frame = frame
        self.sym = sym
        self.isLeft = isLeft
        self.isFirst = isFirst
        self.neededDup = neededDup

class Val(ABC):
    pass

class Index(Val):
    def __init__(self, value):
        #value: Int

        self.value = value

class CName(Val):
    def __init__(self, value):
        #value: String

        self.value = value

class CodeGenVisitor(BaseVisitor, Utils):
    def __init__(self, astTree, env, dir_):
        #astTree: AST
        #env: List[Symbol]
        #dir_: File

        self.astTree = astTree
        self.env = env
        self.className = "MCClass"
        self.path = dir_
        self.emit = Emitter(self.path + "/" + self.className + ".j")

    def genMETHOD(self, consdecl, o, frame):
        #consdecl: FuncDecl
        #o: Any
        #frame: Frame

        isInit = consdecl.name.name == "<init>" and consdecl.returnType is None
        isClinit = consdecl.name.name == "<clinit>" and consdecl.returnType is None
        isMain = consdecl.name.name == "main" and len(consdecl.param) == 0 and type(consdecl.returnType) is VoidType
        returnType = VoidType() if isInit or isClinit else consdecl.returnType
        methodName = consdecl.name.name
        intype = [ArrayPointerType(StringType())] if isMain else list(map(lambda x: x.varType,consdecl.param))
        mtype = MType(intype, returnType)

        self.emit.printout(self.emit.emitMETHOD(methodName, mtype, not isInit, frame))

        frame.enterScope(True)

        glenv = o

        body = consdecl.body

        # Generate code for parameter declarations
        if isInit:
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), "this", ClassType(self.className), frame.getStartLabel(), frame.getEndLabel(), frame))
        elif isMain:
            self.emit.printout(self.emit.emitVAR(frame.getNewIndex(), "args", ArrayPointerType(StringType()), frame.getStartLabel(), frame.getEndLabel(), frame))
        elif isClinit:
            pass
        else:
            for param in consdecl.param:
                varName = param.variable
                varType = param.varType
                index = frame.getNewIndex()
                
                self.emit.printout(self.emit.emitVAR(index, param.variable, ArrayPointerType(StringType()), frame.getStartLabel(), frame.getEndLabel(), frame))
                glenv = [Symbol(varName, varType, Index(index))] + glenv

        # Generate code for global array
        if isClinit:
            list(map(lambda arrDecl: self.emit.printout(self.emit.emitINITGLBARRAY(self.className + "." + arrDecl.variable,arrDecl.varType,frame)), body.member))
        # Generate code for block members
        else:
            if isInit:
                self.emit.printout(self.emit.emitLABEL(frame.getStartLabel(), frame))
                self.emit.printout(self.emit.emitREADVAR("this", ClassType(self.className), 0, frame))
                self.emit.printout(self.emit.emitINVOKESPECIAL(frame))
            else:
                self.emit.printout(self.emit.emitLABEL(frame.getStartLabel(), frame))

                previousMember = None
                for member in body.member:
                    if type(member) is VarDecl:
                            glenv = self.visit(member, SubBody(frame, glenv)).sym
                    else:
                        if previousMember is not None and not isinstance(previousMember, Stmt):
                            self.emit.printout(self.emit.emitLABEL(frame.currentLabel, frame))
                            frame.getNewLabel()

                        self.visitStmt(member, SubBody(frame, glenv))

                    previousMember = member

                if len(body.member) > 0 and type(body.member[-1]) is VarDecl:
                    self.emit.printout(self.emit.emitLABEL(frame.currentLabel, frame))

            
            self.emit.printout(self.emit.emitLABEL(frame.getEndLabel(), frame))
        
        self.emit.printout(self.emit.emitRETURN(returnType, frame))
            
        self.emit.printout(self.emit.emitENDMETHOD(frame))
        
        frame.exitScope();

    def visitProgram(self, ast, c):
        #ast: Program
        #c: list

        self.emit.printout(self.emit.emitPROLOG(self.className, "java.lang.Object"))
        env = reduce(lambda subbody, decl: self.visit(decl, SubBody(subbody.frame, subbody.sym)), ast.decl, SubBody(None, self.env))

        [self.genMETHOD(decl, env.sym, Frame(decl.name, decl.returnType)) for decl in ast.decl if type(decl) is FuncDecl]
            
        # generate default constructor
        self.genMETHOD(FuncDecl(Id("<init>"), [], None, Block([])), c, Frame("<init>", VoidType()))

	# generate clinit static array
        arr = list(filter(lambda decl: type(decl) is VarDecl and type(decl.varType) is ArrayType,ast.decl))
        
        if len(arr) > 0:
            self.genMETHOD(FuncDecl(Id("<clinit>"), [],None, Block(arr)), c, Frame("<clinit>", VoidType()))    

        self.emit.emitEPILOG()
        
        return c

    def visitVarDecl(self, ast, o):
        #ast: VarDecl
        #o: SubBody

        subctxt = o
        frame = subctxt.frame 
        varName = ast.variable
        varType = ast.varType

        #visit global var
        if frame is None:            
            self.emit.printout(self.emit.emitATTRIBUTE(varName,varType,False,None))

            return SubBody(frame, [Symbol(varName, varType, CName(self.className))] + subctxt.sym)
        #visit local var
        else:
            index = frame.getNewIndex()
            self.emit.printout(self.emit.emitVAR(index,varName,varType,frame.currentLabel,frame.getEndLabel(),frame))
            
            if type(varType) is ArrayType:
                self.emit.printout(self.emit.emitINITARRAY(index,varType,frame))
        
            return SubBody(frame, [Symbol(varName, varType, Index(index))] + subctxt.sym)

    def visitFuncDecl(self, ast, o):
        #ast: FuncDecl
        #o: SubBody

        subctxt = o
        frame = o.frame
        
        return SubBody(frame, [Symbol(ast.name.name, MType([param.varType for param in ast.param], ast.returnType), CName(self.className))] + subctxt.sym)

    def visitStmt(self, ast, o):
        #ast: Stmt
        #o: SubBody
        
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym

        if isinstance(ast, Expr):
            expValue, _ = self.visit(ast, Access(frame, sym, False, True))
            self.emit.printout(expValue)	
        else:
            self.visit(ast, SubBody(frame, sym))

    def visitBlock(self, ast, o):
        #ast: Block
        #o: SubBody

        subctxt = o
        frame = subctxt.frame

        frame.enterScope(False)

        self.emit.printout(self.emit.emitLABEL(frame.getStartLabel(),frame))

        previousMember = None
        
        for member in ast.member:
            if type(member) is VarDecl:
                subctxt = self.visit(member, subctxt)
            else:
                if previousMember is not None and not isinstance(previousMember, Stmt):
                    self.emit.printout(self.emit.emitLABEL(frame.currentLabel, frame))
                    frame.getNewLabel()

                self.visitStmt(member, subctxt)

            previousMember = member

        if len(ast.member) > 0 and type(ast.member[-1]) is VarDecl:
            self.emit.printout(self.emit.emitLABEL(frame.currentLabel, frame))
                   
        self.emit.printout(self.emit.emitLABEL(frame.getEndLabel(),frame))
        frame.exitScope()

    def visitIf(self, ast, o):
        #ast: If
        #o: SubBody
        
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym

        trueLabel = frame.getNewLabel()
        falseLabel = frame.getNewLabel()

        expValue, _ = self.visit(ast.expr, Access(frame, sym, False, False, True))
        self.emit.printout(expValue)
        self.emit.printout(self.emit.emitIFFALSE(falseLabel, frame))
        self.visitStmt(ast.thenStmt, subctxt)
        self.emit.printout(self.emit.emitGOTO(trueLabel, frame))
        self.emit.printout(self.emit.emitLABEL(falseLabel, frame))

        if ast.elseStmt is not None:
            self.visitStmt(ast.elseStmt, subctxt)

        self.emit.printout(self.emit.emitLABEL(trueLabel,frame))

    def visitFor(self, ast, o):
        #ast: For
        #o: SubBody
        
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym

        frame.enterLoop()
        
        inLabel= frame.getNewLabel()
        breLabel = frame.getBreakLabel()
        conLabel = frame.getContinueLabel()

        self.visitStmt(ast.expr1, subctxt)
        self.emit.printout(self.emit.emitLABEL(inLabel, frame))
        exp2Value, _ = self.visit(ast.expr2, Access(frame, sym, False, False, True))
        self.emit.printout(exp2Value)
        self.emit.printout(self.emit.emitIFFALSE(breLabel,frame))
        self.visitStmt(ast.loop, subctxt)
        self.emit.printout(self.emit.emitLABEL(conLabel,frame))
        self.visitStmt(ast.expr3, subctxt)	
        self.emit.printout(self.emit.emitGOTO(inLabel,frame))		
        self.emit.printout(self.emit.emitLABEL(breLabel,frame))
        
        frame.exitLoop()

    def visitDowhile(self, ast, o):
        #ast: Dowhile
        #o: SubBody
        
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym
	
        frame.enterLoop()

        inLabel = frame.getNewLabel()
        breLabel = frame.getBreakLabel()
        conLabel = frame.getContinueLabel()
        
        self.emit.printout(self.emit.emitLABEL(inLabel,frame))
        list(map(lambda stmt: self.visitStmt(stmt, subctxt), ast.sl))
        self.emit.printout(self.emit.emitLABEL(conLabel,frame))
        expValue, _ = self.visit(ast.exp, Access(frame, sym, False, False, True))
        self.emit.printout(expValue)
        self.emit.printout(self.emit.emitIFTRUE(inLabel,frame))
        self.emit.printout(self.emit.emitLABEL(breLabel,frame))
        
        frame.exitLoop()

    def visitBreak(self, ast, o):
        #ast: Break
        #o: SubBody
         
        frame = o.frame
        self.emit.printout(self.emit.emitGOTO(frame.getBreakLabel(),frame))

    def visitContinue(self, ast, o):
        #ast: Continue
        #o: SubBody
         
        frame = o.frame
        self.emit.printout(self.emit.emitGOTO(frame.getContinueLabel(),frame))

    def visitReturn(self, ast, o):
        #ast: Return
        #o: Subody

        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym

        if ast.expr is not None:
            expValue, expType = self.visit(ast.expr, Access(frame, sym, False, False, True))
            self.emit.printout(expValue)

            if type(expType) is IntType and type(frame.returnType) is FloatType:
                self.emit.printout(self.emit.emitI2F(frame))

        self.emit.printout(self.emit.emitGOTO(frame.getEndLabel(),frame))

    def visitCallExpr(self, ast, o):
        #ast: CallExpr
        #o: Access
        subctxt = o
        frame = subctxt.frame
        nenv = subctxt.sym
        sym = self.lookup(ast.method.name, nenv, lambda x: x.name)
        cname = sym.value.value
        ctype = sym.mtype

        buffer = list()
        
        for tup in zip(ctype.partype, ast.param):
            str_, type_ = self.visit(tup[1], Access(frame, nenv, False, False, True))
            str_ = str_ + self.emit.emitI2F(frame) if(type(type_) is IntType and type(tup[0]) is FloatType) else str_
            buffer.append(str_)
            
        buffer.append(self.emit.emitINVOKESTATIC(cname + "/" + ast.method.name, ctype, frame))

        return ''.join(buffer), ctype.rettype

    def visitBinaryOp(self, ast, o):
        #ast: BinaryOp
        #o: Access
        
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym

        buffer = list()

        op = ast.op

        if op == "=":
            if type(ast.left) is ArrayCell:
                leftValue, leftType = self.visit(ast.left, Access(frame, sym, True, False, True))
                buffer.append(leftValue)
                
                rightValue, rightType = self.visit(ast.right, Access(frame, sym, False, False, True))
                buffer.append(rightValue)

                if type(leftType) is not type(rightType):
                    buffer.append(self.emit.emitI2F(frame))

                if subctxt.neededDup:
                    buffer.append(self.emit.emitDUPX2(frame))
                
                buffer.append(self.emit.emitASTORE(leftType, frame))
            else:
                rightValue, rightType = self.visit(ast.right, Access(frame, sym, False, False, True))
                buffer.append(rightValue)

                if subctxt.neededDup:
                    buffer.append(self.emit.emitDUP(frame))

                leftValue, leftType = self.visit(ast.left, Access(frame, sym, True, False, True))

                if type(leftType) is not type(rightType):
                    buffer.append(self.emit.emitI2F(frame))
                
                buffer.append(leftValue)

            return ''.join(buffer), leftType
        elif op == "%":
            leftValue, leftType = self.visit(ast.left, Access(frame, sym, False, False, True))
            rightValue, _ = self.visit(ast.right, Access(frame, sym, False, False, True))

            buffer.append(leftValue)
            buffer.append(rightValue)
            buffer.append(self.emit.emitMOD(frame))

            return ''.join(buffer), leftType
        elif op == "&&":
            leftValue, leftType = self.visit(ast.left, Access(frame, sym, False, False, True))
            rightValue, _ = self.visit(ast.right, Access(frame, sym, False, False, True))

            labelTrue = frame.getNewLabel()
            labelFalse = frame.getNewLabel()

            buffer.append(leftValue)
            buffer.append(self.emit.emitIFFALSE(labelFalse,frame))
            buffer.append(rightValue)
            buffer.append(self.emit.emitIFFALSE(labelFalse,frame))
            buffer.append(self.emit.emitPUSHICONST("1", frame))
            buffer.append(self.emit.emitGOTO(labelTrue, frame))
            buffer.append(self.emit.emitLABEL(labelFalse, frame))
            buffer.append(self.emit.emitPUSHICONST("0", frame))
            buffer.append(self.emit.emitLABEL(labelTrue, frame))

            return ''.join(buffer), leftType 
        elif op == "||":
            leftValue, leftType = self.visit(ast.left, Access(frame, sym, False, False, True))
            rightValue, _ = self.visit(ast.right, Access(frame, sym, False, False, True))

            labelTrue = frame.getNewLabel()
            labelFalse = frame.getNewLabel()

            buffer.append(leftValue)
            buffer.append(self.emit.emitIFTRUE(labelTrue,frame))
            buffer.append(rightValue)
            buffer.append(self.emit.emitIFTRUE(labelTrue,frame))
            buffer.append(self.emit.emitPUSHICONST("0", frame))
            buffer.append(self.emit.emitGOTO(labelFalse, frame))
            buffer.append(self.emit.emitLABEL(labelTrue, frame))
            buffer.append(self.emit.emitPUSHICONST("1", frame))
            buffer.append(self.emit.emitLABEL(labelFalse, frame))
            
            return ''.join(buffer), leftType 
        else:
            leftValue, leftType = self.visit(ast.left, Access(frame, sym, False, False, True))
            rightValue, rightType = self.visit(ast.right, Access(frame, sym, False, False, True))

            if type(leftType) is FloatType or type(rightType) is FloatType:
                returnType = FloatType()
            else:
                returnType = IntType()

            buffer.append(leftValue)

            if type(leftType) is IntType and type(returnType) is FloatType: 
                buffer.append(self.emit.emitI2F(frame))

            buffer.append(rightValue)
            
            if type(rightType) is IntType and type(returnType) is FloatType: 
                buffer.append(self.emit.emitI2F(frame))

            if op in ['+', '-']:
                buffer.append(self.emit.emitADDOP(op, returnType, frame))
            elif op in ['*', '/']:
                buffer.append(self.emit.emitMULOP(op, returnType, frame))
            elif op in [">", "<", ">=", "<=", "==", "!="]:
                buffer.append(self.emit.emitREOP(op, returnType, frame))

                returnType = BoolType()

            return ''.join(buffer), returnType

    def visitUnaryOp(self, ast, o):
        #ast: UnaryOp
        #o: Access    
        
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym

        buffer = list()

        op = ast.op
        
        expValue, expType = self.visit(ast.body, Access(frame, sym, False, False, True))
        buffer.append(expValue)
        
        if op == "-":
            buffer.append(self.emit.emitNEGOP(expType,frame))
        elif op == "!":
            buffer.append(self.emit.emitNOT(expType,frame))
            
        return ''.join(buffer), expType

    def visitId(self, ast, o):
        #ast: Id
        #o: Access
        
        subctxt = o
        frame = subctxt.frame
        nenv = subctxt.sym
        sym = self.lookup(ast.name, nenv, lambda x: x.name)
        mtype = sym.mtype
            
        if type(sym.value) is Index:
            name = sym.name
            index = sym.value.value

            if subctxt.isLeft:
                return self.emit.emitWRITEVAR(name, mtype, index, frame), mtype
            else:
                return self.emit.emitREADVAR(name, mtype, index, frame), mtype
        elif type(sym.value) is CName:
            name = sym.value.value+"."+sym.name
            
            if subctxt.isLeft is True:
                return self.emit.emitPUTSTATIC(name, mtype, frame), mtype
            else:
                return self.emit.emitGETSTATIC(name, mtype, frame), mtype

    def visitArrayCell(self, ast, o):
        #ast: ArrayCell
        #o: Access
        
        buffer = list()
        subctxt = o
        frame = subctxt.frame
        sym = subctxt.sym
        
        arrValue, arrType = self.visit(ast.arr, Access(frame, sym, False, False, True))
        idxValue, _ = self.visit(ast.idx, Access(frame, sym, False, False, True))
        et = arrType.eleType

        buffer.append(arrValue)
        buffer.append(idxValue)

        if not subctxt.isLeft:
            buffer.append(self.emit.emitALOAD(et, frame))

        return ''.join(buffer), et    
            
    def visitIntLiteral(self, ast, o):
        #ast: IntLiteral
        #o: Access

        subctxt = o
        frame = subctxt.frame
        return self.emit.emitPUSHICONST(ast.value, frame), IntType()

    def visitFloatLiteral(self, ast, o):
        #ast: FloatLiteral
        #o: Access

        subctxt = o
        frame = subctxt.frame
        return self.emit.emitPUSHFCONST(str(ast.value), frame), FloatType()
	
    def visitStringLiteral(self, ast, o):
        #ast: StringLiteral
        #o: Access
        
        sub = o
        frame = sub.frame
        return self.emit.emitPUSHCONST('"'+ast.value+'"', StringType(), frame), StringType()

    def visitBooleanLiteral(self, ast, o):
        #ast: BooleanLiteral
        #o: Access
        
        subctxt = o
        frame = subctxt.frame
        return self.emit.emitPUSHICONST(str(ast.value), frame), BoolType()
        
    
