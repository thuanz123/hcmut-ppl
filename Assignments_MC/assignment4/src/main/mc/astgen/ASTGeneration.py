"""
    Student name: Nguyen Hoang Thuan
    Student ID: 1752054
"""

from MCVisitor import MCVisitor
from MCParser import MCParser
from AST import *

from functools import reduce

class ASTGeneration(MCVisitor):
    def visitProgram(self, ctx:MCParser.ProgramContext):
        return Program(reduce(lambda declList,decl:declList+(decl if isinstance(decl,list) else [decl]),[self.visit(child) for child in ctx.declaration()],[]))

    def visitDeclaration(self, ctx:MCParser.DeclarationContext):
        return self.visitChildren(ctx)

    def visitVariableDecl(self, ctx:MCParser.VariableDeclContext):
        return [VarDecl(x.ID().getText(), self.visit(ctx.primitiveType()) if x.getChildCount() == 1 else ArrayType(int(x.INTLIT().getText()), self.visit(ctx.primitiveType()))) for x in ctx.variable()]
    
    def visitPrimitiveType(self,ctx:MCParser.PrimitiveTypeContext):
        if ctx.INTTYPE():
            return IntType()
        elif ctx.FLOATTYPE():
            return FloatType()
        elif ctx.STRINGTYPE():
            return StringType()
        else:
            return BoolType()

    def visitFunctionDecl(self, ctx:MCParser.FunctionDeclContext):
        return FuncDecl(Id(ctx.ID().getText()), self.visit(ctx.parameterList()), self.visit(ctx.functionType()), self.visit(ctx.blockStatement()))  

    def visitFunctionType(self, ctx:MCParser.FunctionTypeContext):
        if ctx.primitiveType():
            return self.visit(ctx.primitiveType())
        elif ctx.arrayPointerType():
            return self.visit(ctx.arrayPointerType())
        else:
            return VoidType()

    def visitArrayPointerType(self, ctx:MCParser.ArrayPointerTypeContext):
        return ArrayPointerType(self.visit(ctx.primitiveType()))

    def visitParameterList(self, ctx:MCParser.ParameterListContext):
        return [self.visit(x) for x in ctx.parameterDecl()]

    def visitParameterDecl(self, ctx:MCParser.ParameterDeclContext):
        return VarDecl(ctx.ID().getText(), self.visit(ctx.primitiveType()) if ctx.getChildCount() == 2 else ArrayPointerType(self.visit(ctx.primitiveType())))

    def visitBlockStatement(self, ctx:MCParser.BlockStatementContext):   
        return Block(self.visit(ctx.blockBody()))

    def visitBlockBody(self, ctx:MCParser.BlockBodyContext):
        return reduce(lambda blockBody,blockMem:blockBody+(blockMem if isinstance(blockMem,list) else [blockMem]),[self.visit(ctx.getChild(index)) for index in range(ctx.getChildCount())],[])

    def visitStatement(self, ctx:MCParser.StatementContext):
        return self.visitChildren(ctx)

    def visitIfStatement(self, ctx:MCParser.IfStatementContext):
        return If(self.visit(ctx.expression()), self.visit(ctx.statement(0)), None if len(ctx.statement()) == 1 else self.visit(ctx.statement(1)))

    def visitForStatement(self, ctx:MCParser.ForStatementContext):
        return For(self.visit(ctx.expression(0)), self.visit(ctx.expression(1)), self.visit(ctx.expression(2)), self.visit(ctx.statement()))

    def visitDoWhileStatement(self, ctx:MCParser.DoWhileStatementContext):
        return Dowhile([self.visit(x) for x in ctx.statement()], self.visit(ctx.expression()))

    def visitBreakStatement(self, ctx:MCParser.BreakStatementContext):
        return Break()

    def visitContinueStatement(self, ctx:MCParser.ContinueStatementContext):
        return Continue()

    def visitReturnStatement(self, ctx:MCParser.ReturnStatementContext):
        return Return(self.visit(ctx.expression()) if ctx.expression() else None)

    def visitExpressionStatement(self, ctx:MCParser.ExpressionStatementContext):
        return self.visit(ctx.expression())

    def visitExpression(self, ctx:MCParser.ExpressionContext):
        return self.visit(ctx.expression1()) if ctx.getChildCount() == 1 else BinaryOp(ctx.ASSIGN().getText(), self.visit(ctx.expression1()), self.visit(ctx.expression()))

    def visitExpression1(self, ctx:MCParser.Expression1Context):
        return self.visit(ctx.expression2()) if ctx.getChildCount() == 1 else BinaryOp(ctx.OR().getText(), self.visit(ctx.expression1()), self.visit(ctx.expression2()))

    def visitExpression2(self, ctx:MCParser.Expression2Context):
        return self.visit(ctx.expression3()) if ctx.getChildCount() == 1 else BinaryOp(ctx.AND().getText(), self.visit(ctx.expression2()), self.visit(ctx.expression3()))

    def visitExpression3(self, ctx:MCParser.Expression3Context):
        return self.visit(ctx.expression4(0)) if ctx.getChildCount() == 1 else BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.expression4(0)), self.visit(ctx.expression4(1)))

    def visitExpression4(self, ctx:MCParser.Expression4Context):
        return self.visit(ctx.expression5(0)) if ctx.getChildCount() == 1 else BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.expression5(0)), self.visit(ctx.expression5(1)))

    def visitExpression5(self, ctx:MCParser.Expression5Context):
        return self.visit(ctx.expression6()) if ctx.getChildCount() == 1 else BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.expression5()), self.visit(ctx.expression6()))

    def visitExpression6(self, ctx:MCParser.Expression6Context):
        return self.visit(ctx.expression7()) if ctx.getChildCount() == 1 else BinaryOp(ctx.getChild(1).getText(), self.visit(ctx.expression6()), self.visit(ctx.expression7()))

    def visitExpression7(self, ctx:MCParser.Expression7Context):
        return self.visit(ctx.expression8()) if ctx.getChildCount() == 1 else UnaryOp(ctx.getChild(0).getText(), self.visit(ctx.expression7()))

    def visitExpression8(self, ctx:MCParser.Expression8Context):
        return self.visit(ctx.expression9()) if ctx.getChildCount() == 1 else ArrayCell(self.visit(ctx.expression9()), self.visit(ctx.expression()))

    def visitExpression9(self, ctx:MCParser.Expression9Context):
        if(ctx.literal()):
            return self.visit(ctx.literal())
        elif(ctx.funcall()):
            return self.visit(ctx.funcall())
        elif(ctx.ID()):
            return Id(ctx.ID().getText())
        else:
            return self.visit(ctx.expression())

    def visitLiteral(self, ctx:MCParser.LiteralContext):
        if ctx.INTLIT():
            return IntLiteral(int(ctx.INTLIT().getText()))
        if ctx.FLOATLIT():
            return FloatLiteral(float(ctx.FLOATLIT().getText()))
        elif ctx.STRINGLIT():
            return StringLiteral(ctx.STRINGLIT().getText())
        elif ctx.BOOLEANLIT():
            return BooleanLiteral(ctx.BOOLEANLIT().getText() == "true")

    def visitFuncall(self, ctx:MCParser.FuncallContext):
        return CallExpr(Id(ctx.ID().getText()), self.visit(ctx.expressionList()))

    def visitExpressionList(self, ctx:MCParser.ExpressionListContext):
        return [self.visit(x) for x in ctx.expression()] if ctx.expression() else []
