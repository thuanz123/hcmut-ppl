.source MCClass.java
.class public MCClass
.super java.lang.Object
.field static a [I
.field static b [F

.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String; from Label0 to Label1
Label0:
	getstatic MCClass.a [I
	iconst_0
	iconst_3
	iastore
	getstatic MCClass.b [F
	iconst_0
	iconst_4
	i2f
	fastore
	invokestatic MCClass/foo1()[F
	iconst_0
	faload
	invokestatic io/putFloat(F)V
	invokestatic MCClass/bar()[I
	iconst_0
	iaload
	invokestatic io/putInt(I)V
	invokestatic MCClass/foo()[Ljava/lang/String;
	iconst_0
	aaload
	invokestatic io/putString(Ljava/lang/String;)V
	ldc " "
	invokestatic io/putString(Ljava/lang/String;)V
	invokestatic MCClass/foo()[Ljava/lang/String;
	iconst_1
	aaload
	invokestatic io/putString(Ljava/lang/String;)V
Label1:
	return
.limit stack 3
.limit locals 1
.end method

.method public static foo()[Ljava/lang/String;
Label0:
.var 0 is a [Ljava/lang/String; from Label2 to Label1
	iconst_2
	anewarray java/lang/String
	astore_0
Label2:
	aload_0
	iconst_0
	ldc "hello"
	aastore
	aload_0
	iconst_1
	ldc "world"
	aastore
	aload_0
	goto Label1
Label1:
	areturn
.limit stack 3
.limit locals 1
.end method

.method public static bar()[I
Label0:
	getstatic MCClass.a [I
	goto Label1
Label1:
	areturn
.limit stack 1
.limit locals 0
.end method

.method public static foo1()[F
Label0:
	getstatic MCClass.b [F
	goto Label1
Label1:
	areturn
.limit stack 1
.limit locals 0
.end method

.method public <init>()V
.var 0 is this LMCClass; from Label0 to Label1
Label0:
	aload_0
	invokespecial java/lang/Object/<init>()V
Label1:
	return
.limit stack 1
.limit locals 1
.end method

.method public static <clinit>()V
	iconst_1
	newarray int
	putstatic MCClass.a [I
	iconst_1
	newarray float
	putstatic MCClass.b [F
	return
.limit stack 1
.limit locals 0
.end method
