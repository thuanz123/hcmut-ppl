.source MCClass.java
.class public MCClass
.super java.lang.Object

.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String; from Label0 to Label1
Label0:
	sipush 2018
	invokestatic MCClass/is_leap_year(I)V
Label1:
	return
.limit stack 1
.limit locals 1
.end method

.method public static is_leap_year(I)V
.var 0 is year [Ljava/lang/String; from Label0 to Label1
Label0:
	iload_0
	sipush 400
	irem
	iconst_0
	if_icmpne Label4
	iconst_1
	goto Label5
Label4:
	iconst_0
Label5:
	ifgt Label12
	iload_0
	bipush 100
	irem
	iconst_0
	if_icmpeq Label6
	iconst_1
	goto Label7
Label6:
	iconst_0
Label7:
	ifle Label11
	iload_0
	iconst_4
	irem
	iconst_0
	if_icmpne Label8
	iconst_1
	goto Label9
Label8:
	iconst_0
Label9:
	ifle Label11
	iconst_1
	goto Label10
Label11:
	iconst_0
Label10:
	ifgt Label12
	iconst_0
	goto Label13
Label12:
	iconst_1
Label13:
	ifle Label3
Label14:
	iload_0
	invokestatic io/putInt(I)V
	ldc " is a leap year"
	invokestatic io/putString(Ljava/lang/String;)V
Label15:
	goto Label2
Label3:
Label16:
	iload_0
	invokestatic io/putInt(I)V
	ldc " is not a leap year"
	invokestatic io/putString(Ljava/lang/String;)V
Label17:
Label2:
Label1:
	return
.limit stack 5
.limit locals 1
.end method

.method public <init>()V
.var 0 is this LMCClass; from Label0 to Label1
Label0:
	aload_0
	invokespecial java/lang/Object/<init>()V
Label1:
	return
.limit stack 1
.limit locals 1
.end method
