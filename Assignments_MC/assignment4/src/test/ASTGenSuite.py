"""
    Student name: Nguyen Hoang Thuan
    Student ID: 1752054
"""

import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    def test_simple_unary_op_with_float_number(self):
        input = """void main(){
        int i;
      for(i=0;i<3;i=i+1){}
    }
                   """
        expected = ""
        self.assertTrue(TestAST.checkASTGen(input,expected,301))
