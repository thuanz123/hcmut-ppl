import unittest
from TestUtils import TestLexer

class LexerSuite(unittest.TestCase):
    def test_block_comment(self):
        """test block comment"""
        self.assertTrue(TestLexer.checkLexeme("""/*This is a block comment*/""","""<EOF>""",101))
    def test_block_comment_many_lines(self):
        """test block comment with many lines"""
        self.assertTrue(TestLexer.checkLexeme("""/*This
                                              is
                                              a
                                              block
                                              comment*/""","""<EOF>""",102))
    def test_block_comment_with_start_slash_asterisk(self):
        """test block comment with /*"""
        self.assertTrue(TestLexer.checkLexeme("""/*This /*is a block */ comment""","""comment,<EOF>""",103))
    def test_block_comment_with_end_asterisk_slash(self):
        """test block comment with /*"""
        self.assertTrue(TestLexer.checkLexeme("""/*This */is a block */ comment""","""is,a,block,*,/,comment,<EOF>""",104))
    
    def test_block_comment_with_double_slash(self):
        """test block comment with //"""
        self.assertTrue(TestLexer.checkLexeme("""/*This //is a block */ comment""","""comment,<EOF>""",105))
    def test_line_comment(self):
        """test line comment"""
        self.assertTrue(TestLexer.checkLexeme("""//This is a line comment""","""<EOF>""",106))
    def test_line_seperated_by_double_slash(self):
        """test line seperated by //"""
        self.assertTrue(TestLexer.checkLexeme("""This is a //line comment""","""This,is,a,<EOF>""",107))
    def test_both_block_and_line_comment(self):
        """test both block and line comment"""
        self.assertTrue(TestLexer.checkLexeme("""///*This is not
                                                 a block comment*/""","""a,block,comment,*,/,<EOF>""",108))
    def test_unclosed_block_comment(self):
        """test an unclosed block comment"""
        self.assertTrue(TestLexer.checkLexeme("""/*This is an unclosed comment""","""/,*,This,is,an,unclosed,comment,<EOF>""",109))
    def test_error_token_in_comment(self):
        """test error token in comment"""
        self.assertTrue(TestLexer.checkLexeme("""//Is this a comment?""","""<EOF>""",110))
    def test_error_token(self):
        """test error token"""
        self.assertTrue(TestLexer.checkLexeme("""Is this a comment?""","""Is,this,a,comment,Error Token ?""",111))
    def test_simple_identifier(self):
        """test simple indentifier"""
        self.assertTrue(TestLexer.checkLexeme("""name Name NaMe name1 Name2 NaMe3 NAME4""","""name,Name,NaMe,name1,Name2,NaMe3,NAME4,<EOF>""",112))
    def test_identifier_with_underscore(self):
        """test identifier with underscore"""
        self.assertTrue(TestLexer.checkLexeme("""_ _name _Name _1 name_1""","""_,_name,_Name,_1,name_1,<EOF>""",113))
    def test_identifier_with_unallowed_symbols(self):
        """test identifier with unallowed symbols"""
        self.assertTrue(TestLexer.checkLexeme("""na/me Na-me NA+ME na_*me/1""","""na,/,me,Na,-,me,NA,+,ME,na_,*,me,/,1,<EOF>""",114)) 
    def test_identifier_begining_with_numbers(self):
        """test identifier begining with numbers"""
        self.assertTrue(TestLexer.checkLexeme("""1Name 23name45 name67""","""1,Name,23,name45,name67,<EOF>""",115))
    def test_keyword(self):
        """test keyword"""
        self.assertTrue(TestLexer.checkLexeme("""boolean break continue else for float if int return void do while true false string""","""boolean,break,continue,else,for,float,if,int,return,void,do,while,true,false,string,<EOF>""",116))
    def test_declare_variable(self):
        """test declare variable"""
        self.assertTrue(TestLexer.checkLexeme("""boolean check; 
                                                 string _name;
                                                 int age1, age2; 
                                                 float num;""", """boolean,check,;,string,_name,;,int,age1,,,age2,;,float,num,;,<EOF>""",117))
    def test_declare_function(self):
        """test declare function"""
        self.assertTrue(TestLexer.checkLexeme("""void main(){}
                                                 string[] getName(){}""","""void,main,(,),{,},string,[,],getName,(,),{,},<EOF>""",118))
    def test_if_statement(self):
        """test if statement"""
        self.assertTrue(TestLexer.checkLexeme("""if(true) x=1;
                                                 else x=2;""","""if,(,true,),x,=,1,;,else,x,=,2,;,<EOF>""",119))
    def test_for_statement(self):
        """test for statement"""
        self.assertTrue(TestLexer.checkLexeme("""for(int i=1;i<5;i=i+1){
                                                     print(i);
                                                 }""", """for,(,int,i,=,1,;,i,<,5,;,i,=,i,+,1,),{,print,(,i,),;,},<EOF>""",120))
    def test_do_while_statement(self):
        """test do while statement"""
        self.assertTrue(TestLexer.checkLexeme("""x=0;
                                                 do
                                                     x=x+1;
                                                     x=x*2;
                                                 while(x<100); ""","""x,=,0,;,do,x,=,x,+,1,;,x,=,x,*,2,;,while,(,x,<,100,),;,<EOF>""",121))
    def test_retrun_break_continue_statement(self):
        """test return, break and continue statement"""
        self.assertTrue(TestLexer.checkLexeme("""return a;
                                                 return;
                                                 break;
                                                 continue;""","""return,a,;,return,;,break,;,continue,;,<EOF>""",122))

    def test_valid_operator(self):
        """test valid operator"""
        self.assertTrue(TestLexer.checkLexeme("""+ - ! * / % > < >= <= == != && || =""","""+,-,!,*,/,%,>,<,>=,<=,==,!=,&&,||,=,<EOF>""",123))
    def test_invalid_or_operator(self):
        """test invalid |"""
        self.assertTrue(TestLexer.checkLexeme("""a | b""","""a,Error Token |""",124))        
    def test_invalid_and_operator(self):
        """test invalid |"""
        self.assertTrue(TestLexer.checkLexeme("""a & b""","""a,Error Token &""",125))
    def test_invalid_power_operator(self):
        """test invalid |"""
        self.assertTrue(TestLexer.checkLexeme("""2 ^ 3""","""2,Error Token ^""",126))
    def test_invalid_compare_operator(self):
        """test << and >>"""
        self.assertTrue(TestLexer.checkLexeme("""50>>30<<40""","""50,>,>,30,<,<,40,<EOF>""",127))
    def test_expression_with_math_operator(self):
        """test expression with math operator"""
        self.assertTrue(TestLexer.checkLexeme("""5%3+a/3-b*2.2E2""","""5,%,3,+,a,/,3,-,b,*,2.2E2,<EOF>""",128))
    def test_expression_with_logic_operator(self):
        """test with logic operator"""
        self.assertTrue(TestLexer.checkLexeme("""a||b&&c""","""a,||,b,&&,c,<EOF>""",129))
    def test_expression_with_compare_operator(self):
        """test with compare operator"""
        self.assertTrue(TestLexer.checkLexeme("""a>b<c>=5<=2""","""a,>,b,<,c,>=,5,<=,2,<EOF>""",130))
    def test_seperator(self):
        """test seperator"""
        self.assertTrue(TestLexer.checkLexeme("""( ) [ ] { } ; ,""","""(,),[,],{,},;,,,<EOF>""",131))
    def test_expression_with_seperator(self):
        """test expression with seperator"""
        self.assertTrue(TestLexer.checkLexeme("""(5+a)*x[2]-b%2""","""(,5,+,a,),*,x,[,2,],-,b,%,2,<EOF>""",132))
    def test_expression_with_two_seperator(self):
        """test with two seperator"""
        self.assertTrue(TestLexer.checkLexeme("""(a&&b)||(b||c)""","""(,a,&&,b,),||,(,b,||,c,),<EOF>""",133))
    def test_expression_with_nested_seperator(self):
        """test expression with nested seperator"""
        self.assertTrue(TestLexer.checkLexeme("""[(2+3)*(4-6)+4/5] == 0""","""[,(,2,+,3,),*,(,4,-,6,),+,4,/,5,],==,0,<EOF>""",134))
    def test_expression_with_comma_and_semi_seperator(self):
        """test with comma and semi seperator"""
        self.assertTrue(TestLexer.checkLexeme("""int a,b; string c;""","""int,a,,,b,;,string,c,;,<EOF>""",135))
    def test_valid_int_literal(self):
        """test valid int literal"""
        self.assertTrue(TestLexer.checkLexeme("""0 00 24 39 123456789 000000""","""0,00,24,39,123456789,000000,<EOF>""",136))
    def test_not_sign_in_int_literal(self):
        """test not sign in int literal"""
        self.assertTrue(TestLexer.checkLexeme("""+0 -00 +24 -39""","""+,0,-,00,+,24,-,39,<EOF>""",137))
    def test_not_hex_in_int_literal(self):
        """test not hex in int literal"""
        self.assertTrue(TestLexer.checkLexeme("""0x24EF39""","""0,x24EF39,<EOF>""",138))
    def test__simple_float_literal(self):
        """test simple float literal"""
        self.assertTrue(TestLexer.checkLexeme("""123. .123 1.23""","""123.,.123,1.23,<EOF>""",139))
    def test_float_literal_with_exponent(self):
        """test float literal with exponent"""
        self.assertTrue(TestLexer.checkLexeme("""123.e2 .123E-2 1.23e0""","""123.e2,.123E-2,1.23e0,<EOF>""",140))
    def test_invalid_float_with_plus(self):
        """test invalid float with +"""
        self.assertTrue(TestLexer.checkLexeme("""12.3e+2 1.23E+2""","""12.3,e,+,2,1.23,E,+,2,<EOF>""",141))
    def test_invalid_float_without_number_after_exponent(self):
        """test invalid float without number after exponent"""
        self.assertTrue(TestLexer.checkLexeme("""12.3e 1.23E""","""12.3,e,1.23,E,<EOF>""",142))
    def test_invalid_float_without_number_before_exponent(self):
        """test invalid float without number before exponent"""
        self.assertTrue(TestLexer.checkLexeme("""e2 E-2""","""e2,E,-,2,<EOF>""",143))
    def test_invalid_float_without_decimal_and_fraction(self):
        """test invalid float without decimal and fraction"""
        self.assertTrue(TestLexer.checkLexeme(""".e2 .E-2""","""Error Token .""",144))
    def test_invalid_float_with_float_exponent(self):
        """test invalid float with float exponent"""
        self.assertTrue(TestLexer.checkLexeme("""12.3e1.23 1.23E.123""","""12.3e1,.23,1.23,E,.123,<EOF>""",145))
    def test_valid_boolean_literal(self):
        """test valid boolean literal"""
        self.assertTrue(TestLexer.checkLexeme("""true false""","""true,false,<EOF>""",146))
    def test_invalid_boolean_literal(self):
        """test invalid boolean literal"""
        self.assertTrue(TestLexer.checkLexeme("""True TRUE False FALSE""","""True,TRUE,False,FALSE,<EOF>""",147))
    def test_string_with_undefined_characters(self):
        """test string with undefined characters"""
        self.assertTrue(TestLexer.checkLexeme(""" "1234567890~!@#$%^&*()_+{}:;'<>,./?|" ""","""1234567890~!@#$%^&*()_+{}:;'<>,./?|,<EOF>""",148))
    def test_simple_string(self):
        """test simple string"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is a string" ""","""this is a string,<EOF>""",149))
    def test_empty_string(self):
        """test empty string"""
        self.assertTrue(TestLexer.checkLexeme(""" "" """,""",<EOF>""",150))
    def test_many_string(self):
        """test many string"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is a string" "this is another string" "this is a last string" ""","""this is a string,this is another string,this is a last string,<EOF>""",151))
    def test_many_empty_string(self):
        """test many empty string"""
        self.assertTrue(TestLexer.checkLexeme(""" "" "" "" """,""",,,<EOF>""",152))
    def test_block_comment_in_string(self):
        """test block comment in string"""
        self.assertTrue(TestLexer.checkLexeme(""" "this /*is a*/ string" ""","""this /*is a*/ string,<EOF>""",153))
    def test_line_comment_in_string(self):
        """test line comment in string"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is // a string" ""","""this is // a string,<EOF>""",154))
    def test_string_with_double_quote(self):
        """test string with double qoute"""
        self.assertTrue(TestLexer.checkLexeme(""" "this "is" a string" ""","""this ,is, a string,<EOF>""",155))
    def test_string_with_single_quote(self):
        """test string with sinle qoute"""
        self.assertTrue(TestLexer.checkLexeme(""" "this 'is' a string" ""","""this 'is' a string,<EOF>""",156))
    def test_string_with_escape_sequence(self):
        """test string with escape sequence"""
        self.assertTrue(TestLexer.checkLexeme(""" "This is \\b \\n \\r \\f \\t \\\ \\" \\' a string" ""","""This is \\b \\n \\r \\f \\t \\\ \\" \\' a string,<EOF>""",157))
    def test_complex_string(self):
        """test string with undefined characters"""
        self.assertTrue(TestLexer.checkLexeme(""" "This is a string; and it is complex, which has comma and semicolon" ""","""This is a string; and it is complex, which has comma and semicolon,<EOF>""",158))
    def test_unclosed_string(self):
        """test unclosed string"""
        self.assertTrue(TestLexer.checkLexeme(""" "This is an unclosed string""","""Unclosed String: This is an unclosed string""",159))
    def test_string_with_newline(self):
        """test string with newline"""
        self.assertTrue(TestLexer.checkLexeme(""" "This is
                                                  a string" ""","""Unclosed String: This is""",160))
    def test_unclosed_empty_string(self):
        """test unclosed empty string"""
        self.assertTrue(TestLexer.checkLexeme(""" " ""","""Unclosed String:  """,161))
    def test_unclosed_string_with_escape_sequence(self):
        """test unclosed string with escape sequence"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is a \n\b\r\t string" ""","""Unclosed String: this is a """,162))
    def test_with_three_double_quotes(self):
        """test with three double quotes"""
        self.assertTrue(TestLexer.checkLexeme(""" " " " """,""" ,Unclosed String:  """,163))
    def test_empty_escape(self):
        """test empty escape"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is \\ a string" ""","""Illegal Escape In String: this is \\ """,164))
    def test_illegal_escape(self):
        """test illegal escape"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is \\a string" ""","""Illegal Escape In String: this is \\a""",165))
    def test_illegal_escape_without_white_space(self):
        """test illegal escape without white space"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is a \\string" ""","""Illegal Escape In String: this is a \\s""",166))
    def test_string__ending_with_escape_double_quote(self):
        """test string ending with escape double quote"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is a string\\" ""","""Unclosed String: this is a string\\" """,167))
    def test_string_with_many_slash(self):
        """test string with many slash"""
        self.assertTrue(TestLexer.checkLexeme(""" "this is a \\\\\ string" ""","""Illegal Escape In String: this is a \\\\\ """,168))
    def test_undefined_characters_in_block_comment(self):
        """test undefined characters in block comment"""
        self.assertTrue(TestLexer.checkLexeme("""/* this is @#$%^
                                              block comment?*/""","""<EOF>""",169))
    def test_undefined_characters_in_line_comment(self):
        """test undefined characters in line comment"""
        self.assertTrue(TestLexer.checkLexeme("""// this is @#$%^&*& line comment""","""<EOF>""",170))
    def test_undefined_characters(self):
        """test undefined characters"""
        self.assertTrue(TestLexer.checkLexeme("""thuan@gmail.com""","""thuan,Error Token @""",171))
    def test_simple_program(self):
        """test simple program"""
        self.assertTrue(TestLexer.checkLexeme("""int main() {
                                                 }""","""int,main,(,),{,},<EOF>""",172))
    def test_program_with_line_comment(self):
        """test program with comment"""
        self.assertTrue(TestLexer.checkLexeme("""int main() {
                                                     int a;
                                                     a = 3;
                                                     // this line is ignored
                                                 }""","""int,main,(,),{,int,a,;,a,=,3,;,},<EOF>""",173))
    def test_program_with_block_comment(self):
        """test program with block comment"""
        self.assertTrue(TestLexer.checkLexeme("""int main() {
                                                     int a;
                                                     /*this block
                                                       is ignored*/
                                                 }""","""int,main,(,),{,int,a,;,},<EOF>""",174))
    def test_function_declaration(self):
        """test function declaration"""
        self.assertTrue(TestLexer.checkLexeme("""int foo(int x) {
                                                     x = x + 1;
                                                     return x;
                                                 }""","""int,foo,(,int,x,),{,x,=,x,+,1,;,return,x,;,},<EOF>""",175))
    def test_function_with_array_type_parameters(self):
        """test function with array-typed paramaters"""
        self.assertTrue(TestLexer.checkLexeme("""float sum(float[] sum, int len) {
                                                     float a;
                                                     return a;
                                                 }""","""float,sum,(,float,[,],sum,,,int,len,),{,float,a,;,return,a,;,},<EOF>""",176))
    def test_program_has_statement(self):
        """test program has statement"""
        self.assertTrue(TestLexer.checkLexeme("""int main() {
                                                     if a < 2
                                                         a = 3;
                                                     else
                                                         continue;
                                                 }""","""int,main,(,),{,if,a,<,2,a,=,3,;,else,continue,;,},<EOF>""",177))
    def test_program_has_string(self):
        """test program has string"""
        self.assertTrue(TestLexer.checkLexeme("""int main(){
                                                     string s;
                                                     s = "a string";
                                                 }""","""int,main,(,),{,string,s,;,s,=,a string,;,},<EOF>""",178))
    def test_program_has_builtin_function(self):
        """test with logic operator"""
        self.assertTrue(TestLexer.checkLexeme("""int main(){
                                                     putString(s);
                                                 }""","""int,main,(,),{,putString,(,s,),;,},<EOF>""",179))
    def test_invalid_variable_declaration(self):
        """test invalid variable declaration"""
        self.assertTrue(TestLexer.checkLexeme("""boolean float = true;""","""boolean,float,=,true,;,<EOF>""",180))
    def test_array_pointer_declaration(self):
        """test array pointer declaration"""
        self.assertTrue(TestLexer.checkLexeme("""int a[100];""","""int,a,[,100,],;,<EOF>""",181))
    def test_string_declaration_and_assignment(self):
        """test string declaration and assignment"""
        self.assertTrue(TestLexer.checkLexeme("""string str; str = "A string" ""","""string,str,;,str,=,A string,<EOF>""",182))
    def test_invalid_operator_double_sign(self):
        """test invalid ++ and --"""
        self.assertTrue(TestLexer.checkLexeme("""a++; b--""","""a,+,+,;,b,-,-,<EOF>""",183))
    def test_ASSIGN_expression(self):
        """test expression with ="""
        self.assertTrue(TestLexer.checkLexeme("""a=b=c""","""a,=,b,=,c,<EOF>""",184))
    def test_OR_expression(self):
        """test expresison with ||"""
        self.assertTrue(TestLexer.checkLexeme("""a||b||c""","""a,||,b,||,c,<EOF>""",185))
    def test_AND_expression(self):
        """test expression with &&"""
        self.assertTrue(TestLexer.checkLexeme("""a&&b&&c""","""a,&&,b,&&,c,<EOF>""",186))
    def test_EQUAL_UNEQUAL_expression(self):
        """test expression with == and !="""
        self.assertTrue(TestLexer.checkLexeme("""a==b!=c""","""a,==,b,!=,c,<EOF>""",187))
    def test_LT_LE_GT_GE_expression(self):
        """test expression with <, <=, >, and >="""
        self.assertTrue(TestLexer.checkLexeme("""a<b<=c>d>=e""","""a,<,b,<=,c,>,d,>=,e,<EOF>""",188))
    def test_ADD_SUB_expression(self):
        """test expression with + and -"""
        self.assertTrue(TestLexer.checkLexeme("""a+b-c""","""a,+,b,-,c,<EOF>""",189))
    def test_DIV_MUL_MOD_expression(self):
        """test expression with /, *, and %"""
        self.assertTrue(TestLexer.checkLexeme("""a/b*c%d""","""a,/,b,*,c,%,d,<EOF>""",190))
    def test_SUB_NOT_expression(self):
        """test expression with unary - and !"""
        self.assertTrue(TestLexer.checkLexeme("""-a;!b;""","""-,a,;,!,b,;,<EOF>""",191))
    def test_expression_with_array_operator(self):
        """test expression with array operator"""
        self.assertTrue(TestLexer.checkLexeme("""array[(2+3)*4]""","""array,[,(,2,+,3,),*,4,],<EOF>""",192))
    def test_array_index(self):
        """test array index"""
        self.assertTrue(TestLexer.checkLexeme("""a[b[c[2+3]]] = 1;""","""a,[,b,[,c,[,2,+,3,],],],=,1,;,<EOF>""",193))
    def test_init_float_variable(self):
        """test init float variable"""
        self.assertTrue(TestLexer.checkLexeme("""float a; a = 0.0;""","""float,a,;,a,=,0.0,;,<EOF>""",194))
    def test_init_boolean_variable(self):
        """test init boolean variable"""
        self.assertTrue(TestLexer.checkLexeme("""boolean flag; flag = true;""","""boolean,flag,;,flag,=,true,;,<EOF>""",195))
    def test_init_void_variable(self):
        """test init void variable"""
        self.assertTrue(TestLexer.checkLexeme("""void p;""","""void,p,;,<EOF>""",196))
    def test_void_function_declare(self):
        """test declare void function"""
        self.assertTrue(TestLexer.checkLexeme("""void printLine(string s) {
                                                     putString(s);
                                                 }""","""void,printLine,(,string,s,),{,putString,(,s,),;,},<EOF>""",197))
    def test_int_main_function(self):
        """test int main function"""
        self.assertTrue(TestLexer.checkLexeme("""int main(){}""","""int,main,(,),{,},<EOF>""",198))
    def test_void_main_function(self):
        """test if statement"""
        self.assertTrue(TestLexer.checkLexeme("""void main(){}""","""void,main,(,),{,},<EOF>""",199))
    def test_invalid_two_float(self):
        """test invalid two float"""
        self.assertTrue(TestLexer.checkLexeme("""12..34""","""12.,.34,<EOF>""",200))
