# 1652579 - Pham Ngoc Thoai
import unittest
from TestUtils import TestCodeGen
from AST import *

class CheckCodeGenSuite(unittest.TestCase):
    # Variable
    def test_global_int_var(self):
        input = """int i;
    void main(){
     i=0;
      putInt(i);
    }
                   """
        expect = "0"
        self.assertTrue(TestCodeGen.test(input, expect, 501))
