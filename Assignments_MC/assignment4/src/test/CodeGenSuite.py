# 1652579 - Pham Ngoc Thoai
import unittest
from TestUtils import TestCodeGen
from AST import *

class CheckCodeGenSuite(unittest.TestCase):
    # Variable
    def test_global_int_var(self):
        input = """int a;
    void main(){
      putInt(a);
    }
                   """
        expect = "0"
        self.assertTrue(TestCodeGen.test(input, expect, 501))
        
    def test_global_float_var(self):
        input = """float a;
    void main(){
      a = 1.2;
      putFloat(a);
    }
                   """
        expect = "1.2"
        self.assertTrue(TestCodeGen.test(input, expect, 502))

    def test_global_string_var(self):
        input = """string a;
    void main(){
      a = "hello";
      putString(a);
    }
                   """
        expect = "hello"
        self.assertTrue(TestCodeGen.test(input, expect, 503))

    def test_global_boolean_var(self):
        input = """boolean a;
    void main(){
      a = true;
      putBool(a);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 504))

    def test_global_array_int_var(self):
        input = """int a[3];
    void main(){
      a[0] = 1;
      putIntLn(a[0]);
    }
                   """
        expect = "1\n"
        self.assertTrue(TestCodeGen.test(input, expect, 505))

    def test_global_array_float_var(self):
        input = """float a[3];
    void main(){
      a[0] = 1.2;
      putFloatLn(a[0]);
    }
                   """
        expect = "1.2\n"
        self.assertTrue(TestCodeGen.test(input, expect, 506))

    def test_global_array_string_var(self):
        input = """string a[3];
    void main(){
      a[0] = "hello";
      putStringLn(a[0]);
    }
                   """
        expect = "hello\n"
        self.assertTrue(TestCodeGen.test(input, expect, 507))

    def test_global_array_boolean_var(self):
        input = """boolean a[3];
    void main(){
      a[0] = true;
      putBoolLn(a[0]);
    }
                   """
        expect = "true\n"
        self.assertTrue(TestCodeGen.test(input, expect, 508))
        
    def test_local_int_var(self):
        input = """void main(){
      int a;
      a = 1;
      putInt(a);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 509))

    def test_local_float_var(self):
        input = """void main(){
      float a;
      a = 1.2;
      putFloat(a);
    }
                   """
        expect = "1.2"
        self.assertTrue(TestCodeGen.test(input, expect, 510))

    def test_local_string_var(self):
        input = """void main(){
      string a; 
      a = "hello";
      putString(a);
    }
                   """
        expect = "hello"
        self.assertTrue(TestCodeGen.test(input, expect, 511))

    def test_local_boolean_var(self):
        input = """void main(){
      boolean a;
      a = true;
      putBool(a);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 512))
        
    def test_local_array_int_var(self):
        input = """void main(){
      int a[3]; 
      a[0] = 1;
      putIntLn(a[0]);
    }
                   """
        expect = "1\n"
        self.assertTrue(TestCodeGen.test(input, expect, 513))

    def test_local_array_float_var(self):
        input = """void main(){
      float a[3];
      a[0] = 1.2;
      putFloatLn(a[0]);
    }
                   """
        expect = "1.2\n"
        self.assertTrue(TestCodeGen.test(input, expect, 514))

    def test_local_array_string_var(self):
        input = """void main(){
      string a[3];
      a[0] = "hello";
      putStringLn(a[0]);
    }
                   """
        expect = "hello\n"
        self.assertTrue(TestCodeGen.test(input, expect, 515))

    def test_local_array_boolean_var(self):
        input = """void main(){
      boolean a[3];
      a[0] = true;
      putBoolLn(a[0]);
    }
                   """
        expect = "true\n"
        self.assertTrue(TestCodeGen.test(input, expect, 516))

    # Parameter
        
    def test_function_with_primitive_type_param(self):
        input = """void main(){
      foo(1, 2, "hello", false);
    }
    void foo(int a, float b, string c, boolean d){
      putIntLn(a);
      putFloatLn(b);
      putStringLn(c);
      putBoolLn(d);
    }
                   """
        expect = "1\n2.0\nhello\nfalse\n"
        self.assertTrue(TestCodeGen.test(input, expect, 517))

    def test_function_with_array_type_param(self):
        input = """void main(){
      int a[3];
      foo(a);
      putIntLn(a[0]);      
    }
    void foo(int a[]){
      a[0] = 2;
    }
                   """
        expect = "2\n"
        self.assertTrue(TestCodeGen.test(input, expect, 518))

    # Statement
    # If statement

    def test_simple_if_stmt(self):
        input = """void main(){
      if(true) putStringLn("hello");
    }
                   """
        expect = "hello\n"
        self.assertTrue(TestCodeGen.test(input, expect, 519))

    def test_simple_if_else_stmt(self):
        input = """void main(){
      if(true) putIntLn(1);
      else putIntLn(2);
    }
                   """
        expect = "1\n"
        self.assertTrue(TestCodeGen.test(input, expect, 520))
        
    def test_true_expression_in_simple_if_else_stmt(self):
        input = """void main(){
      if(1>2) putIntLn(1);
      else putIntLn(2);
    }
                   """
        expect = "2\n"
        self.assertTrue(TestCodeGen.test(input, expect, 521))

    def test_false_expression_in_simple_if_else_stmt(self):
        input = """void main(){
      int a;
      float b;
      a = 10;
      b = 5.5;
      if(a<=b) putIntLn(1);
      else putIntLn(2);
    }
                   """
        expect = "2\n"
        self.assertTrue(TestCodeGen.test(input, expect, 522))

    def test_simple_for_stmt(self):
        input = """void main(){
      int i;
      for(i = 1; i <= 5; i = i + 1) putIntLn(i);
    }
                   """
        expect = "1\n2\n3\n4\n5\n"
        self.assertTrue(TestCodeGen.test(input, expect, 523))

    def test_if_stmt_in_simple_for_stmt(self):
        input = """void main(){
      int i;
      for(i = 1; i <= 10; i = i + 1) {
        if(i % 2 == 0) putInt(i);
      } 
    }
                   """
        expect = "246810"
        self.assertTrue(TestCodeGen.test(input, expect, 524))
        
    def test_nested_for_stmt(self):
        input = """void main(){
      int i, j;
      for(i = 1; i <= 3; i = i + 1) {
        for(j = 1; j <= 2; j = j + 1) {
          putInt(i);
          putString(",");
          putInt(j);
          putString(";");
        }
      } 
    }
                   """
        expect = "1,1;1,2;2,1;2,2;3,1;3,2;"
        self.assertTrue(TestCodeGen.test(input, expect, 525))

    def test_dowhile_stmt_in_for_stmt(self):
        input = """void main(){
      int i, j;
      for(i = 1; i <= 3; i = i + 1) {
        j = 1;
        do 
          putInt(i);
          putString(",");
          putInt(j);
          putString(";");
          j = j + 1;
        while (j<=2);
      } 
    }
                   """
        expect = "1,1;1,2;2,1;2,2;3,1;3,2;"
        self.assertTrue(TestCodeGen.test(input, expect, 526))

    # Dowhile Statement

    def test_simple_dowhile_stmt(self):
        input = """void main(){
      int i;
      i = 1;
      do {
        putInt(1);
        i = i + 1;
      }
      while(i<=5);
    }
                   """
        expect = "11111"
        self.assertTrue(TestCodeGen.test(input, expect, 527))

    def test_simple_dowhile_loop_only_once(self):
        input = """void main(){
      int i;
      i = 1;
      do
        putInt(1);
        i = i + 1;
      while(false);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 528))
        
    def test_nested_dowhile(self):
        input = """void main(){
      int i;
      i = 1;
      do
          do
            putInt(1);
            i = i + 1;
          while(i<=3);
          putFloat(1);
          i=i+2;
      while(i<=8);
    }
                   """
        expect = "1111.011.0"
        self.assertTrue(TestCodeGen.test(input, expect, 529))

    # Continue Statement

    def test_continue_stmt_in_for_stmt(self):
        input = """void main(){
      int i;
      for(i = 1; i <= 5; i = i + 1) {
        if(i==2) continue;
        putInt(i);
      }
    }
                   """
        expect = "1345"
        self.assertTrue(TestCodeGen.test(input, expect, 530))

    def test_continue_stmt_in_dowhile_stmt(self):
        input = """void main(){
      int i;
      i = 0;
      do {
        i = i + 1;
        if(i==3) continue;
        putInt(i);
      } while(i < 5);
    }
                   """
        expect = "1245"
        self.assertTrue(TestCodeGen.test(input, expect, 531))

    # Break Statement

    def test_break_stmt_in_for_stmt(self):
        input = """void main(){
      int i;
      for(i = 1; i <= 5; i = i + 1) {
        if(i==3) break;
        putInt(i);
      }
    }
                   """
        expect = "12"
        self.assertTrue(TestCodeGen.test(input, expect, 532))
        
    def test_break_stmt_in_dowhile_stmt(self):
        input = """void main(){
      int i;
      i = 0;
      do {
        i = i + 1;
        if(i==3) break;
        putInt(i);
      } while(i<5);
    }
                   """
        expect = "12"
        self.assertTrue(TestCodeGen.test(input, expect, 533))

    # Return Statement

    def test_simple_return(self):
        input = """void main(){
      putInt(12);
      return;
    }
                   """
        expect = "12"
        self.assertTrue(TestCodeGen.test(input, expect, 534))

    def test_return_int(self):
        input = """void main(){
      putInt(foo());
    }
    int foo() {
      return 1;
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 535))

    def test_return_float(self):
        input = """void main(){
      putFloat(foo());
    }
    float foo() {
      return 1.2;
    }
                   """
        expect = "1.2"
        self.assertTrue(TestCodeGen.test(input, expect, 536))
        
    def test_return_string(self):
        input = """void main(){
      putString(foo());
    }
    string foo() {
      return "hello";
    }
                   """
        expect = "hello"
        self.assertTrue(TestCodeGen.test(input, expect, 537))

    def test_return_boolean(self):
        input = """void main(){
      putBool(foo());
    }
    boolean foo() {
      return true;
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 538))

    def test_return_array(self):
        input = """
        int a[1];
        float b[1];
        void main(){
              a[0]=3;
              b[0]=4;
              putFloat(foo1()[0]);
              putInt(bar()[0]);
              putString(foo()[0]);
              putString(" ");
              putString(foo()[1]);
        }
        string[] foo() {
              string a[2];
              a[0] = "hello";
              a[1] = "world";
              return a;
        }

        int[] bar(){
            return a;
        }

        float[] foo1(){
            return b;
        }
                   """
        expect = "4.03hello world"
        self.assertTrue(TestCodeGen.test(input, expect, 539))

    def test_return_void_in_loop(self):
        input = """void main(){
      int i;
      for(i = 1; i <= 5; i = i + 1) {
        if(i==3) return;
        putInt(i);
      }
    }
                   """
        expect = "12"
        self.assertTrue(TestCodeGen.test(input, expect, 540))

    def test_return_float_in_loop(self):
        input = """void main(){
      putFloat(foo());
    }
    float foo(){
      for(1;true;1) return 24.01;
      return 0.0;
    }
                   """
        expect = "24.01"
        self.assertTrue(TestCodeGen.test(input, expect, 541))
        
    def test_return_in_if_stmt(self):
        input = """void main(){
      putInt(foo());
    }
    int foo(){
      if(true) return 1;
      return 0;
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 542))

    def test_return_in_dowhile_stmt(self):
        input = """void main(){
      putBool(foo());
    }
    boolean foo(){
      do
        return false;
      while(true);
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 543))

    def test_return_in_if_no_else_stmt(self):
        input = """void main(){
      putString(foo());
    }
    string foo(){
      if(true) return "hello";
      return "goodbye";
    }
                   """
        expect = "hello"
        self.assertTrue(TestCodeGen.test(input, expect, 544))

    def test_return_int_coerced_to_float(self):
        input = """void main(){
      putFloatLn(foo());
    }
    float foo(){
      return 1;
    }
                   """
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 545))

    # Expression
    # BinOp

    # [+ - * /]
        
    def test_intLit_add_intLit(self):
        input = """void main(){
                      putInt(1+2);
                    }
                   """
        expect = "3"
        self.assertTrue(TestCodeGen.test(input, expect, 546))

    def test_intLit_add_floatLit(self):
        input = """void main(){
                      putFloat(1 + 2.3);
                    }
                   """
        expect = "3.3"
        self.assertTrue(TestCodeGen.test(input, expect, 547))

    def test_floatLit_add_floatLit(self):
        input = """void main(){
                      putFloat(20.3 + 10.2);
                    }
                   """
        expect = "30.5"
        self.assertTrue(TestCodeGen.test(input, expect, 548))

    def test_intLit_subtract_intLit(self):
        input = """void main(){
      putInt(1-2);
    }
                   """
        expect = "-1"
        self.assertTrue(TestCodeGen.test(input, expect, 549))
        
    def test_floatVar_subtract_intLit(self):
        input = """void main(){
      float a;
      a = 2.3;
      putFloat(a - 1);
    }
                   """
        expect = "1.3"
        self.assertTrue(TestCodeGen.test(input, expect, 550))

    def test_intLit_add_floatLit_subtract_intLit(self):
        input = """void main(){
      putFloat(1 + 2.3 - 2);
    }
                   """
        expect = "1.3"
        self.assertTrue(TestCodeGen.test(input, expect, 551))

    def test_intLit_add_intLit_subtract_floatLit(self):
        input = """void main(){
      putFloat(1 - 2 + 3.4);
    }
                   """
        expect = "2.4"
        self.assertTrue(TestCodeGen.test(input, expect, 552))

    def test_intExp_add_floatExp(self):
        input = """void main(){
      putFloat((1 + 2) + (3.4 + 5));
    }
                   """
        expect = "11.4"
        self.assertTrue(TestCodeGen.test(input, expect, 553))
        
    def test_mul_op_with_add_op(self):
        input = """void main(){
      putFloat(1 * 2.3 + 4 * 5);
    }
                   """
        expect = "22.3"
        self.assertTrue(TestCodeGen.test(input, expect, 554))

    def test_mul_op_and_div_op_with_add_op(self):
        input = """void main(){
      putFloat((0.1 * 2.3) + (4 / 5));
    }
                   """
        expect = "0.23"
        self.assertTrue(TestCodeGen.test(input, expect, 555))

    # %

    def test_intLit_mod_intLit(self):
        input = """void main(){
      putInt(10 % 3);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 556))

    def test_intVar_mod_intExp(self):
        input = """void main(){
      int a;
      a = 10;
      putInt(a % (a % 3 + 2));
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 557))

    # [> < >= <=]
        
    def test_intLit_greater_than_intLit(self):
        input = """void main(){
      putBool(1 > 2);
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 558))

    def test_intLit_greater_than_or_equal_intLit(self):
        input = """void main(){
      putBool(2 >= 2);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 559))

    def test_intLit_less_than_intLit(self):
        input = """void main(){
      putBool(10 < 20.5);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 560))

    def test_intLit_less_than_or_equal_intLit(self):
        input = """void main(){
      float a;
      int b;
      a = 0.1;
      b = 3;
      putBool(a <= b);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 561))

     # [&& ||]
    
    def test_boolExp_and_boolExp(self):
        input = """void main(){
      putBool((1>2)&&(0.1<2));
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 562))

    def test_boolVar_and_boolVar(self):
        input = """void main(){
      boolean a,b;
      a = false;
      b = true;
      putBool(a&&b);
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 563))

    def test_boolExp_or_boolExp(self):
        input = """void main(){
                      putBool(1 > 2 || 0.1 < 2);
                    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 564))

    def test_boolVar_or_boolVar(self):
        input = """void main(){
      boolean a,b;
      a = false;
      b = true;
      putBool(a||b);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 565))
        
    def test_boolVar_or_boolLit(self):
        input = """void main(){
      boolean a,b;
      a = false;
      b = true;
      putBool(a || b && false);
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 566))

    def test_short_circuit_with_and_op(self):
        input = """void main(){
      int a;
      boolean b;
      a = 1;
      b = true && false && (a=2)==2;
      putInt(a);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 567))

    def test_short_circuit_with_and_op_with_zero_division(self):
        input = """void main(){
      int a;
      boolean b;
      a = 2;
      b = false && (a/0)==0;
      putBool(b);
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 568))

    def test_short_circuit_with_or_op(self):
        input = """void main(){
      int a;
      boolean b;
      a = 1;
      b = true || (a=2)==2;
      putInt(a);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 569))

    # [== !=]
        
    def test_intVar_equal_intExp(self):
        input = """void main(){
      int a;
      a = 100;
      putBool(a == 50 + 50);
    }
                   """
        expect = "true"
        self.assertTrue(TestCodeGen.test(input, expect, 570))

    def test_boolVar_equal_boolLit(self):
        input = """void main(){
      boolean a;
      a = true;
      putBool(a==false);
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 571))

    def test_intVar_not_equal_intLit(self):
        input = """void main(){
      int a;
      a = 100;
      putBoolLn(a != 50);
    }
                   """
        expect = "true\n"
        self.assertTrue(TestCodeGen.test(input, expect, 572))

    def test_boolVar_not_equal_boolLit(self):
        input = """void main(){
      boolean a;
      a = true;
      putBoolLn(a!=false);
    }
                   """
        expect = "true\n"
        self.assertTrue(TestCodeGen.test(input, expect, 573))

    # =
        
    def test_simple_var_assignment(self):
        input = """void main(){
      int a;
      a = 1;
      putIntLn(a);
    }
                   """
        expect = "1\n"
        self.assertTrue(TestCodeGen.test(input, expect, 574))

    def test_simple_var_assignment_in_expression(self):
        input = """void main(){
      int a;
      int b;
      b = (a=1)+1;
      putInt(b);
      putInt(a);
    }
                   """
        expect = "21"
        self.assertTrue(TestCodeGen.test(input, expect, 575))

    def test_float_var_assignment(self):
        input = """void main(){
      float a;
      a = 1.2 + 12;
      putFloat(a);
    }
                   """
        expect = "13.2"
        self.assertTrue(TestCodeGen.test(input, expect, 576))

    def test_coerced_assignment(self):
        input = """void main(){
      float a;
      a = 1;
      putFloatLn(a);
    }
                   """
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 577))
        
    def test_many_assignments(self):
        input = """void main(){
      int a,b;
      a = b = 1;
      putIntLn(a);
      putIntLn(b);
    }
                   """
        expect = "1\n1\n"
        self.assertTrue(TestCodeGen.test(input, expect, 578))

    def test_coerced_many_assignments(self):
        input = """void main(){
      float a;
      int b;
      a = b = 1;
      putFloat(a);
      putInt(b);
    }
                   """
        expect = "1.01"
        self.assertTrue(TestCodeGen.test(input, expect, 579))

    def test_assignment_in_bin_op(self):
        input = """void main(){
      int a;
      999 + (a = 5);
      putIntLn(a);
    }
                   """
        expect = "5\n"
        self.assertTrue(TestCodeGen.test(input, expect, 580))

    def test_array_assignment(self):
        input = """void main(){
      int b[3];
      b[0] = 1;
      putInt(b[0]);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 581))
        
    def test_array_assignment_in_index(self):
        input = """void main(){
      int b[3];
      b[0] = 0;
      b[1] = 1;
      b[b[b[0]=1]=2]=3;
      putInt(b[0]);
    }
                   """
        expect = "1"
        self.assertTrue(TestCodeGen.test(input, expect, 582))

    def test_coerced_array_assignment_with_intLit(self):
        input = """void main(){
      float b[3];
      b[0] = 1;
      putFloatLn(b[0]);
    }
                   """
        expect = "1.0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 583))

    def test_coerced_array_assignment_with_array(self):
        input = """void main(){
      float a[3];
      int b[3];
      a[0] = b[0] = 1;
      putFloatLn(a[0]+b[0]);
    }
                   """
        expect = "2.0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 584))

    def test_assignment_with_callExpr(self):
        input = """int a[1];
                    void main(){
                        putInt(foo()[0] = 1);
                        putInt(a[0]);
                   }

                   int[] foo(){
                        return a;
                   }
                   """
        expect = "11"
        self.assertTrue(TestCodeGen.test(input, expect, 585))

    # UnaryOp

    # -
        
    def test_simple_negative_op(self):
        input = """void main(){
      putInt(-1);
    }
                   """
        expect = "-1"
        self.assertTrue(TestCodeGen.test(input, expect, 586))

    def test_simple_unary_op_with_float_number(self):
        input = """void main(){
      putFloatLn(-1.2e3);
    }
                   """
        expect = "-1200.0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 587))

    # !

    def test_simple_not_op(self):
        input = """void main(){
      boolean a;
      putBool(!(0.1<2));
    }
                   """
        expect = "false"
        self.assertTrue(TestCodeGen.test(input, expect, 588))

    # CallExpr

    def test_coerced_param_in_callExpr(self):
        input = """void main(){
      putFloatLn(24);
    }
                   """
        expect = "24.0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 589))
        
    def test_callExpr_in_param(self):
        input = """void main(){
      putIntLn(foo(foo(2)));
    }
    int foo(int a) {
      return a - 1;
    }
                   """
        expect = "0\n"
        self.assertTrue(TestCodeGen.test(input, expect, 590))

    # ArrayCell

    def test_simple_arrayCell(self):
        input = """void main(){
      int a[3];
      a[0] = 1000; 
      putIntLn(a[0]);
    }
                   """
        expect = "1000\n"
        self.assertTrue(TestCodeGen.test(input, expect, 591))

    def test_simple_arrayCell_with_intExp_idx(self):
        input = """void main(){
      int a[3];
      a[0] = 1000; 
      putIntLn(a[10-10]);
    }
                   """
        expect = "1000\n"
        self.assertTrue(TestCodeGen.test(input, expect, 592))

    def test_simple_arrayCell_with_arrayCell_idx(self):
        input = """void main(){
      int a[3];
      a[0] = 1000; 
      a[1] = 0;
      putIntLn(a[a[1]]);
    }
                   """
        expect = "1000\n"
        self.assertTrue(TestCodeGen.test(input, expect, 593))
        
    def test_simple_callExpr_in_arrayCell(self):
        input = """void main(){ 
      putIntLn(foo()[0]);
    }
    int [] foo(){
      int a[3];
      a[0] = 1000; 
      return a;
    }
                   """
        expect = "1000\n"
        self.assertTrue(TestCodeGen.test(input, expect, 594))

    def test_simple_callExpr_in_arrayCell_with_intExp_idx(self):
        input = """void main(){
      putIntLn(foo()[10-10]);
    }
    int [] foo(){
      int a[3];
      a[0] = 1000; 
      return a;
    }  
                   """
        expect = "1000\n"
        self.assertTrue(TestCodeGen.test(input, expect, 595))

    def test_simple_callExpr_in_arrayCell_with_arrayCel_idx(self):
        input = """void main(){
      putIntLn(foo()[foo()[1]]);
    }
    int [] foo(){
      int a[3];
      a[0] = 1000; 
      a[1] = 0;
      return a;
    } 
                   """
        expect = "1000\n"
        self.assertTrue(TestCodeGen.test(input, expect, 596))

    # Program

    def test_recursive_function(self):
        input = """void main(){
      putIntLn(fact(6));
    }
    int fact(int n) {
      if(n==0) return 1;
      else return n*fact(n-1);
    }
                   """
        expect = "720\n"
        self.assertTrue(TestCodeGen.test(input, expect, 597))
        
    def test_sorting_function(self):
        input = """void main(){
      int a[5];
      a[0] = 9;
      a[1] = 6;
      a[2] = 8;
      a[3] = 2;
      a[4] = 5;
      sort(a,5);
    }
    void sort(int a[], int n) {
      int i,j;
      for(i = 0; i < n - 1; i = i +1)
        for(j = i + 1; j < n; j = j + 1)
          if(a[i] > a[j]) {
            a[i] = a[i] + a[j];
            a[j] = a[i] - a[j];
            a[i] = a[i] - a[j];
          }
      for (i = 0; i < n; i = i + 1) putInt(a[i]);
    }
                   """
        expect = "25689"
        self.assertTrue(TestCodeGen.test(input, expect, 598))

    def test_sum_function(self):
        input = """void main(){
      putIntLn(sum(6));
    }
    int sum(int n) {
      if(n==0) return 0;
      else return n + sum(n-1);
    }
                   """
        expect = "21\n"
        self.assertTrue(TestCodeGen.test(input, expect, 599))

    def test_check_leap_year_function(self):
        input = """void main(){
      is_leap_year(2018);
    }
    void is_leap_year(int year) {
      if(year % 400 == 0 || year % 100 != 0 && year % 4 == 0) {
        putInt(year);
        putString(" is a leap year");
      }
      else {
        putInt(year);
        putString(" is not a leap year");
      }
    }
                   """
        expect = "2018 is not a leap year"
        self.assertTrue(TestCodeGen.test(input, expect, 600))
