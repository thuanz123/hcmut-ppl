import unittest
from TestUtils import TestChecker
from AST import *

class CheckSuite(unittest.TestCase):
    
    # Global Scope
    def test__global_scope_of_vardecl_and_funcdecl(self):
        input = """int main(){a; foo(); return 1;} int a; void foo(){}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 401))

    # Redeclared Variable
    def test_no_redeclared_variable(self):
        input = """
    int a, b; void main(){{int a,b;} int a, b; {int b;}}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 402))

    def test_simple_redeclared_variable(self):
        input = """int a,b;int a; void main(){}"""
        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input, expect, 403))
        
    def test_simple_redeclared_variable_with_different_type(self):
        input = """int a,b; string a; void main(){}"""
        expect = "Redeclared Variable: a"
        self.assertTrue(TestChecker.test(input, expect, 404))

    def test_variable_redeclare_function(self):
        input = """int foo(){return 1;} void main(){foo();} int foo;"""
        expect = "Redeclared Variable: foo"
        self.assertTrue(TestChecker.test(input, expect, 405))
        
    def test_redeclared_variable_in_local_scope(self):
        input = """void main(){int b,b;}"""
        expect = "Redeclared Variable: b"
        self.assertTrue(TestChecker.test(input, expect, 406))

    def test_variable_redeclare_built_in_function(self):
        input = """void main(){} string getInt;"""
        expect = "Redeclared Variable: getInt"
        self.assertTrue(TestChecker.test(input, expect, 407))

    # Redeclared Function
        
    def test_no_redeclared_function(self):
        input = """int foo(){return 1;} int bar(){return 1;} void main(){foo();bar();}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 408))

    def test_simple_redeclared_function(self):
        input = """int foo(){return 1;} int foo(){return 1;} void main(){foo();}"""
        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 409))

    def test_simple_redeclared_function_with_different_type(self):
        input = """int foo(){return 1;} string foo(){return 1;} void main(){foo();}"""
        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 410))

    def test_function_redeclare_variable(self):
        input = """string foo; int foo(){return 1;} void main(){foo();}"""
        expect = "Redeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 411))
        
    def test_function_redeclared_built_in_function(self):
        input = """void getFloat(int a){return ;} void main(){}"""
        expect = "Redeclared Function: getFloat"
        self.assertTrue(TestChecker.test(input, expect, 412))

    # Redeclared Paramater

    def test_no_redeclared_parameter(self):
        input = """int a; void foo(int a){} void main(){foo(1);}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 413))
        
    def test_simple_redeclared_parameter(self):
        input = """void foo(int a, float a){} void main(){foo(1,1.1);}"""
        expect = "Redeclared Parameter: a"
        self.assertTrue(TestChecker.test(input, expect, 414))

    def test_more_complex_redeclared_parameter(self):
        input = """void foo(int b, string c, float b){} void main(){foo(1,"thuanz123",1.1);}"""
        expect = "Redeclared Parameter: b"
        self.assertTrue(TestChecker.test(input, expect, 415))

    # Redeclared Local Variable
        
    def test_no_redeclared_local_variable(self):
        input = """int c; void main(){float c;}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 416))
        
    def test_simple_redeclared_local_variable(self):
        input = """void main(){int c; float c;}"""
        expect = "Redeclared Variable: c"
        self.assertTrue(TestChecker.test(input, expect, 417))

    def test_local_variable_redeclare_parameter(self):
        input = """int c; void foo(int c){float c;} void main(){foo(1);}"""
        expect = "Redeclared Variable: c"
        self.assertTrue(TestChecker.test(input, expect, 418))

    def test_redelcared_local_variable_in_nested_block(self):
        input = """int main(){{int c; string c;} return 1;}"""
        expect = "Redeclared Variable: c"
        self.assertTrue(TestChecker.test(input, expect, 419))

    # Hide Declaration

    # Variable
    
    def test_parameter_hide_global_variable(self):
        input = """int a; 
    int foo(string a){
      a = 1;
      return 1;
    }
    
    void main(){foo("thuanz123");}"""
        expect = "Type Mismatch In Expression: BinaryOp(=,Id(a),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 420))

    def test_local_variable_hide_non_local_variable(self):
        input = """int a;
    int foo(){
      boolean a; 
      a = 1; 
      return 1;
    }
    void main(){foo();}"""
        expect = "Type Mismatch In Expression: BinaryOp(=,Id(a),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 421))
        
    def test_local_variable_hide_function(self):
        input = """void foo(){
      return;
    } 
    int main(){
      int foo;
      foo();
      return 1;
    }"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[])"
        self.assertTrue(TestChecker.test(input, expect, 422))

    # Function

    # Build In Function

    def test_built_in_function(self):
        input = """int main(){
      getInt();
      putInt(1);
      putIntLn(2);
      getFloat();
      putFloat(1);
      putFloatLn(1.2);
      putBool(true);
      putBoolLn(false);
      putString("thuan");
      putStringLn("z123");
      putLn();
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 423))
        
    def test_local_parameter_hide_built_in_function(self):
        input = """int foo(int getInt){getInt();} void main(){}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(getInt),[])"
        self.assertTrue(TestChecker.test(input, expect, 424))

    # Undeclared Identifier

    def test_no_undeclared_variable(self):
        input = """int a;
    boolean check; 
    int main(){
      a;
      if(check) 1; else 2;
      for(a;true;1){a;}
      do a; while true;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 425))
        
    def test_simple_undeclared_variable(self):
        input = """void main(){a;}"""
        expect = "Undeclared Identifier: a"
        self.assertTrue(TestChecker.test(input, expect, 426))

    def test_undeclared_variable_in_BinaryOp_expression(self):
        input = """int a; void main(){a+b;}"""
        expect = "Undeclared Identifier: b"
        self.assertTrue(TestChecker.test(input, expect, 427))
        
    def test_undeclared_variable_in_UnaryOp_expression(self):
        input = """void main(){-c;}"""
        expect = "Undeclared Identifier: c"
        self.assertTrue(TestChecker.test(input, expect, 428))

    def test_undeclared_variable_in_ArrayCell_expression(self):
        input = """void main(){d[1];}"""
        expect = "Undeclared Identifier: d"
        self.assertTrue(TestChecker.test(input, expect, 429))
        
    def test_undeclared_variable_in_funcall_expression(self):
        input = """void foo(int a){} void main(){foo(a);}"""
        expect = "Undeclared Identifier: a"
        self.assertTrue(TestChecker.test(input, expect, 430))

    # Undeclared Function

    def test_simple_undeclared_function(self):
        input = """int main(){
      foo();
      return 1;
    }"""
        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 431))
        
    def test_undeclared_function_in_BinaryOp_expression(self):
        input = """int main(){
      foo()+2;
      return 1;
    }"""
        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 432))

    def test_undeclared_function_in_UnaryOp_expression(self):
        input = """int main(){
      !foo();
      return 1;
    }"""
        expect = "Undeclared Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 433))

    # Type Mismatch In Expression
  
    # BinaryOp(+ - * /)
        
    def test_no_type_mismatch_in_math_operation(self):
        input = """int a;
    float b;
    int main(){
      a+241;
      39-b;
      b*a;
      3.9/24.1;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 434))

    def test_type_mismatch_in_math_operation_with_string_literal(self):
        input = """int main(){
      "1"+2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(+,StringLiteral(1),IntLiteral(2))"
        self.assertTrue(TestChecker.test(input, expect, 435))
        
    def test_type_mismatch_in_math_operation_with_boolean_type(self):
        input = """int main(){
      boolean a;
      a-1.2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(-,Id(a),FloatLiteral(1.2))"
        self.assertTrue(TestChecker.test(input, expect, 436))

    # BinaryOp(< > <= >= == !=)

    def test_no_type_mismatch_in_comparison_operation(self):
        input = """int a;
    float b;
    boolean c;
    int main(){
      a > 241;
      39 >= b;
      b < a;
      3.9 >= 24.1;
      2 == 1+1;
      a == 1;
      2 != a;
      c == true;
      false != true;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 437))
        
    def test_type_mismatch_in_comparison_operation_with_string_type_id(self):
        input = """string a;
    int main(){
      a>=1.2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(>=,Id(a),FloatLiteral(1.2))"
        self.assertTrue(TestChecker.test(input, expect, 438))

    def test_type_mismatch_in_comparison_operation_with_boolean_literal(self):
        input = """int main(){
      true<1.2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(<,BooleanLiteral(true),FloatLiteral(1.2))"
        self.assertTrue(TestChecker.test(input, expect, 439))
        
    def test_type_mismatch_in_comparison_operation_with_float_type_expression(self):
        input = """int main(){
      1.2==1 * 3;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(==,FloatLiteral(1.2),BinaryOp(*,IntLiteral(1),IntLiteral(3)))"
        self.assertTrue(TestChecker.test(input, expect, 440))

    # BinaryOp(=)

    def test_no_type_mismatch_in_assignment(self):
        input = """int a;
    float b;
    string c;
    boolean d;
    int e[3];
    int main(){
      a = 39;
      b = 24.1;
      b = a = e[1];
      c = "thuanz123";
      d = true;
      {
        int b;
        b = 39;
      }
      b = 1+2*1.2-1/8;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 441))
        
    def test_type_mismatch_in_assignment_with_array(self):
        input = """string a[3],b[3];
    int main(){
      a=b;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(=,Id(a),Id(b))"
        self.assertTrue(TestChecker.test(input, expect, 442))

    def test_simple_type_mismatch_in_assignment(self):
        input = """int a;
    int main(){
      a=1.2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(=,Id(a),FloatLiteral(1.2))"
        self.assertTrue(TestChecker.test(input, expect, 443))

    def test_type_mismatch_in_assignment_with_arraycell(self):
        input = """float a[3]; string b;
    int main(){
      a[0] = 1;
      a[1]=b;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(=,ArrayCell(Id(a),IntLiteral(1)),Id(b))"
        self.assertTrue(TestChecker.test(input, expect, 444))

    # BinaryOp(%)
        
    def test_no_type_mismatch_in_modulo(self):
        input = """int a;
    int main(){
      39%241;
      a%39;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 445))

    def test_type_mismatch_in_modulo_with_float_literal(self):
        input = """int main(){
      1.2%5;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(%,FloatLiteral(1.2),IntLiteral(5))"
        self.assertTrue(TestChecker.test(input, expect, 446))
        
    def test_type_mismatch_in_modulo_with_string_type_id(self):
        input = """int main(){string a;
      309%a;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(%,IntLiteral(309),Id(a))"
        self.assertTrue(TestChecker.test(input, expect, 447))

    def test_type_mismatch_in_modulo_with_boolean_type_expression(self):
        input = """int main(){
      int a;
      309%(a==1);
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(%,IntLiteral(309),BinaryOp(==,Id(a),IntLiteral(1)))"
        self.assertTrue(TestChecker.test(input, expect, 448))

    # BinaryOp(&& ||)
    
    def test_no_type_mismatch_in_logical_operator(self):
        input = """boolean a;
    int main(){
      a || true;
      true && false;
      true && false || a && 1==2;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 449))

    def test_type_mismatch_in_logical_operator_with_int_literal_and_boolean_literal(self):
        input = """int main(){
      309 || true;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(||,IntLiteral(309),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input, expect, 450))
        
    def test_type_mismatch_in_logical_operator_with_string_type_id_and_boolean_type_expression(self):
        input = """int main(){
      string a;
      a || 3 > 2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(||,Id(a),BinaryOp(>,IntLiteral(3),IntLiteral(2)))"
        self.assertTrue(TestChecker.test(input, expect, 451))

    def test_type_mismatch_in_logical_operator_with_boolean_type_id_and_float_type_expression(self):
        input = """int main(){
      boolean a;
      a || 3 + 2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: BinaryOp(||,Id(a),BinaryOp(+,IntLiteral(3),IntLiteral(2)))"
        self.assertTrue(TestChecker.test(input, expect, 452))

    # UnaryOp(-!)
        
    def test_no_type_mismatch_in_unary_op(self):
        input = """int a;
    float b;
    boolean c;
    int main(){
      -241;
      -3.9;
      -a;
      -b;
      -(3+1);
      -(3.2-1);
      !(true || false);
      !false;
      !c;
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 453))

    def test_type_mismatch_in_unary_minus_with_string_literal(self):
        input = """int main(){
      -"a";
      return 1;
    }"""
        expect = "Type Mismatch In Expression: UnaryOp(-,StringLiteral(a))"
        self.assertTrue(TestChecker.test(input, expect, 454))
        
    def test_type_mismatch_in_unary_minus_with_boolean_literal(self):
        input = """int main(){
      -true;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: UnaryOp(-,BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input, expect, 455))

    def test_type_mismatch_in_not_operator_with_float_literal(self):
        input = """int main(){
      !1.2;
      return 1;
    }"""
        expect = "Type Mismatch In Expression: UnaryOp(!,FloatLiteral(1.2))"
        self.assertTrue(TestChecker.test(input, expect, 456))
        
    def test_type_mismatch_in_not_operator_with_string_literal(self):
        input = """int main(){
      !"thuanz123";
      return 1;
    }"""
        expect = "Type Mismatch In Expression: UnaryOp(!,StringLiteral(thuanz123))"
        self.assertTrue(TestChecker.test(input, expect, 457))

    # ArrayCell
        
    def test_no_type_mismatch_in_arraycell(self):
        input = """int[] foo(){
      int a[3];
      return a;
    } 
    int a[3],b[3],c;
    int main(int d[]){
      a[1];
      a[c];
      a[b[1]];
      a[c+1];
      d[1];
      foo()[1];
      return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 458))

    def test_type_mismatch_in_arraycell_with_int_type_id(self):
        input = """int a;
    int main(){
      a[1];
      return 1;
    }"""
        expect = "Type Mismatch In Expression: ArrayCell(Id(a),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 459))
        
    def test_type_mismatch_in_arraycell_with_float_type_index(self):
        input = """float i;
    int a[3];
    int main(){
      a[i];
      return 1;
    }"""
        expect = "Type Mismatch In Expression: ArrayCell(Id(a),Id(i))"
        self.assertTrue(TestChecker.test(input, expect, 460))
        
    def test_type_mismatch_in_arraycell_with_boolean_literal(self):
        input = """int[] foo(){
      int a[3]; 
      return a;
    } 
    int main(){
      foo()[true];
      return 1;
    }"""
        expect = "Type Mismatch In Expression: ArrayCell(CallExpr(Id(foo),[]),BooleanLiteral(true))"
        self.assertTrue(TestChecker.test(input, expect, 461))

    # Call Expression

    def test_no_type_mismatch_in_call_expression(self):
        input = """void foo(int a, float b, string c, boolean d){
      int e[2];
      bar(e);
      return;
    }
    void bar(int a[]){   
      bar(a);
      return;
    } 
    void main(){
      string a;
      foo(1,1.2,"thuanz123",true);
      return;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 462))
        
    def test_type_mismatch_in_call_expression_with_unequal_param_number(self):
        input = """void foo(int a){return;} void main(){foo();return;}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[])"
        self.assertTrue(TestChecker.test(input, expect, 463))

    def test_simple_type_mismatch_in_call_expression_with_float_type_actual_param_and_int_type_formal_param(self):
        input = """void foo(int a){return;}  void main(){foo(1.2);return;}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[FloatLiteral(1.2)])"
        self.assertTrue(TestChecker.test(input, expect, 464))
        
    def test_type_mismatch_in_call_expression_with_boolean_type_actual_param_and_float_with_string_type_formal_params(self):
        input = """void foo(float a, string b){return;}  void main(){foo(true);return;}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[BooleanLiteral(true)])"
        self.assertTrue(TestChecker.test(input, expect, 465))

    def test_type_mismatch_in_call_expression_with_int_type_actual_param_and_boolean_type_formal_param(self):
        input = """void foo(float a, boolean b, string c){return;}  void main(){foo(1.3, 1, "thuanz123");return;}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[FloatLiteral(1.3),IntLiteral(1),StringLiteral(thuanz123)])"
        self.assertTrue(TestChecker.test(input, expect, 466))
        
    def test_type_mismatch_in_call_expression_with_array_int_type_actual_param_and_array_float_type_formal_param(self):
        input = """void foo(float a[]){return;}  void main(){int a[3]; foo(a);return;}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[Id(a)])"
        self.assertTrue(TestChecker.test(input, expect, 467))
    
    def test_type_mismatch_in_call_expression_with_array_float_type_actual_param_and_array_int_type_formal_param(self):
        input = """void foo(int a[]){return;}  void main(float b[]){foo(b);return;}"""
        expect = "Type Mismatch In Expression: CallExpr(Id(foo),[Id(b)])"
        self.assertTrue(TestChecker.test(input, expect, 468))

    # Type Mismatch In Statement
    # If Statement
    
    def test_no_type_mismatch_in_if_stmt(self):
        input = """int main(){if(true) 1;return 1;}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 469))

    def test_type_mismatch_in_if_stmt(self):
        input = """int main(){if(1) 1;return 1;}"""
        expect = "Type Mismatch In Statement: If(IntLiteral(1),IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 470))

    # For Statement
        
    def test_no_type_mismatch_in_for_stmt(self):
        input = """int main(){for(1;true;1)1; return 1;}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 471))

    def test_type_mismatch_in_first_expression_of_for_stmt(self):
        input = """int main(){for("a";true;1)1;return 1;}"""
        expect = "Type Mismatch In Statement: For(StringLiteral(a);BooleanLiteral(true);IntLiteral(1);IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 472))
        
    def test_type_mismatch_in_second_expression_of_for_stmt(self):
        input = """int main(){for(1;1;1)1;return 1;}"""
        expect = "Type Mismatch In Statement: For(IntLiteral(1);IntLiteral(1);IntLiteral(1);IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 473))

    def test_type_mismatch_in_third_expression_of_for_stmt(self):
        input = """int main(){for(1;false;1.2)1;return 1;}"""
        expect = "Type Mismatch In Statement: For(IntLiteral(1);BooleanLiteral(false);FloatLiteral(1.2);IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 474))

    # Dowhile Statement
        
    def test_no_type_mismatch_in_dowhile_stmt(self):
        input = """int main(){do 1; while true;return 1;}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 475))

    def test_type_mismatch_in_dowhile_stmt(self):
        input = """int main(){do 1; while 1;return 1;}"""
        expect = "Type Mismatch In Statement: Dowhile([IntLiteral(1)],IntLiteral(1))"
        self.assertTrue(TestChecker.test(input, expect, 476))

    # Return Statement
        
    def test_no_type_mismatch_in_return_stmt(self):
        input = """void main(){
      float a[1];
      foo();
      foo1();
      bar("thuanz123");
      bar1(a);
      return ;
    }
    int foo(){
      return 1;
    }
    string bar(string a){
      return a;
    }
    boolean[] foo1(){
      boolean a[3];
      return a;
    }
    float[] bar1(float a[]){
      {
        return a;
      }
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 477))

    def test_type_mismatch_in_return_stmt_of_array_float_type_function_with_array_int_type_expression(self):
        input = """float[] main(int a[]){ return a;}"""
        expect = "Type Mismatch In Statement: Return(Id(a))"
        self.assertTrue(TestChecker.test(input, expect, 478))
        
    def test_type_mismatch_in_return_stmt_of_void_type_function_with_non_empty_void_type_expression(self):
        input = """void main(){return main();}"""
        expect = "Type Mismatch In Statement: Return(CallExpr(Id(main),[]))"
        self.assertTrue(TestChecker.test(input, expect, 479))

    def test_type_mismatch_in_return_stmt_of_non_void_type_function_with_empty_stmt(self):
        input = """int main(){return;}"""
        expect = "Type Mismatch In Statement: Return()"
        self.assertTrue(TestChecker.test(input, expect, 480))

    # Function Not Return

    # Simple No Return Statement
        
    def test_simple_function_not_return(self):
        input = """int main(){}"""
        expect = "Function main Not Return "
        self.assertTrue(TestChecker.test(input, expect, 481))

    # Return Statement In If Statement

    def test_no_function_not_return_with_if_stmt(self):
        input = """int main(){foo(); if(true) 1; return 1;} int foo() {if(true) return 1; return 1;}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 482))
        
    def test_function_not_return_in_if_no_else_stmt(self):
        input = """int main(){if(true) return 1;}"""
        expect = "Function main Not Return "
        self.assertTrue(TestChecker.test(input, expect, 483))

    def test_function_not_return_in_if_else_stmt_where_else_has_return(self):
        input = """int main(){if(true) 1; else return 1;}"""
        expect = "Function main Not Return "
        self.assertTrue(TestChecker.test(input, expect, 484))
        
    def test_function_not_return_in_if_else_stmt_where_else_has_no_return(self):
        input = """int main(){if(true) 1; else 1;}"""
        expect = "Function main Not Return "
        self.assertTrue(TestChecker.test(input, expect, 485))

    # Return Statement In For Statement

    def test_no_function_not_return_with_for_stmt(self):
        input = """int main(){
      for(1;true;1) return 1; return 1;
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 486))

    def test_function_not_return_with_for_stmt(self):
        input = """int main(){
      for(1;true;1) return 1;
    }"""
        expect = "Function main Not Return "
        self.assertTrue(TestChecker.test(input, expect, 487))

    # Return Statement In DoWhile Statement

    def test_no_function_not_return_with_dowhile_stmt(self):
        input = """int main(){
      do return 1; while(true);
    }"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 488))
        
    def test_function_not_return_with_dowhile_stmt(self):
        input = """int main(){
      do 1; while(true);
    }"""
        expect = "Function main Not Return "
        self.assertTrue(TestChecker.test(input, expect, 489))

    # Break/Continue Not In Loop

    def test_break_or_continue_in_loop(self):
        input = """void main(){
                      for(1;true;1) {
                           {
                           break;
                           }
                      }
                      
                      for(1;true;1) {
                           {
                           continue;
                           }
                      }

                      do
                          if(true) {break;}
                       while(true);

                       do
                          if(true) continue; else 1;
                       while(true);

                       for(1;true;1)
                           break;

                       for(1;true;1)
                           continue;

                       do
                          break;
                       while(true);

                       do
                          continue;
                       while(true);

                       return;}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 490))
        
    def test_break_not_in_loop(self):
        input = """void main(){
      if(true)
          break; 
      return;
    }"""
        expect = "Break Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 491))
        
    def test_continue_not_in_loop(self):
        input = """void main(){
      continue;
      return;
    }"""
        expect = "Continue Not In Loop"
        self.assertTrue(TestChecker.test(input, expect, 492))

    # No Entry Point

    def test_no_entry_point(self):
        input = """void foo(){}"""
        expect = "No Entry Point"
        self.assertTrue(TestChecker.test(input, expect, 493))

    def test_no_entry_point_even_with_main_var(self):
        input = """int main;"""
        expect = "No Entry Point"
        self.assertTrue(TestChecker.test(input, expect, 494))

    # Unreachable Function

    def test_no_unreachable_function(self):
        input = """int main() {return 1;} void foo(){bar();} void bar(){foo();}"""
        expect = ""
        self.assertTrue(TestChecker.test(input, expect, 495))
        
    def test_simple_unreachable_function(self):
        input = """int main() {return 1;} void foo(){}"""
        expect = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 496))

    def test_unreachable_function_when_invoked_in_the_same_function(self):
        input = """int main() {return 1;} void foo(){foo();}"""
        expect = "Unreachable Function: foo"
        self.assertTrue(TestChecker.test(input, expect, 497))

    # Not Left Value

    def test_not_left_value_with_funcall_in_lhs(self):
        input = """int main() {int a[5]; bar(a)[2] = 1; foo() = 1; return 1;} void foo(){} int[] bar(int a[]) {return a;}"""
        expect = "Not Left Value: CallExpr(Id(foo),[])"
        self.assertTrue(TestChecker.test(input, expect, 498))

    def test_not_left_value_with_binary_op_in_lhs(self):
        input = """int main() {int a; a = a+2 = 1; return 1;}"""
        expect = "Not Left Value: BinaryOp(+,Id(a),IntLiteral(2))"
        self.assertTrue(TestChecker.test(input, expect, 499))

    def test_not_left_value_with_unary_op_in_lhs(self):
        input = """int main() {int a; -a = a = 1; return 1;}"""
        expect = "Not Left Value: UnaryOp(-,Id(a))"
        self.assertTrue(TestChecker.test(input, expect, 500))
  
