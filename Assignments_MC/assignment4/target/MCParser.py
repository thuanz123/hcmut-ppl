# Generated from main/mc/parser/MC.g4 by ANTLR 4.7.2
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\63")
        buf.write("\u012e\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23\t\23")
        buf.write("\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31")
        buf.write("\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36")
        buf.write("\4\37\t\37\4 \t \4!\t!\4\"\t\"\3\2\7\2F\n\2\f\2\16\2I")
        buf.write("\13\2\3\2\3\2\3\3\3\3\5\3O\n\3\3\4\3\4\3\4\3\4\7\4U\n")
        buf.write("\4\f\4\16\4X\13\4\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\3\6")
        buf.write("\5\6c\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\5\b")
        buf.write("o\n\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\7\nx\n\n\f\n\16\n{\13")
        buf.write("\n\5\n}\n\n\3\13\3\13\3\13\3\13\5\13\u0083\n\13\3\f\3")
        buf.write("\f\3\f\3\f\3\r\3\r\7\r\u008b\n\r\f\r\16\r\u008e\13\r\3")
        buf.write("\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u0098\n\16")
        buf.write("\3\17\3\17\3\17\3\17\3\17\3\17\3\17\5\17\u00a1\n\17\3")
        buf.write("\20\3\20\6\20\u00a5\n\20\r\20\16\20\u00a6\3\20\3\20\3")
        buf.write("\20\3\20\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\24\3\24\5\24\u00bf")
        buf.write("\n\24\3\24\3\24\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26")
        buf.write("\5\26\u00cb\n\26\3\27\3\27\3\27\3\27\3\27\3\27\7\27\u00d3")
        buf.write("\n\27\f\27\16\27\u00d6\13\27\3\30\3\30\3\30\3\30\3\30")
        buf.write("\3\30\7\30\u00de\n\30\f\30\16\30\u00e1\13\30\3\31\3\31")
        buf.write("\3\31\3\31\3\31\5\31\u00e8\n\31\3\32\3\32\3\32\3\32\3")
        buf.write("\32\5\32\u00ef\n\32\3\33\3\33\3\33\3\33\3\33\3\33\7\33")
        buf.write("\u00f7\n\33\f\33\16\33\u00fa\13\33\3\34\3\34\3\34\3\34")
        buf.write("\3\34\3\34\7\34\u0102\n\34\f\34\16\34\u0105\13\34\3\35")
        buf.write("\3\35\3\35\5\35\u010a\n\35\3\36\3\36\3\36\3\36\3\36\3")
        buf.write("\36\5\36\u0112\n\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37")
        buf.write("\5\37\u011b\n\37\3 \3 \3!\3!\3!\3!\3!\3\"\3\"\3\"\7\"")
        buf.write("\u0127\n\"\f\"\16\"\u012a\13\"\5\"\u012c\n\"\3\"\2\6,")
        buf.write(".\64\66#\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(")
        buf.write("*,.\60\62\64\668:<>@B\2\t\3\2\7\n\3\2\35\36\4\2\37 \"")
        buf.write("#\3\2\26\27\3\2\30\32\4\2\27\27!!\3\2\3\6\2\u012f\2G\3")
        buf.write("\2\2\2\4N\3\2\2\2\6P\3\2\2\2\b[\3\2\2\2\nb\3\2\2\2\fd")
        buf.write("\3\2\2\2\16n\3\2\2\2\20p\3\2\2\2\22|\3\2\2\2\24~\3\2\2")
        buf.write("\2\26\u0084\3\2\2\2\30\u008c\3\2\2\2\32\u0097\3\2\2\2")
        buf.write("\34\u0099\3\2\2\2\36\u00a2\3\2\2\2 \u00ac\3\2\2\2\"\u00b6")
        buf.write("\3\2\2\2$\u00b9\3\2\2\2&\u00bc\3\2\2\2(\u00c2\3\2\2\2")
        buf.write("*\u00ca\3\2\2\2,\u00cc\3\2\2\2.\u00d7\3\2\2\2\60\u00e7")
        buf.write("\3\2\2\2\62\u00ee\3\2\2\2\64\u00f0\3\2\2\2\66\u00fb\3")
        buf.write("\2\2\28\u0109\3\2\2\2:\u0111\3\2\2\2<\u011a\3\2\2\2>\u011c")
        buf.write("\3\2\2\2@\u011e\3\2\2\2B\u012b\3\2\2\2DF\5\4\3\2ED\3\2")
        buf.write("\2\2FI\3\2\2\2GE\3\2\2\2GH\3\2\2\2HJ\3\2\2\2IG\3\2\2\2")
        buf.write("JK\7\2\2\3K\3\3\2\2\2LO\5\6\4\2MO\5\f\7\2NL\3\2\2\2NM")
        buf.write("\3\2\2\2O\5\3\2\2\2PQ\5\b\5\2QV\5\n\6\2RS\7,\2\2SU\5\n")
        buf.write("\6\2TR\3\2\2\2UX\3\2\2\2VT\3\2\2\2VW\3\2\2\2WY\3\2\2\2")
        buf.write("XV\3\2\2\2YZ\7+\2\2Z\7\3\2\2\2[\\\t\2\2\2\\\t\3\2\2\2")
        buf.write("]c\7-\2\2^_\7-\2\2_`\7%\2\2`a\7\3\2\2ac\7&\2\2b]\3\2\2")
        buf.write("\2b^\3\2\2\2c\13\3\2\2\2de\5\16\b\2ef\7-\2\2fg\7\'\2\2")
        buf.write("gh\5\22\n\2hi\7(\2\2ij\5\26\f\2j\r\3\2\2\2ko\5\b\5\2l")
        buf.write("o\5\20\t\2mo\7\13\2\2nk\3\2\2\2nl\3\2\2\2nm\3\2\2\2o\17")
        buf.write("\3\2\2\2pq\5\b\5\2qr\7%\2\2rs\7&\2\2s\21\3\2\2\2ty\5\24")
        buf.write("\13\2uv\7,\2\2vx\5\24\13\2wu\3\2\2\2x{\3\2\2\2yw\3\2\2")
        buf.write("\2yz\3\2\2\2z}\3\2\2\2{y\3\2\2\2|t\3\2\2\2|}\3\2\2\2}")
        buf.write("\23\3\2\2\2~\177\5\b\5\2\177\u0082\7-\2\2\u0080\u0081")
        buf.write("\7%\2\2\u0081\u0083\7&\2\2\u0082\u0080\3\2\2\2\u0082\u0083")
        buf.write("\3\2\2\2\u0083\25\3\2\2\2\u0084\u0085\7)\2\2\u0085\u0086")
        buf.write("\5\30\r\2\u0086\u0087\7*\2\2\u0087\27\3\2\2\2\u0088\u008b")
        buf.write("\5\6\4\2\u0089\u008b\5\32\16\2\u008a\u0088\3\2\2\2\u008a")
        buf.write("\u0089\3\2\2\2\u008b\u008e\3\2\2\2\u008c\u008a\3\2\2\2")
        buf.write("\u008c\u008d\3\2\2\2\u008d\31\3\2\2\2\u008e\u008c\3\2")
        buf.write("\2\2\u008f\u0098\5\34\17\2\u0090\u0098\5\36\20\2\u0091")
        buf.write("\u0098\5 \21\2\u0092\u0098\5\"\22\2\u0093\u0098\5$\23")
        buf.write("\2\u0094\u0098\5&\24\2\u0095\u0098\5(\25\2\u0096\u0098")
        buf.write("\5\26\f\2\u0097\u008f\3\2\2\2\u0097\u0090\3\2\2\2\u0097")
        buf.write("\u0091\3\2\2\2\u0097\u0092\3\2\2\2\u0097\u0093\3\2\2\2")
        buf.write("\u0097\u0094\3\2\2\2\u0097\u0095\3\2\2\2\u0097\u0096\3")
        buf.write("\2\2\2\u0098\33\3\2\2\2\u0099\u009a\7\f\2\2\u009a\u009b")
        buf.write("\7\'\2\2\u009b\u009c\5*\26\2\u009c\u009d\7(\2\2\u009d")
        buf.write("\u00a0\5\32\16\2\u009e\u009f\7\r\2\2\u009f\u00a1\5\32")
        buf.write("\16\2\u00a0\u009e\3\2\2\2\u00a0\u00a1\3\2\2\2\u00a1\35")
        buf.write("\3\2\2\2\u00a2\u00a4\7\17\2\2\u00a3\u00a5\5\32\16\2\u00a4")
        buf.write("\u00a3\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a4\3\2\2\2")
        buf.write("\u00a6\u00a7\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00a9\7")
        buf.write("\20\2\2\u00a9\u00aa\5*\26\2\u00aa\u00ab\7+\2\2\u00ab\37")
        buf.write("\3\2\2\2\u00ac\u00ad\7\16\2\2\u00ad\u00ae\7\'\2\2\u00ae")
        buf.write("\u00af\5*\26\2\u00af\u00b0\7+\2\2\u00b0\u00b1\5*\26\2")
        buf.write("\u00b1\u00b2\7+\2\2\u00b2\u00b3\5*\26\2\u00b3\u00b4\7")
        buf.write("(\2\2\u00b4\u00b5\5\32\16\2\u00b5!\3\2\2\2\u00b6\u00b7")
        buf.write("\7\21\2\2\u00b7\u00b8\7+\2\2\u00b8#\3\2\2\2\u00b9\u00ba")
        buf.write("\7\22\2\2\u00ba\u00bb\7+\2\2\u00bb%\3\2\2\2\u00bc\u00be")
        buf.write("\7\23\2\2\u00bd\u00bf\5*\26\2\u00be\u00bd\3\2\2\2\u00be")
        buf.write("\u00bf\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c1\7+\2\2")
        buf.write("\u00c1\'\3\2\2\2\u00c2\u00c3\5*\26\2\u00c3\u00c4\7+\2")
        buf.write("\2\u00c4)\3\2\2\2\u00c5\u00c6\5,\27\2\u00c6\u00c7\7$\2")
        buf.write("\2\u00c7\u00c8\5*\26\2\u00c8\u00cb\3\2\2\2\u00c9\u00cb")
        buf.write("\5,\27\2\u00ca\u00c5\3\2\2\2\u00ca\u00c9\3\2\2\2\u00cb")
        buf.write("+\3\2\2\2\u00cc\u00cd\b\27\1\2\u00cd\u00ce\5.\30\2\u00ce")
        buf.write("\u00d4\3\2\2\2\u00cf\u00d0\f\4\2\2\u00d0\u00d1\7\34\2")
        buf.write("\2\u00d1\u00d3\5.\30\2\u00d2\u00cf\3\2\2\2\u00d3\u00d6")
        buf.write("\3\2\2\2\u00d4\u00d2\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5")
        buf.write("-\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d7\u00d8\b\30\1\2\u00d8")
        buf.write("\u00d9\5\60\31\2\u00d9\u00df\3\2\2\2\u00da\u00db\f\4\2")
        buf.write("\2\u00db\u00dc\7\33\2\2\u00dc\u00de\5\60\31\2\u00dd\u00da")
        buf.write("\3\2\2\2\u00de\u00e1\3\2\2\2\u00df\u00dd\3\2\2\2\u00df")
        buf.write("\u00e0\3\2\2\2\u00e0/\3\2\2\2\u00e1\u00df\3\2\2\2\u00e2")
        buf.write("\u00e3\5\62\32\2\u00e3\u00e4\t\3\2\2\u00e4\u00e5\5\62")
        buf.write("\32\2\u00e5\u00e8\3\2\2\2\u00e6\u00e8\5\62\32\2\u00e7")
        buf.write("\u00e2\3\2\2\2\u00e7\u00e6\3\2\2\2\u00e8\61\3\2\2\2\u00e9")
        buf.write("\u00ea\5\64\33\2\u00ea\u00eb\t\4\2\2\u00eb\u00ec\5\64")
        buf.write("\33\2\u00ec\u00ef\3\2\2\2\u00ed\u00ef\5\64\33\2\u00ee")
        buf.write("\u00e9\3\2\2\2\u00ee\u00ed\3\2\2\2\u00ef\63\3\2\2\2\u00f0")
        buf.write("\u00f1\b\33\1\2\u00f1\u00f2\5\66\34\2\u00f2\u00f8\3\2")
        buf.write("\2\2\u00f3\u00f4\f\4\2\2\u00f4\u00f5\t\5\2\2\u00f5\u00f7")
        buf.write("\5\66\34\2\u00f6\u00f3\3\2\2\2\u00f7\u00fa\3\2\2\2\u00f8")
        buf.write("\u00f6\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\65\3\2\2\2\u00fa")
        buf.write("\u00f8\3\2\2\2\u00fb\u00fc\b\34\1\2\u00fc\u00fd\58\35")
        buf.write("\2\u00fd\u0103\3\2\2\2\u00fe\u00ff\f\4\2\2\u00ff\u0100")
        buf.write("\t\6\2\2\u0100\u0102\58\35\2\u0101\u00fe\3\2\2\2\u0102")
        buf.write("\u0105\3\2\2\2\u0103\u0101\3\2\2\2\u0103\u0104\3\2\2\2")
        buf.write("\u0104\67\3\2\2\2\u0105\u0103\3\2\2\2\u0106\u0107\t\7")
        buf.write("\2\2\u0107\u010a\58\35\2\u0108\u010a\5:\36\2\u0109\u0106")
        buf.write("\3\2\2\2\u0109\u0108\3\2\2\2\u010a9\3\2\2\2\u010b\u010c")
        buf.write("\5<\37\2\u010c\u010d\7%\2\2\u010d\u010e\5*\26\2\u010e")
        buf.write("\u010f\7&\2\2\u010f\u0112\3\2\2\2\u0110\u0112\5<\37\2")
        buf.write("\u0111\u010b\3\2\2\2\u0111\u0110\3\2\2\2\u0112;\3\2\2")
        buf.write("\2\u0113\u0114\7\'\2\2\u0114\u0115\5*\26\2\u0115\u0116")
        buf.write("\7(\2\2\u0116\u011b\3\2\2\2\u0117\u011b\5> \2\u0118\u011b")
        buf.write("\5@!\2\u0119\u011b\7-\2\2\u011a\u0113\3\2\2\2\u011a\u0117")
        buf.write("\3\2\2\2\u011a\u0118\3\2\2\2\u011a\u0119\3\2\2\2\u011b")
        buf.write("=\3\2\2\2\u011c\u011d\t\b\2\2\u011d?\3\2\2\2\u011e\u011f")
        buf.write("\7-\2\2\u011f\u0120\7\'\2\2\u0120\u0121\5B\"\2\u0121\u0122")
        buf.write("\7(\2\2\u0122A\3\2\2\2\u0123\u0128\5*\26\2\u0124\u0125")
        buf.write("\7,\2\2\u0125\u0127\5*\26\2\u0126\u0124\3\2\2\2\u0127")
        buf.write("\u012a\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2")
        buf.write("\u0129\u012c\3\2\2\2\u012a\u0128\3\2\2\2\u012b\u0123\3")
        buf.write("\2\2\2\u012b\u012c\3\2\2\2\u012cC\3\2\2\2\34GNVbny|\u0082")
        buf.write("\u008a\u008c\u0097\u00a0\u00a6\u00be\u00ca\u00d4\u00df")
        buf.write("\u00e7\u00ee\u00f8\u0103\u0109\u0111\u011a\u0128\u012b")
        return buf.getvalue()


class MCParser ( Parser ):

    grammarFileName = "MC.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "'boolean'", "'int'", "'float'", "'string'", 
                     "'void'", "'if'", "'else'", "'for'", "'do'", "'while'", 
                     "'break'", "'continue'", "'return'", "'true'", "'false'", 
                     "'+'", "'-'", "'*'", "'/'", "'%'", "'&&'", "'||'", 
                     "'=='", "'!='", "'<='", "'>='", "'!'", "'<'", "'>'", 
                     "'='", "'['", "']'", "'('", "')'", "'{'", "'}'", "';'", 
                     "','" ]

    symbolicNames = [ "<INVALID>", "INTLIT", "FLOATLIT", "BOOLEANLIT", "STRINGLIT", 
                      "BOOLEANTYPE", "INTTYPE", "FLOATTYPE", "STRINGTYPE", 
                      "VOIDTYPE", "IF", "ELSE", "FOR", "DO", "WHILE", "BREAK", 
                      "CONTINUE", "RETURN", "TRUE", "FALSE", "ADD", "SUB", 
                      "MUL", "DIV", "MOD", "AND", "OR", "EQUAL", "NOTEQUAL", 
                      "LE", "GE", "NOT", "LT", "GT", "ASSIGN", "LSB", "RSB", 
                      "LB", "RB", "LP", "RP", "SEMI", "COMMA", "ID", "WS", 
                      "BLOCK_COMMENT", "Line_COMMENT", "ILLEGAL_ESCAPE", 
                      "UNCLOSE_STRING", "ERROR_CHAR" ]

    RULE_program = 0
    RULE_declaration = 1
    RULE_variableDecl = 2
    RULE_primitiveType = 3
    RULE_variable = 4
    RULE_functionDecl = 5
    RULE_functionType = 6
    RULE_arrayPointerType = 7
    RULE_parameterList = 8
    RULE_parameterDecl = 9
    RULE_blockStatement = 10
    RULE_blockBody = 11
    RULE_statement = 12
    RULE_ifStatement = 13
    RULE_doWhileStatement = 14
    RULE_forStatement = 15
    RULE_breakStatement = 16
    RULE_continueStatement = 17
    RULE_returnStatement = 18
    RULE_expressionStatement = 19
    RULE_expression = 20
    RULE_expression1 = 21
    RULE_expression2 = 22
    RULE_expression3 = 23
    RULE_expression4 = 24
    RULE_expression5 = 25
    RULE_expression6 = 26
    RULE_expression7 = 27
    RULE_expression8 = 28
    RULE_expression9 = 29
    RULE_literal = 30
    RULE_funcall = 31
    RULE_expressionList = 32

    ruleNames =  [ "program", "declaration", "variableDecl", "primitiveType", 
                   "variable", "functionDecl", "functionType", "arrayPointerType", 
                   "parameterList", "parameterDecl", "blockStatement", "blockBody", 
                   "statement", "ifStatement", "doWhileStatement", "forStatement", 
                   "breakStatement", "continueStatement", "returnStatement", 
                   "expressionStatement", "expression", "expression1", "expression2", 
                   "expression3", "expression4", "expression5", "expression6", 
                   "expression7", "expression8", "expression9", "literal", 
                   "funcall", "expressionList" ]

    EOF = Token.EOF
    INTLIT=1
    FLOATLIT=2
    BOOLEANLIT=3
    STRINGLIT=4
    BOOLEANTYPE=5
    INTTYPE=6
    FLOATTYPE=7
    STRINGTYPE=8
    VOIDTYPE=9
    IF=10
    ELSE=11
    FOR=12
    DO=13
    WHILE=14
    BREAK=15
    CONTINUE=16
    RETURN=17
    TRUE=18
    FALSE=19
    ADD=20
    SUB=21
    MUL=22
    DIV=23
    MOD=24
    AND=25
    OR=26
    EQUAL=27
    NOTEQUAL=28
    LE=29
    GE=30
    NOT=31
    LT=32
    GT=33
    ASSIGN=34
    LSB=35
    RSB=36
    LB=37
    RB=38
    LP=39
    RP=40
    SEMI=41
    COMMA=42
    ID=43
    WS=44
    BLOCK_COMMENT=45
    Line_COMMENT=46
    ILLEGAL_ESCAPE=47
    UNCLOSE_STRING=48
    ERROR_CHAR=49

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(MCParser.EOF, 0)

        def declaration(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.DeclarationContext)
            else:
                return self.getTypedRuleContext(MCParser.DeclarationContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = MCParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEANTYPE) | (1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.STRINGTYPE) | (1 << MCParser.VOIDTYPE))) != 0):
                self.state = 66
                self.declaration()
                self.state = 71
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 72
            self.match(MCParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variableDecl(self):
            return self.getTypedRuleContext(MCParser.VariableDeclContext,0)


        def functionDecl(self):
            return self.getTypedRuleContext(MCParser.FunctionDeclContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_declaration

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeclaration" ):
                return visitor.visitDeclaration(self)
            else:
                return visitor.visitChildren(self)




    def declaration(self):

        localctx = MCParser.DeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_declaration)
        try:
            self.state = 76
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,1,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 74
                self.variableDecl()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 75
                self.functionDecl()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariableDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitiveType(self):
            return self.getTypedRuleContext(MCParser.PrimitiveTypeContext,0)


        def variable(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VariableContext)
            else:
                return self.getTypedRuleContext(MCParser.VariableContext,i)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_variableDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVariableDecl" ):
                return visitor.visitVariableDecl(self)
            else:
                return visitor.visitChildren(self)




    def variableDecl(self):

        localctx = MCParser.VariableDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_variableDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 78
            self.primitiveType()
            self.state = 79
            self.variable()
            self.state = 84
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==MCParser.COMMA:
                self.state = 80
                self.match(MCParser.COMMA)
                self.state = 81
                self.variable()
                self.state = 86
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 87
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class PrimitiveTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BOOLEANTYPE(self):
            return self.getToken(MCParser.BOOLEANTYPE, 0)

        def INTTYPE(self):
            return self.getToken(MCParser.INTTYPE, 0)

        def FLOATTYPE(self):
            return self.getToken(MCParser.FLOATTYPE, 0)

        def STRINGTYPE(self):
            return self.getToken(MCParser.STRINGTYPE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_primitiveType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimitiveType" ):
                return visitor.visitPrimitiveType(self)
            else:
                return visitor.visitChildren(self)




    def primitiveType(self):

        localctx = MCParser.PrimitiveTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_primitiveType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 89
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEANTYPE) | (1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.STRINGTYPE))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class VariableContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def INTLIT(self):
            return self.getToken(MCParser.INTLIT, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_variable

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitVariable" ):
                return visitor.visitVariable(self)
            else:
                return visitor.visitChildren(self)




    def variable(self):

        localctx = MCParser.VariableContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_variable)
        try:
            self.state = 96
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 91
                self.match(MCParser.ID)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 92
                self.match(MCParser.ID)
                self.state = 93
                self.match(MCParser.LSB)
                self.state = 94
                self.match(MCParser.INTLIT)
                self.state = 95
                self.match(MCParser.RSB)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def functionType(self):
            return self.getTypedRuleContext(MCParser.FunctionTypeContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def parameterList(self):
            return self.getTypedRuleContext(MCParser.ParameterListContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def blockStatement(self):
            return self.getTypedRuleContext(MCParser.BlockStatementContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_functionDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionDecl" ):
                return visitor.visitFunctionDecl(self)
            else:
                return visitor.visitChildren(self)




    def functionDecl(self):

        localctx = MCParser.FunctionDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_functionDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 98
            self.functionType()
            self.state = 99
            self.match(MCParser.ID)
            self.state = 100
            self.match(MCParser.LB)
            self.state = 101
            self.parameterList()
            self.state = 102
            self.match(MCParser.RB)
            self.state = 103
            self.blockStatement()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FunctionTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitiveType(self):
            return self.getTypedRuleContext(MCParser.PrimitiveTypeContext,0)


        def arrayPointerType(self):
            return self.getTypedRuleContext(MCParser.ArrayPointerTypeContext,0)


        def VOIDTYPE(self):
            return self.getToken(MCParser.VOIDTYPE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_functionType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFunctionType" ):
                return visitor.visitFunctionType(self)
            else:
                return visitor.visitChildren(self)




    def functionType(self):

        localctx = MCParser.FunctionTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_functionType)
        try:
            self.state = 108
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 105
                self.primitiveType()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 106
                self.arrayPointerType()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 107
                self.match(MCParser.VOIDTYPE)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ArrayPointerTypeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitiveType(self):
            return self.getTypedRuleContext(MCParser.PrimitiveTypeContext,0)


        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_arrayPointerType

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitArrayPointerType" ):
                return visitor.visitArrayPointerType(self)
            else:
                return visitor.visitChildren(self)




    def arrayPointerType(self):

        localctx = MCParser.ArrayPointerTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_arrayPointerType)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 110
            self.primitiveType()
            self.state = 111
            self.match(MCParser.LSB)
            self.state = 112
            self.match(MCParser.RSB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def parameterDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ParameterDeclContext)
            else:
                return self.getTypedRuleContext(MCParser.ParameterDeclContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_parameterList

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameterList" ):
                return visitor.visitParameterList(self)
            else:
                return visitor.visitChildren(self)




    def parameterList(self):

        localctx = MCParser.ParameterListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_parameterList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 122
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.BOOLEANTYPE) | (1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.STRINGTYPE))) != 0):
                self.state = 114
                self.parameterDecl()
                self.state = 119
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.COMMA:
                    self.state = 115
                    self.match(MCParser.COMMA)
                    self.state = 116
                    self.parameterDecl()
                    self.state = 121
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ParameterDeclContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def primitiveType(self):
            return self.getTypedRuleContext(MCParser.PrimitiveTypeContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_parameterDecl

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParameterDecl" ):
                return visitor.visitParameterDecl(self)
            else:
                return visitor.visitChildren(self)




    def parameterDecl(self):

        localctx = MCParser.ParameterDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_parameterDecl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 124
            self.primitiveType()
            self.state = 125
            self.match(MCParser.ID)
            self.state = 128
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==MCParser.LSB:
                self.state = 126
                self.match(MCParser.LSB)
                self.state = 127
                self.match(MCParser.RSB)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LP(self):
            return self.getToken(MCParser.LP, 0)

        def blockBody(self):
            return self.getTypedRuleContext(MCParser.BlockBodyContext,0)


        def RP(self):
            return self.getToken(MCParser.RP, 0)

        def getRuleIndex(self):
            return MCParser.RULE_blockStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockStatement" ):
                return visitor.visitBlockStatement(self)
            else:
                return visitor.visitChildren(self)




    def blockStatement(self):

        localctx = MCParser.BlockStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_blockStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 130
            self.match(MCParser.LP)
            self.state = 131
            self.blockBody()
            self.state = 132
            self.match(MCParser.RP)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BlockBodyContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def variableDecl(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.VariableDeclContext)
            else:
                return self.getTypedRuleContext(MCParser.VariableDeclContext,i)


        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StatementContext)
            else:
                return self.getTypedRuleContext(MCParser.StatementContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_blockBody

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockBody" ):
                return visitor.visitBlockBody(self)
            else:
                return visitor.visitChildren(self)




    def blockBody(self):

        localctx = MCParser.BlockBodyContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_blockBody)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 138
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.BOOLEANTYPE) | (1 << MCParser.INTTYPE) | (1 << MCParser.FLOATTYPE) | (1 << MCParser.STRINGTYPE) | (1 << MCParser.IF) | (1 << MCParser.FOR) | (1 << MCParser.DO) | (1 << MCParser.BREAK) | (1 << MCParser.CONTINUE) | (1 << MCParser.RETURN) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.LP) | (1 << MCParser.ID))) != 0):
                self.state = 136
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [MCParser.BOOLEANTYPE, MCParser.INTTYPE, MCParser.FLOATTYPE, MCParser.STRINGTYPE]:
                    self.state = 134
                    self.variableDecl()
                    pass
                elif token in [MCParser.INTLIT, MCParser.FLOATLIT, MCParser.BOOLEANLIT, MCParser.STRINGLIT, MCParser.IF, MCParser.FOR, MCParser.DO, MCParser.BREAK, MCParser.CONTINUE, MCParser.RETURN, MCParser.SUB, MCParser.NOT, MCParser.LB, MCParser.LP, MCParser.ID]:
                    self.state = 135
                    self.statement()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 140
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ifStatement(self):
            return self.getTypedRuleContext(MCParser.IfStatementContext,0)


        def doWhileStatement(self):
            return self.getTypedRuleContext(MCParser.DoWhileStatementContext,0)


        def forStatement(self):
            return self.getTypedRuleContext(MCParser.ForStatementContext,0)


        def breakStatement(self):
            return self.getTypedRuleContext(MCParser.BreakStatementContext,0)


        def continueStatement(self):
            return self.getTypedRuleContext(MCParser.ContinueStatementContext,0)


        def returnStatement(self):
            return self.getTypedRuleContext(MCParser.ReturnStatementContext,0)


        def expressionStatement(self):
            return self.getTypedRuleContext(MCParser.ExpressionStatementContext,0)


        def blockStatement(self):
            return self.getTypedRuleContext(MCParser.BlockStatementContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_statement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStatement" ):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = MCParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_statement)
        try:
            self.state = 149
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.IF]:
                self.enterOuterAlt(localctx, 1)
                self.state = 141
                self.ifStatement()
                pass
            elif token in [MCParser.DO]:
                self.enterOuterAlt(localctx, 2)
                self.state = 142
                self.doWhileStatement()
                pass
            elif token in [MCParser.FOR]:
                self.enterOuterAlt(localctx, 3)
                self.state = 143
                self.forStatement()
                pass
            elif token in [MCParser.BREAK]:
                self.enterOuterAlt(localctx, 4)
                self.state = 144
                self.breakStatement()
                pass
            elif token in [MCParser.CONTINUE]:
                self.enterOuterAlt(localctx, 5)
                self.state = 145
                self.continueStatement()
                pass
            elif token in [MCParser.RETURN]:
                self.enterOuterAlt(localctx, 6)
                self.state = 146
                self.returnStatement()
                pass
            elif token in [MCParser.INTLIT, MCParser.FLOATLIT, MCParser.BOOLEANLIT, MCParser.STRINGLIT, MCParser.SUB, MCParser.NOT, MCParser.LB, MCParser.ID]:
                self.enterOuterAlt(localctx, 7)
                self.state = 147
                self.expressionStatement()
                pass
            elif token in [MCParser.LP]:
                self.enterOuterAlt(localctx, 8)
                self.state = 148
                self.blockStatement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class IfStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def IF(self):
            return self.getToken(MCParser.IF, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StatementContext)
            else:
                return self.getTypedRuleContext(MCParser.StatementContext,i)


        def ELSE(self):
            return self.getToken(MCParser.ELSE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_ifStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIfStatement" ):
                return visitor.visitIfStatement(self)
            else:
                return visitor.visitChildren(self)




    def ifStatement(self):

        localctx = MCParser.IfStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_ifStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 151
            self.match(MCParser.IF)
            self.state = 152
            self.match(MCParser.LB)
            self.state = 153
            self.expression()
            self.state = 154
            self.match(MCParser.RB)
            self.state = 155
            self.statement()
            self.state = 158
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,11,self._ctx)
            if la_ == 1:
                self.state = 156
                self.match(MCParser.ELSE)
                self.state = 157
                self.statement()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class DoWhileStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DO(self):
            return self.getToken(MCParser.DO, 0)

        def WHILE(self):
            return self.getToken(MCParser.WHILE, 0)

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.StatementContext)
            else:
                return self.getTypedRuleContext(MCParser.StatementContext,i)


        def getRuleIndex(self):
            return MCParser.RULE_doWhileStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDoWhileStatement" ):
                return visitor.visitDoWhileStatement(self)
            else:
                return visitor.visitChildren(self)




    def doWhileStatement(self):

        localctx = MCParser.DoWhileStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_doWhileStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 160
            self.match(MCParser.DO)
            self.state = 162 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 161
                self.statement()
                self.state = 164 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.IF) | (1 << MCParser.FOR) | (1 << MCParser.DO) | (1 << MCParser.BREAK) | (1 << MCParser.CONTINUE) | (1 << MCParser.RETURN) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.LP) | (1 << MCParser.ID))) != 0)):
                    break

            self.state = 166
            self.match(MCParser.WHILE)
            self.state = 167
            self.expression()
            self.state = 168
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ForStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def FOR(self):
            return self.getToken(MCParser.FOR, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(MCParser.ExpressionContext,i)


        def SEMI(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.SEMI)
            else:
                return self.getToken(MCParser.SEMI, i)

        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def statement(self):
            return self.getTypedRuleContext(MCParser.StatementContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_forStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitForStatement" ):
                return visitor.visitForStatement(self)
            else:
                return visitor.visitChildren(self)




    def forStatement(self):

        localctx = MCParser.ForStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_forStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 170
            self.match(MCParser.FOR)
            self.state = 171
            self.match(MCParser.LB)
            self.state = 172
            self.expression()
            self.state = 173
            self.match(MCParser.SEMI)
            self.state = 174
            self.expression()
            self.state = 175
            self.match(MCParser.SEMI)
            self.state = 176
            self.expression()
            self.state = 177
            self.match(MCParser.RB)
            self.state = 178
            self.statement()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class BreakStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def BREAK(self):
            return self.getToken(MCParser.BREAK, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def getRuleIndex(self):
            return MCParser.RULE_breakStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBreakStatement" ):
                return visitor.visitBreakStatement(self)
            else:
                return visitor.visitChildren(self)




    def breakStatement(self):

        localctx = MCParser.BreakStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_breakStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 180
            self.match(MCParser.BREAK)
            self.state = 181
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ContinueStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CONTINUE(self):
            return self.getToken(MCParser.CONTINUE, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def getRuleIndex(self):
            return MCParser.RULE_continueStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitContinueStatement" ):
                return visitor.visitContinueStatement(self)
            else:
                return visitor.visitChildren(self)




    def continueStatement(self):

        localctx = MCParser.ContinueStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_continueStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 183
            self.match(MCParser.CONTINUE)
            self.state = 184
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReturnStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def RETURN(self):
            return self.getToken(MCParser.RETURN, 0)

        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_returnStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReturnStatement" ):
                return visitor.visitReturnStatement(self)
            else:
                return visitor.visitChildren(self)




    def returnStatement(self):

        localctx = MCParser.ReturnStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_returnStatement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 186
            self.match(MCParser.RETURN)
            self.state = 188
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.ID))) != 0):
                self.state = 187
                self.expression()


            self.state = 190
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionStatementContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def SEMI(self):
            return self.getToken(MCParser.SEMI, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expressionStatement

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpressionStatement" ):
                return visitor.visitExpressionStatement(self)
            else:
                return visitor.visitChildren(self)




    def expressionStatement(self):

        localctx = MCParser.ExpressionStatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_expressionStatement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 192
            self.expression()
            self.state = 193
            self.match(MCParser.SEMI)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression1(self):
            return self.getTypedRuleContext(MCParser.Expression1Context,0)


        def ASSIGN(self):
            return self.getToken(MCParser.ASSIGN, 0)

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def getRuleIndex(self):
            return MCParser.RULE_expression

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression" ):
                return visitor.visitExpression(self)
            else:
                return visitor.visitChildren(self)




    def expression(self):

        localctx = MCParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_expression)
        try:
            self.state = 200
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 195
                self.expression1(0)
                self.state = 196
                self.match(MCParser.ASSIGN)
                self.state = 197
                self.expression()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 199
                self.expression1(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expression1Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression2(self):
            return self.getTypedRuleContext(MCParser.Expression2Context,0)


        def expression1(self):
            return self.getTypedRuleContext(MCParser.Expression1Context,0)


        def OR(self):
            return self.getToken(MCParser.OR, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression1

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression1" ):
                return visitor.visitExpression1(self)
            else:
                return visitor.visitChildren(self)



    def expression1(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Expression1Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 42
        self.enterRecursionRule(localctx, 42, self.RULE_expression1, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 203
            self.expression2(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 210
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,15,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Expression1Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expression1)
                    self.state = 205
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 206
                    self.match(MCParser.OR)
                    self.state = 207
                    self.expression2(0) 
                self.state = 212
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,15,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expression2Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression3(self):
            return self.getTypedRuleContext(MCParser.Expression3Context,0)


        def expression2(self):
            return self.getTypedRuleContext(MCParser.Expression2Context,0)


        def AND(self):
            return self.getToken(MCParser.AND, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression2

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression2" ):
                return visitor.visitExpression2(self)
            else:
                return visitor.visitChildren(self)



    def expression2(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Expression2Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 44
        self.enterRecursionRule(localctx, 44, self.RULE_expression2, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 214
            self.expression3()
            self._ctx.stop = self._input.LT(-1)
            self.state = 221
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,16,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Expression2Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expression2)
                    self.state = 216
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 217
                    self.match(MCParser.AND)
                    self.state = 218
                    self.expression3() 
                self.state = 223
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,16,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expression3Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression4(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Expression4Context)
            else:
                return self.getTypedRuleContext(MCParser.Expression4Context,i)


        def EQUAL(self):
            return self.getToken(MCParser.EQUAL, 0)

        def NOTEQUAL(self):
            return self.getToken(MCParser.NOTEQUAL, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression3

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression3" ):
                return visitor.visitExpression3(self)
            else:
                return visitor.visitChildren(self)




    def expression3(self):

        localctx = MCParser.Expression3Context(self, self._ctx, self.state)
        self.enterRule(localctx, 46, self.RULE_expression3)
        self._la = 0 # Token type
        try:
            self.state = 229
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 224
                self.expression4()
                self.state = 225
                _la = self._input.LA(1)
                if not(_la==MCParser.EQUAL or _la==MCParser.NOTEQUAL):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 226
                self.expression4()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 228
                self.expression4()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expression4Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression5(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.Expression5Context)
            else:
                return self.getTypedRuleContext(MCParser.Expression5Context,i)


        def LT(self):
            return self.getToken(MCParser.LT, 0)

        def LE(self):
            return self.getToken(MCParser.LE, 0)

        def GT(self):
            return self.getToken(MCParser.GT, 0)

        def GE(self):
            return self.getToken(MCParser.GE, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression4

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression4" ):
                return visitor.visitExpression4(self)
            else:
                return visitor.visitChildren(self)




    def expression4(self):

        localctx = MCParser.Expression4Context(self, self._ctx, self.state)
        self.enterRule(localctx, 48, self.RULE_expression4)
        self._la = 0 # Token type
        try:
            self.state = 236
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 231
                self.expression5(0)
                self.state = 232
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.LE) | (1 << MCParser.GE) | (1 << MCParser.LT) | (1 << MCParser.GT))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 233
                self.expression5(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 235
                self.expression5(0)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expression5Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression6(self):
            return self.getTypedRuleContext(MCParser.Expression6Context,0)


        def expression5(self):
            return self.getTypedRuleContext(MCParser.Expression5Context,0)


        def ADD(self):
            return self.getToken(MCParser.ADD, 0)

        def SUB(self):
            return self.getToken(MCParser.SUB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression5

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression5" ):
                return visitor.visitExpression5(self)
            else:
                return visitor.visitChildren(self)



    def expression5(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Expression5Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 50
        self.enterRecursionRule(localctx, 50, self.RULE_expression5, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 239
            self.expression6(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 246
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,19,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Expression5Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expression5)
                    self.state = 241
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 242
                    _la = self._input.LA(1)
                    if not(_la==MCParser.ADD or _la==MCParser.SUB):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 243
                    self.expression6(0) 
                self.state = 248
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,19,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expression6Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression7(self):
            return self.getTypedRuleContext(MCParser.Expression7Context,0)


        def expression6(self):
            return self.getTypedRuleContext(MCParser.Expression6Context,0)


        def DIV(self):
            return self.getToken(MCParser.DIV, 0)

        def MUL(self):
            return self.getToken(MCParser.MUL, 0)

        def MOD(self):
            return self.getToken(MCParser.MOD, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression6

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression6" ):
                return visitor.visitExpression6(self)
            else:
                return visitor.visitChildren(self)



    def expression6(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = MCParser.Expression6Context(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 52
        self.enterRecursionRule(localctx, 52, self.RULE_expression6, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 250
            self.expression7()
            self._ctx.stop = self._input.LT(-1)
            self.state = 257
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,20,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = MCParser.Expression6Context(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_expression6)
                    self.state = 252
                    if not self.precpred(self._ctx, 2):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 2)")
                    self.state = 253
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.MUL) | (1 << MCParser.DIV) | (1 << MCParser.MOD))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume()
                    self.state = 254
                    self.expression7() 
                self.state = 259
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,20,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class Expression7Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression7(self):
            return self.getTypedRuleContext(MCParser.Expression7Context,0)


        def SUB(self):
            return self.getToken(MCParser.SUB, 0)

        def NOT(self):
            return self.getToken(MCParser.NOT, 0)

        def expression8(self):
            return self.getTypedRuleContext(MCParser.Expression8Context,0)


        def getRuleIndex(self):
            return MCParser.RULE_expression7

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression7" ):
                return visitor.visitExpression7(self)
            else:
                return visitor.visitChildren(self)




    def expression7(self):

        localctx = MCParser.Expression7Context(self, self._ctx, self.state)
        self.enterRule(localctx, 54, self.RULE_expression7)
        self._la = 0 # Token type
        try:
            self.state = 263
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [MCParser.SUB, MCParser.NOT]:
                self.enterOuterAlt(localctx, 1)
                self.state = 260
                _la = self._input.LA(1)
                if not(_la==MCParser.SUB or _la==MCParser.NOT):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 261
                self.expression7()
                pass
            elif token in [MCParser.INTLIT, MCParser.FLOATLIT, MCParser.BOOLEANLIT, MCParser.STRINGLIT, MCParser.LB, MCParser.ID]:
                self.enterOuterAlt(localctx, 2)
                self.state = 262
                self.expression8()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expression8Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression9(self):
            return self.getTypedRuleContext(MCParser.Expression9Context,0)


        def LSB(self):
            return self.getToken(MCParser.LSB, 0)

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def RSB(self):
            return self.getToken(MCParser.RSB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression8

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression8" ):
                return visitor.visitExpression8(self)
            else:
                return visitor.visitChildren(self)




    def expression8(self):

        localctx = MCParser.Expression8Context(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_expression8)
        try:
            self.state = 271
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,22,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 265
                self.expression9()
                self.state = 266
                self.match(MCParser.LSB)
                self.state = 267
                self.expression()
                self.state = 268
                self.match(MCParser.RSB)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 270
                self.expression9()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Expression9Context(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expression(self):
            return self.getTypedRuleContext(MCParser.ExpressionContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def literal(self):
            return self.getTypedRuleContext(MCParser.LiteralContext,0)


        def funcall(self):
            return self.getTypedRuleContext(MCParser.FuncallContext,0)


        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def getRuleIndex(self):
            return MCParser.RULE_expression9

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpression9" ):
                return visitor.visitExpression9(self)
            else:
                return visitor.visitChildren(self)




    def expression9(self):

        localctx = MCParser.Expression9Context(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_expression9)
        try:
            self.state = 280
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,23,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 273
                self.match(MCParser.LB)
                self.state = 274
                self.expression()
                self.state = 275
                self.match(MCParser.RB)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 277
                self.literal()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 278
                self.funcall()
                pass

            elif la_ == 4:
                self.enterOuterAlt(localctx, 4)
                self.state = 279
                self.match(MCParser.ID)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class LiteralContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def INTLIT(self):
            return self.getToken(MCParser.INTLIT, 0)

        def FLOATLIT(self):
            return self.getToken(MCParser.FLOATLIT, 0)

        def STRINGLIT(self):
            return self.getToken(MCParser.STRINGLIT, 0)

        def BOOLEANLIT(self):
            return self.getToken(MCParser.BOOLEANLIT, 0)

        def getRuleIndex(self):
            return MCParser.RULE_literal

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLiteral" ):
                return visitor.visitLiteral(self)
            else:
                return visitor.visitChildren(self)




    def literal(self):

        localctx = MCParser.LiteralContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_literal)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 282
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.STRINGLIT))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class FuncallContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(MCParser.ID, 0)

        def LB(self):
            return self.getToken(MCParser.LB, 0)

        def expressionList(self):
            return self.getTypedRuleContext(MCParser.ExpressionListContext,0)


        def RB(self):
            return self.getToken(MCParser.RB, 0)

        def getRuleIndex(self):
            return MCParser.RULE_funcall

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFuncall" ):
                return visitor.visitFuncall(self)
            else:
                return visitor.visitChildren(self)




    def funcall(self):

        localctx = MCParser.FuncallContext(self, self._ctx, self.state)
        self.enterRule(localctx, 62, self.RULE_funcall)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 284
            self.match(MCParser.ID)
            self.state = 285
            self.match(MCParser.LB)
            self.state = 286
            self.expressionList()
            self.state = 287
            self.match(MCParser.RB)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExpressionListContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(MCParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(MCParser.ExpressionContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(MCParser.COMMA)
            else:
                return self.getToken(MCParser.COMMA, i)

        def getRuleIndex(self):
            return MCParser.RULE_expressionList

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpressionList" ):
                return visitor.visitExpressionList(self)
            else:
                return visitor.visitChildren(self)




    def expressionList(self):

        localctx = MCParser.ExpressionListContext(self, self._ctx, self.state)
        self.enterRule(localctx, 64, self.RULE_expressionList)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 297
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << MCParser.INTLIT) | (1 << MCParser.FLOATLIT) | (1 << MCParser.BOOLEANLIT) | (1 << MCParser.STRINGLIT) | (1 << MCParser.SUB) | (1 << MCParser.NOT) | (1 << MCParser.LB) | (1 << MCParser.ID))) != 0):
                self.state = 289
                self.expression()
                self.state = 294
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==MCParser.COMMA:
                    self.state = 290
                    self.match(MCParser.COMMA)
                    self.state = 291
                    self.expression()
                    self.state = 296
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)



        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[21] = self.expression1_sempred
        self._predicates[22] = self.expression2_sempred
        self._predicates[25] = self.expression5_sempred
        self._predicates[26] = self.expression6_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expression1_sempred(self, localctx:Expression1Context, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 2)
         

    def expression2_sempred(self, localctx:Expression2Context, predIndex:int):
            if predIndex == 1:
                return self.precpred(self._ctx, 2)
         

    def expression5_sempred(self, localctx:Expression5Context, predIndex:int):
            if predIndex == 2:
                return self.precpred(self._ctx, 2)
         

    def expression6_sempred(self, localctx:Expression6Context, predIndex:int):
            if predIndex == 3:
                return self.precpred(self._ctx, 2)
         




