"""
    Student name: Nguyen Hoang Thuan
    Student ID: 1752054
"""

import unittest
from TestUtils import TestAST
from AST import *

class ASTGenSuite(unittest.TestCase):
    def test_declare_a_variable_type_int(self):
        input = """int a;"""
        expected = str(Program([VarDecl("a",IntType())]))
        self.assertTrue(TestAST.checkASTGen(input,expected,301))

    def test_declare_a_variable_type_float(self):
        input = """float a;"""
        expected = str(Program([VarDecl("a",FloatType())]))
        self.assertTrue(TestAST.checkASTGen(input,expected,302))

    def test_declare_a_array_3_elements_type_string(self):
        input = """string a[3];"""
        expected = str(Program([VarDecl("a",ArrayType(3,StringType()))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,303))

    def test_declare_2_variable_type_boolean_inline(self):
        input = """boolean a,b;"""
        expected = str(Program([VarDecl("a",BoolType()),VarDecl("b",BoolType())]))
        self.assertTrue(TestAST.checkASTGen(input,expected,304))

    def test_declare_a_variable_and_a_array_type_int_inline(self):
        input = """int a,b[3];"""
        expected = str(Program([VarDecl("a",IntType()),VarDecl("b",ArrayType(3,IntType()))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,305))

    def test_declare_2_variable_in_multiple_line(self):
        input = """int a;float b;"""
        expected = str(Program([VarDecl("a",IntType()),VarDecl("b",FloatType())]))
        self.assertTrue(TestAST.checkASTGen(input,expected,306))

    def test_declare_variable_and_array_in_multiple_line(self):
        input = """string a;boolean b[3];"""
        expected = str(Program([VarDecl("a",StringType()),VarDecl("b",ArrayType(3,BoolType()))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,307))

    def test_declare_function_no_paramater_no_body(self):
        input = """int main(){}"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,308))

    def test_declare_function_a_paramater_no_body(self):
        input = """float main(string a){}"""
        expected = str(Program([FuncDecl(Id("main"),[VarDecl("a",StringType())],FloatType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,309))

    def test_declare_function_with_a_paramater_no_body(self):
        input = """string main(int c[]){}"""
        expected = str(Program([FuncDecl(Id("main"),[VarDecl("c",ArrayPointerType(IntType()))],StringType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,310))

    def test_declare_function_with_many_paramater_no_body_without_array_pointer_type_paramater(self):
        input = """boolean main(string a, boolean b){}"""
        expected = str(Program([FuncDecl(Id("main"),[VarDecl("a",StringType()),VarDecl("b",BoolType())],BoolType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,311))

    def test_declare_function_with_many_paramater_no_body_with_array_pointer_type_paramater(self):
        input = """string main(int a, boolean b[]){}"""
        expected = str(Program([FuncDecl(Id("main"),[VarDecl("a",IntType()),VarDecl("b",ArrayPointerType(BoolType()))],StringType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,312))

    def test_declare_function_return_array_pointer_type_int(self):
        input = """int[] main(int a){}"""
        expected = str(Program([FuncDecl(Id("main"),[VarDecl("a",IntType())],ArrayPointerType(IntType()),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,313))

    def test_declare_function_return_array_pointer_type_float(self):
        input = """float[] main(string a[]){}"""
        expected = str(Program([FuncDecl(Id("main"),[VarDecl("a",ArrayPointerType(StringType()))],ArrayPointerType(FloatType()),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,314))

    def test_declare_variable_and_function(self):
        input = """
        int a;
        float main(){}
        """
        expected = str(Program([VarDecl("a",IntType()),FuncDecl(Id("main"),[],FloatType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,315))

    def test_declare_many_variable_and_function(self):
        input = """
        int a,b[3];
        float main(){}
        boolean c;
        string[] foo(int d){}
        """
        expected = str(Program([VarDecl("a",IntType()),VarDecl("b",ArrayType(3,IntType())),FuncDecl(Id("main"),[],FloatType(),Block([])),VarDecl("c",BoolType()),FuncDecl(Id("foo"),[VarDecl("d",IntType())],ArrayPointerType(StringType()),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,316))

    def test_return_statement_no_expression(self):
        input = """int main(){return;}"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Return(None)]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,317))

    def test_return_statement_with_expression(self):
        input = """int main(){return true;}"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Return(BooleanLiteral(True))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,318))

    def test_break_statement(self):
        input = """int main(){break;}"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Break()]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,319))

    def test_continue_statement(self):
        input = """int main(){continue;}"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Continue()]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,320))
    def test_simple_if_statement(self):
        input = """
    int main(){
      if(true) return; 
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([If(BooleanLiteral(True),Return(None),None)]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,321))

    def test_simple_if_else_statement(self):
        input = """
    int main(){
      if(false) break;
      else return;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([If(BooleanLiteral(False),Break(),Return(None))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,322))

    def test_assign_operator(self):
        input = """
    int main(){
      a = b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("=",Id("a"),Id("b"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,323))
        
    def test_math_operator(self):
        input = """
    int main(){
      a + b;
      a - b;
      a * b;
      a / b;
      a % b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("+",Id("a"),Id("b")),BinaryOp("-",Id("a"),Id("b")),BinaryOp("*",Id("a"),Id("b")),BinaryOp("/",Id("a"),Id("b")),BinaryOp("%",Id("a"),Id("b"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,324))

    def test_multiple_math_operator(self):
        input = """
    int main(){
      a%1 - 1.2*a + a/3;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("+",BinaryOp("-",BinaryOp("%",Id("a"),IntLiteral(1)),BinaryOp("*",FloatLiteral(1.2),Id("a"))),BinaryOp("/",Id("a"),IntLiteral(3)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,325))

    def test_AND_OR_operator(self):
        input = """
    int main(){
      a||b;
      a&&b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("||",Id("a"),Id("b")),BinaryOp("&&",Id("a"),Id("b"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,326))

    def test_equal_operator(self):
        input = """
    int main(){
      a != 3;
      a == 1.2;
      a == true;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("!=",Id("a"),IntLiteral(3)),BinaryOp("==",Id("a"),FloatLiteral(1.2)),BinaryOp("==",Id("a"),BooleanLiteral(True))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,327))

    def test_compare_operator(self):
        input = """
    int main(){
      a > 1;
      a < 1.2;
      a >= 1.2e3;
      a <= 2;
     }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp(">",Id("a"),IntLiteral(1)),BinaryOp("<",Id("a"),FloatLiteral(1.2)),BinaryOp(">=",Id("a"),FloatLiteral(1200.0)),BinaryOp("<=",Id("a"),IntLiteral(2))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,328))

    def test_unary_operator(self):
        input = """
    int main(){
      !a;
      !1;
      -2;
      -1.2e-3;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([UnaryOp("!",Id("a")),UnaryOp("!",IntLiteral(1)),UnaryOp("-",IntLiteral(2)),UnaryOp("-",FloatLiteral(0.0012))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,329))

    def test_multiple_unary_operator(self):
        input = """
    int main(){
      !!a;
      -!1;
      --1.2;
      ---false;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([UnaryOp("!",UnaryOp("!",Id("a"))),UnaryOp("-",UnaryOp("!",IntLiteral(1))),UnaryOp("-",UnaryOp("-",FloatLiteral(1.2))),UnaryOp("-",UnaryOp("-",UnaryOp("-",BooleanLiteral(False))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,330))

    def test_multiple_assign_operator(self):
        input = """
    int main(){
      a=1="string"=1.2e3;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("=",Id("a"),BinaryOp("=",IntLiteral(1),BinaryOp("=",StringLiteral("string"),FloatLiteral(1200.0))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,331))

    def test_nested_if_else_statement_inline(self):
        input = """
    void func(){
      if(a>1) if(a>2) a=1; else a=2;    
    }
    """
        expected = str(Program([FuncDecl(Id("func"),[],VoidType(),Block([If(BinaryOp(">",Id("a"),IntLiteral(1)),If(BinaryOp(">",Id("a"),IntLiteral(2)),BinaryOp("=",Id("a"),IntLiteral(1)),BinaryOp("=",Id("a"),IntLiteral(2))),None)]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,332))

    def test_if_else_statement_with_block_statement(self):
        input = """
    int main(){
      if(exp1){}
      else{}
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([If(Id("exp1"),Block([]),Block([]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,333))

    def test_nested_if_else_statement_with_block_statement(self):
        input = """
    int main(){
      if(a>1){
        if(a>2) a=1;
      }
      else a=2;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([If(BinaryOp(">",Id("a"),IntLiteral(1)),Block([If(BinaryOp(">",Id("a"),IntLiteral(2)),BinaryOp("=",Id("a"),IntLiteral(1)),None)]),BinaryOp("=",Id("a"),IntLiteral(2)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,334))

    def test_for_statement(self):
        input = """
    int main(){
      for(i=1;i<10;i=i+1) print(i);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([For(BinaryOp("=",Id("i"),IntLiteral(1)),BinaryOp("<",Id("i"),IntLiteral(10)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),CallExpr(Id("print"),[Id("i")]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,335))

    def test_nested_for_statement(self):
        input = """
    int main(){
      for(i=1;i<10;i=i+1)
        for(j=1;j<10;j=j+1)
          print(i+j);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([For(BinaryOp("=",Id("i"),IntLiteral(1)),BinaryOp("<",Id("i"),IntLiteral(10)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),For(BinaryOp("=",Id("j"),IntLiteral(1)),BinaryOp("<",Id("j"),IntLiteral(10)),BinaryOp("=",Id("j"),BinaryOp("+",Id("j"),IntLiteral(1))),CallExpr(Id("print"),[BinaryOp("+",Id("i"),Id("j"))])))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,336))

    def test_nested_for_statement_with_block_statement(self):
        input = """
    int main(){
      for(i=1;i<10;i=i+1){
        for(j=1;j<10;j=j+1){
          print(i);
          print(j);
        }
      }
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([For(BinaryOp("=",Id("i"),IntLiteral(1)),BinaryOp("<",Id("i"),IntLiteral(10)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),Block([For(BinaryOp("=",Id("j"),IntLiteral(1)),BinaryOp("<",Id("j"),IntLiteral(10)),BinaryOp("=",Id("j"),BinaryOp("+",Id("j"),IntLiteral(1))),Block([CallExpr(Id("print"),[Id("i")]),CallExpr(Id("print"),[Id("j")])]))]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,337))

    def test_do_while_statement(self):
        input = """
    int main(){
      do
      {}
      while(a<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Dowhile([Block([])],BinaryOp("<",Id("a"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,338))

    def test_do_while_with_many_statement(self):
        input = """
    int main(){
      do
        a=a+1;
        a=a+2;
      while(a<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Dowhile([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1))),BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(2)))],BinaryOp("<",Id("a"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,339))

    def test_do_while_with_block_statement(self):
        input = """
    int main(){
      do{
        a=a+1;
        a=a+2;
      }
      while(a<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Dowhile([Block([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1))),BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(2)))])],BinaryOp("<",Id("a"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,340))

    def test_do_while_with_many_block_statement(self):
        input = """
    int main(){
      do{
        a=a+1;
      }{
        a=a+2;
      }
      while(a<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Dowhile([Block([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1)))]),Block([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(2)))])],BinaryOp("<",Id("a"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,341))

    def test_declare_variable_in_block_statement(self):
        input = """
    int main(){
      int a;
      float b[3];
      string c,d[3];
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",IntType()),VarDecl("b",ArrayType(3,FloatType())),VarDecl("c",StringType()),VarDecl("d",ArrayType(3,StringType()))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,342))

    def test_declare_and_intital_variable_in_block_statement(self):
        input = """
    int main(){
      int a;
      a=1;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",IntType()),BinaryOp("=",Id("a"),IntLiteral(1))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,343))

    def test_declare_and_if_statement(self):
        input = """
    int main(){
      int a;
      a=1;
      if(a>0) println("a la so nguyen duong");
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",IntType()),BinaryOp("=",Id("a"),IntLiteral(1)),If(BinaryOp(">",Id("a"),IntLiteral(0)),CallExpr(Id("println"),[StringLiteral("a la so nguyen duong")]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,344))
        
    def test_factorial_function(self):
        input = """
    int fact(int n){
      if(n<=0) return 1;
      else return n*fact(n-1);
    }
    """
        expected = str(Program([FuncDecl(Id("fact"),[VarDecl("n",IntType())],IntType(),Block([If(BinaryOp("<=",Id("n"),IntLiteral(0)),Return(IntLiteral(1)),Return(BinaryOp("*",Id("n"),CallExpr(Id("fact"),[BinaryOp("-",Id("n"),IntLiteral(1))]))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,345))

    def test_sum_function(self):
        input = """
    int sum(int n){
      int s,i;
      s=0;
      for(i=1;i<=n;i=i+1) s=s+i;
      return s;
    }
    """
        expected = str(Program([FuncDecl(Id("sum"),[VarDecl("n",IntType())],IntType(),Block([VarDecl("s",IntType()),VarDecl("i",IntType()),BinaryOp("=",Id("s"),IntLiteral(0)),For(BinaryOp("=",Id("i"),IntLiteral("1")),BinaryOp("<=",Id("i"),Id("n")),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),BinaryOp("=",Id("s"),BinaryOp("+",Id("s"),Id("i")))),Return(Id("s"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,346))

    def test_funcall_expression(self):
        input = """
    int main(){
      sum(10);
      fact(10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([CallExpr(Id("sum"),[IntLiteral(10)]),CallExpr(Id("fact"),[IntLiteral(10)])]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,347))

    def test_index_operator(self):
        input = """
    int main(){
      int a[1];
      a[0]=5;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",ArrayType(1,IntType())),BinaryOp("=",ArrayCell(Id("a"),IntLiteral(0)),IntLiteral(5))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,348))

    def test_array_function(self):
        input = """
    int[] array(int a,int b){
      int arr[2];
      arr[0]=a;
      arr[1]=b;
      return arr;
    }
    """
        expected = str(Program([FuncDecl(Id("array"),[VarDecl("a",IntType()),VarDecl("b",IntType())],ArrayPointerType(IntType()),Block([VarDecl("arr",ArrayType(2,IntType())),BinaryOp("=",ArrayCell(Id("arr"),IntLiteral(0)),Id("a")),BinaryOp("=",ArrayCell(Id("arr"),IntLiteral(1)),Id("b")),Return(Id("arr"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,349))

    def test_funcall_with_index_operator(self):
        input = """
    int main(){
      array(1,2)[0];
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([ArrayCell(CallExpr(Id("array"),[IntLiteral(1),IntLiteral(2)]),IntLiteral(0))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,350))
    def test_expression_with_bracket(self):
        input = """
    int main(){
      int a;
      a=(1+2)*sum(2);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",IntType()),BinaryOp("=",Id("a"),BinaryOp("*",BinaryOp("+",IntLiteral(1),IntLiteral(2)),CallExpr(Id("sum"),[IntLiteral(2)])))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,351))

    def test_nested_funcall_statement(self):
        input = """
    int main(){
      sum(fact(10));
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([CallExpr(Id("sum"),[CallExpr(Id("fact"),[IntLiteral(10)])])]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,352))

    def test_funcall_with_many_paramater(self):
        input = """
    int main(){
      complex(a[2]+1*fact(2)/-2-5,true,1.23);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([CallExpr(Id("complex"),[BinaryOp("-",BinaryOp("+",ArrayCell(Id("a"),IntLiteral(2)),BinaryOp("/",BinaryOp("*",IntLiteral(1),CallExpr(Id("fact"),[IntLiteral(2)])),UnaryOp("-",IntLiteral(2)))),IntLiteral(5)),BooleanLiteral(True),FloatLiteral(1.23)])]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,353))

    def test_if_statement_inner_for_statement(self):
        input = """
    int main(){
      int i;
      for(i=0;i<10;i=i+1)
        if(i%2==0) print(i);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),For(BinaryOp("=",Id("i"),IntLiteral(0)),BinaryOp("<",Id("i"),IntLiteral(10)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),If(BinaryOp("==",BinaryOp("%",Id("i"),IntLiteral(2)),IntLiteral(0)),CallExpr(Id("print"),[Id("i")]),None))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,354))

    def test_if_statement_inner_do_while_statement(self):
        input = """
    int main(){
      int i;
      i=0;
      do
        if(i%2==0) print(i);
        i=i+1;
      while(i<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),BinaryOp("=",Id("i"),IntLiteral(0)),Dowhile([If(BinaryOp("==",BinaryOp("%",Id("i"),IntLiteral(2)),IntLiteral(0)),CallExpr(Id("print"),[Id("i")]),None),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1)))],BinaryOp("<",Id("i"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,355))

    def test_continue_innner_for_statement(self):
        input = """
    int main(){
      int i;
      for(i=0;i<10;i=i+1){
        if(i%2!=0) continue; 
        print(i);
      }
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),For(BinaryOp("=",Id("i"),IntLiteral(0)),BinaryOp("<",Id("i"),IntLiteral(10)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),Block([If(BinaryOp("!=",BinaryOp("%",Id("i"),IntLiteral(2)),IntLiteral(0)),Continue(),None),CallExpr(Id("print"),[Id("i")])]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,356))

    def test_break_inner_do_while_statement(self):
        input = """
    int main(){
      int i;
      i=0;
      do
        if(i%2==0) print(i);
        i=i+1;
        if(i>=10) break;
      while(true);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),BinaryOp("=",Id("i"),IntLiteral(0)),Dowhile([If(BinaryOp("==",BinaryOp("%",Id("i"),IntLiteral(2)),IntLiteral(0)),CallExpr(Id("print"),[Id("i")]),None),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),If(BinaryOp(">=",Id("i"),IntLiteral(10)),Break(),None)],BooleanLiteral(True))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,357))

    def test_declare_global_and_local_variable(self):
        input = """
    int a;
    int main(){
      float a;
      string a[3];
    }
    """
        expected = str(Program([VarDecl("a",IntType()),FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",FloatType()),VarDecl("a",ArrayType(3,StringType()))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,358))

    def test_nested_do_while_statement(self):
        input = """
    int main(){
      int i,j;
      i=0;j=0;
      do 
        do
          print(i);
          print(j);
          j=j+1; 
        while(j<10);
        i=i+1; 
      while(i<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),VarDecl("j",IntType()),BinaryOp("=",Id("i"),IntLiteral(0)),BinaryOp("=",Id("j"),IntLiteral(0)),Dowhile([Dowhile([CallExpr(Id("print"),[Id("i")]),CallExpr(Id("print"),[Id("j")]),BinaryOp("=",Id("j"),BinaryOp("+",Id("j"),IntLiteral(1)))],BinaryOp("<",Id("j"),IntLiteral(10))),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1)))],BinaryOp("<",Id("i"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,359))

    def test_for_statement_innner_do_while_statement(self):
        input = """
    int main(){
      int i,j;
      i=0;
      do
        for(j=0;j<10;j=j+1){
          print(i);
          print(j);
        }
        i=i+1;
      while(i<10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),VarDecl("j",IntType()),BinaryOp("=",Id("i"),IntLiteral(0)),Dowhile([For(BinaryOp("=",Id("j"),IntLiteral(0)),BinaryOp("<",Id("j"),IntLiteral(10)),BinaryOp("=",Id("j"),BinaryOp("+",Id("j"),IntLiteral(1))),Block([CallExpr(Id("print"),[Id("i")]),CallExpr(Id("print"),[Id("j")])])),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1)))],BinaryOp("<",Id("i"),IntLiteral(10)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,360))

    def test_many_assign_and_equal_operator(self):
        input = """
    int main(){
      (a=b==c)!=d=e;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("=",BinaryOp("!=",BinaryOp("=",Id("a"),BinaryOp("==",Id("b"),Id("c"))),Id("d")),Id("e"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,361))

    def test_many_compare_operator(self):
        input = """
    int main(){
      (a>b)>=c==(d<e)<=f;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("==",BinaryOp(">=",BinaryOp(">",Id("a"),Id("b")),Id("c")),BinaryOp("<=",BinaryOp("<",Id("d"),Id("e")),Id("f")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,362))

    def test_return_a_funcall(self):
        input = """
    int main(){
      return sum(10);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Return(CallExpr(Id("sum"),[IntLiteral(10)]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,363))

    def test_return_a_string(self):
        input = """
    string main(){
      return "String";
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],StringType(),Block([Return(StringLiteral("String"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,364))
    

    def test_return_a_expression(self):
        input = """
    float main(){
      return 1.23+4*a%5/b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],FloatType(),Block([Return(BinaryOp("+",FloatLiteral(1.23),BinaryOp("/",BinaryOp("%",BinaryOp("*",IntLiteral(4),Id("a")),IntLiteral(5)),Id("b"))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,365))

    def test_return_a_float(self):
        input = """
    float main(){
      return 1.23E-4;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],FloatType(),Block([Return(FloatLiteral(1.23E-4))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,366))

    def test_mix_up_operator(self):
        input = """ 
    int foo(){
      foo(2)[3]=a||b&&(c-35.4e10)*9+10.0/4+true;
    }"""
        expected = str(Program([FuncDecl(Id("foo"),[],IntType(),Block([BinaryOp("=",ArrayCell(CallExpr(Id("foo"),[IntLiteral(2)]),IntLiteral(3)),BinaryOp("||",Id("a"),BinaryOp("&&",Id("b"),BinaryOp("+",BinaryOp("+",BinaryOp("*",BinaryOp("-",Id("c"),FloatLiteral(3.54E11)),IntLiteral(9)),BinaryOp("/",FloatLiteral(10.0),IntLiteral(4))),BooleanLiteral(True)))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,367))

    def test_program_with_line_comment(self):
        input = """
    //Main Function
    int main(){
      //Body
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,368))

    def test_program_with_block_comment(self):
        input = """
      int main(){
        /* Empty
        Body */
      }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,369))

    def test_complex_index_expression(self):
        input = """
    int main(){
      int a[10];
      return a[(i-j)*(i+j)%2];
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("a",ArrayType(10,IntType())),Return(ArrayCell(Id("a"),BinaryOp("%",BinaryOp("*",BinaryOp("-",Id("i"),Id("j")),BinaryOp("+",Id("i"),Id("j"))),IntLiteral(2))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,370))

    def test_do_while_statement_innner_for_statement(self):
        input = """
    int main(){
      int i,j;
      j=0;
      for(i=0;i<10;i=i+1)
        do
          print(i);
          print(j);
          j==j+1;
        while i<10;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([VarDecl("i",IntType()),VarDecl("j",IntType()),BinaryOp("=",Id("j"),IntLiteral(0)),For(BinaryOp("=",Id("i"),IntLiteral(0)),BinaryOp("<",Id("i"),IntLiteral(10)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),Dowhile([CallExpr(Id("print"),[Id("i")]),CallExpr(Id("print"),[Id("j")]),BinaryOp("==",Id("j"),BinaryOp("+",Id("j"),IntLiteral(1)))],BinaryOp("<",Id("i"),IntLiteral(10))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,371))

    def test_if_else_stmt_with_a_function_call_in_condition_part(self):
        input = """
    void main(){
      if(sum(5)) a=b;
      else c=d;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([If(CallExpr(Id("sum"),[IntLiteral(5)]),BinaryOp("=",Id("a"),Id("b")),BinaryOp("=",Id("c"),Id("d")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,372))

    def test_do_while_stmt_with_conditional_expression_is_index_expression(self):
        input = """
    void main(){
      do
      a=a+1;
      while array[1];
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([Dowhile([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1)))],ArrayCell(Id("array"),IntLiteral(1)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,373))

    def test_many_block_statement_in_block_statement(self):
        input = """
    int main(){
      {
        {
          {
            {
              {
                return 0;
              }
            }
          }
        }
      }
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([Block([Block([Block([Block([Block([Return(IntLiteral(0))])])])])])]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,374))    
    def test_do_while_stmt_with_conditional_expression_is_a_function_call(self):
        input = """
    void main(){
      do
        a=a+1;
      while sum(3);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([Dowhile([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1)))],CallExpr(Id("sum"),[IntLiteral(3)]))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,375))

    def test_return_an_or_statement(self):
        input = """
    void main(){
      return a||b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([Return(BinaryOp("||",Id("a"),Id("b")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,376))

    def test_minus_sign_before_bracket(self):
        input = """
    int main(){
      -(1+2);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([UnaryOp("-",BinaryOp("+",IntLiteral(1),IntLiteral(2)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,377))

    def test_minus_sign_with_index_expression(self):
        input = """
    int main(){
      -array[1];
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([UnaryOp("-",ArrayCell(Id("array"),IntLiteral(1)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,378))

    def test_minus_sign_with_subtract_operator_in_expression(self):
        input = """
    int main(){
      -1--2--3;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),{},IntType(),Block([BinaryOp("-",BinaryOp("-",UnaryOp("-",IntLiteral(1)),UnaryOp("-",IntLiteral(2))),UnaryOp("-",IntLiteral(3)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,379))

    def test_nested_index_operator(self):
        input = """
    int main(){
      array[array[array[1]]];
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([ArrayCell(Id("array"),ArrayCell(Id("array"),ArrayCell(Id("array"),IntLiteral(1))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,380))

    def test_expression_statement_with_only_a_operand(self):
        input = """
    void main(){
      1;
      a;
      1.E-2;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([IntLiteral(1),Id("a"),FloatLiteral(0.01)]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,381))

    def test_or_and_with_not_of_a_variable(self):
        input = """
    void main(){
      a || !b;
      a && !b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp("||",Id("a"),UnaryOp("!",Id("b"))),BinaryOp("&&",Id("a"),UnaryOp("!",Id("b")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,382))

    def test_add_sub_a_number_with_a_negative_number(self):
        input = """
    void main(){
      a + -b;
      a - -b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp("+",Id("a"),UnaryOp("-",Id("b"))),BinaryOp("-",Id("a"),UnaryOp("-",Id("b")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,383))

    def test_mul_div_mod_a_number_with_a_negative_number(self):
        input = """
    void main(){
      a % -b;
      a / -b;
      a * -b;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp("%",Id("a"),UnaryOp("-",Id("b"))),BinaryOp("/",Id("a"),UnaryOp("-",Id("b"))),BinaryOp("*",Id("a"),UnaryOp("-",Id("b")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,384))

    def test_bracket_and_none_associativity(self):
        input = """
    void main(){
      (a == b) == c;
      a = b == c = d == e;
      a = b == c = d;
      a=b=c=d;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp("==",BinaryOp("==",Id("a"),Id("b")),Id("c")),BinaryOp("=",Id("a"),BinaryOp("=",BinaryOp("==",Id("b"),Id("c")),BinaryOp("==",Id("d"),Id("e")))),BinaryOp("=",Id("a"),BinaryOp("=",BinaryOp("==",Id("b"),Id("c")),Id("d"))),BinaryOp("=",Id("a"),BinaryOp("=",Id("b"),BinaryOp("=",Id("c"),Id("d"))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,385))

    def test_two_kind_of_none_associativity(self):
        input = """
    void main(){
      a == b >= c;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp("==",Id("a"),BinaryOp(">=",Id("b"),Id("c")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,386))

    def test_for_do_while_statement_inside_block_of_statement(self):
        input = """
    void main(){
      for(i=0;i<3;i=i+1) a=a+1;
      do
        a=a+1;
      while true;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([For(BinaryOp("=",Id("i"),IntLiteral(0)),BinaryOp("<",Id("i"),IntLiteral(3)),BinaryOp("=",Id("i"),BinaryOp("+",Id("i"),IntLiteral(1))),BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1)))),Dowhile([BinaryOp("=",Id("a"),BinaryOp("+",Id("a"),IntLiteral(1)))],BooleanLiteral(True))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,387))

    def test_ucln_function(self):
        input = """
    int ucln(int a, int b){
      int u;
      if (a<b) u=a;
      else u=b;
      do u=u-1;
      while((a%u!=0)||(b%u!=0));
    }
    """
        expected = str(Program([FuncDecl(Id("ucln"),[VarDecl("a",IntType()),VarDecl("b",IntType())],IntType(),Block([VarDecl("u",IntType()),If(BinaryOp("<",Id("a"),Id("b")),BinaryOp("=",Id("u"),Id("a")),BinaryOp("=",Id("u"),Id("b"))),Dowhile([BinaryOp("=",Id("u"),BinaryOp("-",Id("u"),IntLiteral(1)))],BinaryOp("||",BinaryOp("!=",BinaryOp("%",Id("a"),Id("u")),IntLiteral(0)),BinaryOp("!=",BinaryOp("%",Id("b"),Id("u")),IntLiteral(0))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,388))

    def test_program_read_a_file(self):
        input = """
    int main(){
      fp = fopen("file.txt", "r");
      fclose(fp);
      return 0;
    }"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([BinaryOp("=",Id("fp"),CallExpr(Id("fopen"),[StringLiteral("file.txt"),StringLiteral("r")])),CallExpr(Id("fclose"),[Id("fp")]),Return(IntLiteral(0))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,389))

    def test_complex_expression(self):
        input = """
    void foo(){
      -1.23+!array[2]*sum(3);
    }
    """
        expected = str(Program([FuncDecl(Id("foo"),[],VoidType(),Block([BinaryOp("+",UnaryOp("-",FloatLiteral(1.23)),BinaryOp("*",UnaryOp("!",ArrayCell(Id("array"),IntLiteral(2))),CallExpr(Id("sum"),[IntLiteral(3)])))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,390))

    def test_none_associativity_of_GE_and_GT(self):
        input = """
    void main(){
      a >= (b > c);
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp(">=",Id("a"),BinaryOp(">",Id("b"),Id("c")))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,391))

    def test_none_associativity_of_LE_and_LT(self):
        input = """
    void main(){
      (a < b) <= c;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([BinaryOp("<=",BinaryOp("<",Id("a"),Id("b")),Id("c"))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,392))

    def test_nested_block_statement(self):
        input = """
    void main(){
      {
        int a, b;
        a = b;
        {
          int c;
          c = 0;
        }
      }
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([Block([VarDecl("a",IntType()),VarDecl("b",IntType()),BinaryOp("=",Id("a"),Id("b")),Block([VarDecl("c",IntType()),BinaryOp("=",Id("c"),IntLiteral(0))])])]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,393))

    def test_return_an_expression(self):
        input = """
    void main(){
      return a+1==b*2 = c%3!=d/4;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([Return(BinaryOp("=",BinaryOp("==",BinaryOp("+",Id("a"),IntLiteral(1)),BinaryOp("*",Id("b"),IntLiteral(2))),BinaryOp("!=",BinaryOp("%",Id("c"),IntLiteral(3)),BinaryOp("/",Id("d"),IntLiteral(4)))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,394))

    def test_do_complex_while_statement(self):
        input = """
    void main(){
      do
        a = b;
        c = c + 1;
        foo();
        {
          int a;
          int b;
          a = b;
        }
      while a == 0;
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],VoidType(),Block([Dowhile([BinaryOp("=",Id("a"),Id("b")),BinaryOp("=",Id("c"),BinaryOp("+",Id("c"),IntLiteral(1))),CallExpr(Id("foo"),[]),Block([VarDecl("a",IntType()),VarDecl("b",IntType()),BinaryOp("=",Id("a"),Id("b"))])],BinaryOp("==",Id("a"),IntLiteral(0)))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,395))

    def test_Hello_Word(self):
        input = """
    int main(){
      print("Hello Word\\n");
    }
    """
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([CallExpr(Id("print"),[StringLiteral("Hello Word\\n")])]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,396))

    def test_check_leap_year_function(self):
        input = """
    boolean isLeapYear(int year){
      if (year < 0 )
        cout ("This is a invalid Year." ,endl);
      else
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
          return true;
        else
          return false;
    }
    """
        expected = str(Program([FuncDecl(Id("isLeapYear"),[VarDecl("year",IntType())],BoolType(),Block([If(BinaryOp("<",Id("year"),IntLiteral(0)),CallExpr(Id("cout"),[StringLiteral("This is a invalid Year."),Id("endl")]),If(BinaryOp("||",BinaryOp("==",BinaryOp("%",Id("year"),IntLiteral(400)),IntLiteral(0)),BinaryOp("&&",BinaryOp("==",BinaryOp("%",Id("year"),IntLiteral(4)),IntLiteral(0)),BinaryOp("!=",BinaryOp("%",Id("year"),IntLiteral(100)),IntLiteral(0)))),Return(BooleanLiteral(True)),Return(BooleanLiteral(False))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,397))

    def test_super_dangling_if_else(self):
        input = """
    void foo(){
      if(true) if(true) a = 1; else if(true) a=1; else a=2;
    }
    """
        expected = str(Program([FuncDecl(Id("foo"),[],VoidType(),Block([If(BooleanLiteral(True),If(BooleanLiteral(True),BinaryOp("=",Id("a"),IntLiteral(1)),If(BooleanLiteral(True),BinaryOp("=",Id("a"),IntLiteral(1)),BinaryOp("=",Id("a"),IntLiteral(2)))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,398))
    def test_newline_in_string(self):
        input = """int main(){"\\n";}"""
        expected = str(Program([FuncDecl(Id("main"),[],IntType(),Block([StringLiteral("\\n")]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,399))
        
    def test_if_if_else_expression(self):
        input = """
    void foo(){
      if(a == 0) if(b == 0) a = 1; else a = 2;
    }
    """
        expected = str(Program([FuncDecl(Id("foo"),[],VoidType(),Block([If(BinaryOp("==",Id("a"),IntLiteral(0)),If(BinaryOp("==",Id("b"),IntLiteral(0)),BinaryOp("=",Id("a"),IntLiteral(1)),BinaryOp("=",Id("a"),IntLiteral(2))))]))]))
        self.assertTrue(TestAST.checkASTGen(input,expected,400))

        
