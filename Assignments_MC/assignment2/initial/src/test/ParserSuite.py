import unittest
from TestUtils import TestParser

class ParserSuite(unittest.TestCase):
    def test_simple_program(self):
        """Simple program: int main() {} """
        input = """int main() {a[b[2]];}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,201))

    def test_more_complex_program(self):
        """More complex program"""
        input = """int main () {
                       int a;
                       putIntLn(4);
                   }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,202))
    
    def test_wrong_miss_close(self):
        """Miss ) int main( {}"""
        input = """int main( {}"""
        expect = "Error on line 1 col 10: {"
        self.assertTrue(TestParser.checkParser(input,expect,203))

    def test_simple_variable_declaration(self):
        """simple variable declaration"""
        input = """int a;"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,204))
    def test_multiple_variable_declaration(self):
        """multiple variable declaration"""
        input = """float b,c,d;"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,205))
    def test_simple_array_declaration(self):
        """simple array declaration"""
        input = """string str[5];"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,206))
    def test_multiple_variable_with_array(self):
        """multiple variable declaration with array"""
        input = """boolean d,e,f[10];"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,207))
    def test_multiple_type_variable_declaration(self):
        """multiple type variable declaration"""
        input = """int a; float b,c[3]; boolean d,e; string f;"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,208))
    def test_invalid_variable_declaration(self):
        """ invalid variable declaration"""
        input = """int a"""
        expect = "Error on line 1 col 5: <EOF>"
        self.assertTrue(TestParser.checkParser(input,expect,209))
    def test_invalid_keyword_variable_declaration(self):
        """invalid keyword variable declaration"""
        input = """float true;"""
        expect = "Error on line 1 col 6: true"
        self.assertTrue(TestParser.checkParser(input,expect,210))
    def test_invalid_empty_length_array_declaration(self):
        """invalid empty-length array declaration"""
        input = """int a[];"""
        expect = "Error on line 1 col 6: ]"
        self.assertTrue(TestParser.checkParser(input,expect,211))
    def test_invalid_void_declaration(self):
        """invalid void variable declaration"""
        input = """void a;"""
        expect = "Error on line 1 col 6: ;"
        self.assertTrue(TestParser.checkParser(input,expect,212))
    def test_invalid_2D_array_declaration(self):
        """invalid 2d array declaration"""
        input = """int a[3][5];"""
        expect = "Error on line 1 col 8: ["
        self.assertTrue(TestParser.checkParser(input,expect,213))
    def test_invalid_float_array_declaration(self):
        """invalid float array declaration"""
        input = """int a[1.23E2];"""
        expect = "Error on line 1 col 6: 1.23E2"
        self.assertTrue(TestParser.checkParser(input,expect,214))
    def test_invalid_identifier_array_declaration(self):
        """invalid indentifier array declaration"""
        input = """int a[var];"""
        expect = "Error on line 1 col 6: var"
        self.assertTrue(TestParser.checkParser(input,expect,215))
    def test_invalid_initialized_variable_declaration(self):
        """invalid initialized variable declaration"""
        input = """int a = 3;"""
        expect = "Error on line 1 col 6: ="
        self.assertTrue(TestParser.checkParser(input,expect,216))
    def test_invalid_array_with_keyword_in_bracket(self):
        """invalid array with keyword in bracket"""
        input = """int a[do];"""
        expect = "Error on line 1 col 6: do"
        self.assertTrue(TestParser.checkParser(input,expect,217))
    def test_simple_function_declaration(self):
        """simple function declaration"""
        input = """void foo() {}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,218))
    def test_simple_function_with_parameter(self):
        """simple function with parameters"""
        input = """float sum(int a, float b){
                       return a+b;
                   }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,219))
    def test_simple_function_with_array_parameter(self):
        """simple function with array parameter"""
        input = """void foo(boolean flags[]) {}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,220))
    def test_empty_parameter_function(self):
        """empty parameter function"""
        input = """void func(){int a; a=1;return;}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,221))
    def test_invalid_function_type(self):
        """invalid function type"""
        input = """void[] func(){}"""
        expect = "Error on line 1 col 4: ["
        self.assertTrue(TestParser.checkParser(input,expect,222))
    def test_invalid_array_parameter(self):
        """invalid array parameter"""
        input = """void func(int a[5]){}"""
        expect = "Error on line 1 col 16: 5"
        self.assertTrue(TestParser.checkParser(input,expect,223))
    def test_invalid_parameter_type(self):
        """invalid parameter type"""
        input = """void func(void a){}"""
        expect = "Error on line 1 col 10: void"
        self.assertTrue(TestParser.checkParser(input,expect,224))
    def test_invalid_array_type_parameter(self):
        input = """void func(int[] a){ return; }"""
        expect = "Error on line 1 col 13: ["
        self.assertTrue(TestParser.checkParser(input,expect,225))
    def test_no_body_function(self):
        """no body function"""
        input = """void func()"""
        expect = "Error on line 1 col 11: <EOF>"
        self.assertTrue(TestParser.checkParser(input,expect,226))
    def test_nested_function(self):
        """nested function"""
        input = """ void func1(){
                      int func2(){}
                    }"""
        expect = "Error on line 2 col 31: ("
        self.assertTrue(TestParser.checkParser(input,expect,227))
    def test_error_in_block_statement(self):
        """error in block statement"""
        input = """ void func(int a[],int b){
                      int a;
                      a=1;      
                      void b;
                    }"""
        expect = "Error on line 4 col 22: void"
        self.assertTrue(TestParser.checkParser(input,expect,228))
    def test_valid_block_statement(self):
        """valid block statement"""
        input = """ void func(int a[],int b){      
                      int a;
                      a=1;
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,229))
    def test_if_statement(self):
        """if statement"""
        input = """ void func(){
                      int a;
                      if(true) a=1;
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,230))
    def test_if_else_statement(self):
        """if else statement"""
        input = """ void func(){
                      int a;
                      if(true) a=1; else a=2;
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,231))
    def test_for_statement(self):
        """for statement"""
        input = """ void func(){
                      int i,a;
                      for(i=0;i<10;i=i+1) a=1;
                    }"""
        expect ="successful"
        self.assertTrue(TestParser.checkParser(input,expect,232))
    def test_valid_nested_if_else_statement(self):
        """valid nested if else statement"""
        input = """ void func(){
                      int a;
                      if(true) if(false) a=1; else a=2;    
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,233))
    def test_valid_break_continue_in_for_statement(self):
        """valid break continue in for statement"""
        input = """ void func(){
                      int i;
                      for(i=0;i<10;i=i+1){
                        if(i==3) continue;
                        if(i==9) break;
                      } 
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,234))
    def test_valid_while_statement(self):
        """valid while statement"""
        input = """ void func(){
                      a=0;
                      do
                        a=a+1;      
                      while(a<10);
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,235))
    def test_invalid_while_statement(self):
        """invalid statement in while statement"""
        input = """ void func(){
                      a=0;
                      do
                        a=a+1;      
                      while(a<10;);
                    }"""
        expect = "Error on line 5 col 32: ;"
        self.assertTrue(TestParser.checkParser(input,expect,236))
    def test_nested_loop_statement(self):
        """nested for statement"""
        input = """ void func(){
                      int i,j,a;
                      for(i=0;i<5;i=i+1)
                        for(j=10;j>5;j=j-1)
                          a=1;
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,237))
    def test_nested_loop_statement_for_while(self):
        """nested for do while statement"""
        input = """ void func(){
                      int i,j;
                      for(i=0;i<5;i=i+1){
                        j=0;
                        do
                          j = j+1;
                        while (j<5);
                      }
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,238))
    def test_not_statement_after_if(self):
        """uncompleted if statement"""
        input = """ int func(){
                      if(true)
                    }"""
        expect = "Error on line 3 col 20: }"
        self.assertTrue(TestParser.checkParser(input,expect,239))
    def test_declare_variable_in_block_statement_after_if(self):
        input = """ int func(){
                      int a;
                      if(a==0){
                        int b;
                      }
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,240))
    def test_valid_for_statement(self):
        input = """ int func(){
                      int i;
                      for(i=0;i+3;i=i+1){}
                    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,241))
    def test_invalid_no_statement_after_for(self):
        input = """ int func(){
                      int i;
                      for(i=0;i<=3;i=i+1)
                    }"""
        expect = "Error on line 4 col 20: }"
        self.assertTrue(TestParser.checkParser(input,expect,242))
    def test_not_enough_expression_in_for_statement(self):
        input = """ int func(){
                      int a;
                      for(;a+3;a=a+1)
                    }"""
        expect = "Error on line 3 col 26: ;"
        self.assertTrue(TestParser.checkParser(input,expect,243))
    def test_not_statement_after_do(self):
        input = """ int func(){
                      do
                      while(true);
                    }"""
        expect = "Error on line 3 col 22: while"
        self.assertTrue(TestParser.checkParser(input,expect,244))
    def test_missing_SEMI_after_while(self):
        input = """ int func(){
                      do{}
                      while(true)
                    }"""
        expect = "Error on line 4 col 20: }"
        self.assertTrue(TestParser.checkParser(input,expect,245))
    def test_invalid_expression_condition(self):
        input = """ void func(int mark){
                      if(0<a<=10)a=1;
                    }"""
        expect = "Error on line 2 col 28: <="
        self.assertTrue(TestParser.checkParser(input,expect,246))
    def test_many_relational_operator(self):
        input = """ void func(){
                      a>b<c;
                    }"""
        expect = "Error on line 2 col 25: <"
        self.assertTrue(TestParser.checkParser(input,expect,247))
    def test_many_equality_operator(self):
        input = """ void func(){
                      a==b==c;
                    }"""
        expect = "Error on line 2 col 26: =="
        self.assertTrue(TestParser.checkParser(input,expect,248))
    def test_invalid_AND_operator(self):
        input = """ int func(){
                      a&&&&b;
                    }"""
        expect = "Error on line 2 col 25: &&";
        self.assertTrue(TestParser.checkParser(input,expect,249))
    def test_invalid_OR_operator(self):
        input = """ int func(){
                      a=b||c||;
                    }"""
        expect = "Error on line 2 col 30: ;";
        self.assertTrue(TestParser.checkParser(input,expect,250))
    def test_invalid_unary_NOT_operator(self):
        input = """ int func(){
                      a!b;
                    }"""
        expect = "Error on line 2 col 23: !";
        self.assertTrue(TestParser.checkParser(input,expect,251))
    def test_invalid_2D_array_operator(self):
        input = """ int foo(){
                      a[1][1];
                    }"""
        expect = "Error on line 2 col 26: [";
        self.assertTrue(TestParser.checkParser(input,expect,252))
    def test_invalid_empty_array_operator(self):
        input = """ int foo(){
                      b[];
                    }"""
        expect = "Error on line 2 col 24: ]";
        self.assertTrue(TestParser.checkParser(input,expect,253))
    def test_valid_array_operator(self):
        input = """ int foo(){
                      a=b[c]+24[true];
                    }"""
        expect = "successful";
        self.assertTrue(TestParser.checkParser(input,expect,254))
    def test_valid_array_operator_with_function(self):
        input = """ int foo(){
                      foo(3)[9];
                    }"""
        expect = "successful";
        self.assertTrue(TestParser.checkParser(input,expect,255))
    def test_many_assigment_statement(self):
        input = """ int foo(){
                      a=b=c=d;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,256))
    def test_many_assigment_statement_with_different_expression(self):
        input = """ int foo(){
                      foo(2)=arr[7]="string"=10=false=3.2e2;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,257))
    def test_mix_up_operator(self):
        input = """ int foo(){
                      foo(2)[3]=a||b&&(c-35.4e10)*9+10.0/4+true;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,258))
    def test_invalid_NOT_EQUAL_operator(self):
        input = """ int foo(){
                      a!=b!=c;
                    }"""
        expect = "Error on line 2 col 26: !=";
        self.assertTrue(TestParser.checkParser(input,expect,259))
    def test_valid_COMPARE_operator(self):
        input = """ int foo(){
                      (a==b)!=c;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,260))
    def test_valid_relation_operator(self):
        input = """ int foo(){
                      ((a>b)<d)>=(c<=d);
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,261))
    def test_valid_nested_array_operator(self):
        input = """ int foo() {
                        foo[3]= ((a(c,c*d+false,10.99e1)[3])[10[1]])[c+2];
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,262))
    def test_valid_boolean_expression_statement(self):
        input = """ int foo(){
                      ((a>b)<d)>=(c<=d);
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,263))
    def test_valid_intlit_expression_statement(self):
        input = """ int foo() {
                      39;
                      12.3e2;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,264))
    def test_valid_stringlit_expression_statement(self):
        input = """ int foo() {
                      "string";
                      "";
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,265))
    def test_valid_array_expression_statement(self):
        input = """ int foo() {
                      arr[3];
                      arr(2)[3];
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,266))
    def test_valid_funcall_expression_statement(self):
        input = """ int foo() {
                      foo();
                      foo(3);
                      foo("",true);
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,267))
    def test_valid_equal_expression_statement(self):
        input = """ int foo() {
                      c==d=f=g;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,268))
    def test_valid_EQUAL_and_ASIGN_expression_statement(self):
        input = """ int foo() {
                      c==d=f=g==h;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,269))
    def test_invalid_if_else_statement(self):
        input = """ int foo() {
                      if(1) 1;
                      else 2;
                      else 3;
                    }"""
        expect = """Error on line 4 col 22: else"""
        self.assertTrue(TestParser.checkParser(input,expect,270))
    def test_invalid_uncompleted_if_else_statement(self):
        input = """ int foo() {
                      if(1) 1;
                      else ;
                    }"""
        expect = """Error on line 3 col 27: ;"""
        self.assertTrue(TestParser.checkParser(input,expect,271))
    def test_invalid_if_no_else_statement(self):
        input = """ int foo() {
                      if(1) int a;
                    }"""
        expect = """Error on line 2 col 28: int"""
        self.assertTrue(TestParser.checkParser(input,expect,272))
    def test_invalid_SUB_operator(self):
        input = """ int foo() {
                      a--;
                    }"""
        expect = """Error on line 2 col 25: ;"""
        self.assertTrue(TestParser.checkParser(input,expect,273))
    def test_valid_unary_SUB_operator(self):
        input = """ int foo() {
                    ---a;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,274))
    def test_valid_unary_NOT_operator(self):
        input = """ int foo() {
                    !!!a;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,275))
    def test_invalid_NOT_operator(self):
        input = """ int foo() {
                      a!;
                    }"""
        expect = """Error on line 2 col 23: !"""
        self.assertTrue(TestParser.checkParser(input,expect,276))
    def test_invalid_ADD_operator(self):
        input = """ int foo() {
                      a++;
                    }"""
        expect = """Error on line 2 col 24: +"""
        self.assertTrue(TestParser.checkParser(input,expect,277))
    def test_valid_binary_SUB_operator(self):
        input = """ int foo() {
                      a----b;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,278))
    def test_invalid_for_loop(self):
        input = """ int foo() {
                      for(1;2;) 1;
                    }"""
        expect = """Error on line 2 col 30: )"""
        self.assertTrue(TestParser.checkParser(input,expect,279))
    def test_valid_for_loop(self):
        input = """ int foo() {
                      for(1;2;3) 1;
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,280))
    def test_invalid_name_function(self):
        input = """ int 20day() {} """
        expect = """Error on line 1 col 5: 20"""
        self.assertTrue(TestParser.checkParser(input,expect,281))
    def test_invalid_do_while_statement(self):
        input = """ int foo() {
                      do {}
                      while();
                    }"""
        expect = """Error on line 3 col 28: )"""
        self.assertTrue(TestParser.checkParser(input,expect,282))
    def test_valid_multiple_block_do_while_statement(self):
        input = """ int foo() {
                      do
                      {}{}{}
                      while(1);
                    }"""
        expect = """successful"""
        self.assertTrue(TestParser.checkParser(input,expect,283))
    def test_error_token(self):
        input = """ int func(){
                      float a;
                      a = .e2;
                    }"""
        expect = "."
        self.assertTrue(TestParser.checkParser(input,expect,284))
    def test_unclose_string_in_function(self):
        input = """ int func(){
                      print("string);
                    }"""
        expect = "string);"
        self.assertTrue(TestParser.checkParser(input,expect,285))
    def test_unclose_string_in_var_declar(self):
        input = """ int func(){
                      string a;
                      a = "string;
                    }"""
        expect = "string;"
        self.assertTrue(TestParser.checkParser(input,expect,286))
    def test_illegal_escape_string(self):
        input = """ float main (int a, float b) {
                      s = "This is a \p string";
                    }"""
        expect ="""This is a \p"""
        self.assertTrue(TestParser.checkParser(input,expect,287))
    def test_invalid_mising_SEMI_return_statement(self):
        input = """ int func(){
                      return a
                    }"""
        expect = "Error on line 3 col 20: }"
        self.assertTrue(TestParser.checkParser(input,expect,288))
    def test_invalid_many_expression_return_statement(self):
        input = """ int func(){
                      return a b;
                    }"""
        expect = "Error on line 2 col 31: b"
        self.assertTrue(TestParser.checkParser(input,expect,289))
    def test_complex_program(self):
        input =""" void putIn(int itemSize, int itemValue) {
                       int chart;   
                       int numItem;
                       int bagSize;
                       int sizeBag;
                       for (i = numItem; i > 0; i=i-1) {
                           if ((chart[i])[sizeBag] != (chart[i - 1])[sizeBag]) {
                               size=value = itemSize_at(i - 1);
                               addItem(size, value);
                               sizeBag = sizeBag-size;
                           }
                       }
                       print("Put these items in bag from set");
                       showItem();
                   }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,290))
    def test_complex_for_program(self):
        input = """ void main(){
                       for (i = 0; i < numItem + 1; i) {
                          (chart[i])[0] = 0;
                       }
                       for (i = 0; i < bagSize + 1; i) {
                         (chart[0])[i] = 0;
                       }
                       for (item = 1; item < numItem + 1; item) {
                           for (size = 0; size < bagSize + 1; size) {
                               S = itemSizeat(item - 1);
                               V = itemValueat(item - 1);
                               if (S <= size) {
                                   if (V + (chart[item - 1])[size - S] >(chart[item - 1])[size])
                                       (chart[item])[size] = V + chart[size - S];
                                   else
                                       chart[size] = chart[item - 1];
                               }
                               else
                                   chart[item] = chart[size];
                           }
                       }
                       return chart;
                   }"""
        expect ="successful"
        self.assertTrue(TestParser.checkParser(input,expect,291))
    def test_complex_if_program(self):
        input = """int zeroOne_myAttemp(){
                       for ( i = place; i < numItem; i) {
                         if (last_one || no_more_space(i == numItem - 1) || (size - itemSize == 0)) {
                           if (itemSize > size) {
                             if (value > max){
                               max = value;
                               return;
                             }
                             if (value + itemValue[at(i)] > max)
                               max = value + itemValue;
                             return;
                           }
                           if (size < itemSize)
                             continue;
                           for (j = 1; j < numItem - i + 1;j+1) {
                             findMax(size - itemSizeat(i),i + j,value + itemValue[at(i)]);
                           }
                         }
                       }
                       return max;
                   }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,292))
    def test_complex_program_with_one_function(self):
        input = """
                int fibonacci(int N){
                  if (N == 0) return  0;
                  if (N == 1) return 1;
                  else return fibonacci(N - 1) + fibonacci(N - 2);
                }
                int main(){
                  int N,i;
                  N=0;
                  for (i = 0; i < N; i)
                    cout(fibonacci(i), " ");
                    system("pause");
                }
                """
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input,expect,293))
    def test_bracket_error_1(self):
        input = """void main(){{(int a;)int c;}} """
        expect = "Error on line 1 col 14: int"
        self.assertTrue(TestParser.checkParser(input, expect, 294))
    def test_bracket_error_2(self):
        input = """void main(){[int c;{int d;}]} """
        expect = "Error on line 1 col 12: ["
        self.assertTrue(TestParser.checkParser(input, expect, 295))
    def test_bracket_2(self):
        input = """void main(){{{int a;}}} int sub(){{ break;} continue;}"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 296))
    def test_program_with_builtIn_function_putInt(self):
            input = """int main () {
	        putInt(4);
	        }"""
            expect = "successful"
            self.assertTrue(TestParser.checkParser(input, expect, 297))
    def test_program_with_builtIn_function_getFloat(self):
        input = """int main () {
	    getFloat();
	    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 298))
    def test_program_with_builtIn_function_putFloatLn(self):
        input = """int main () {
	    putFloatLn(add(5,8));
	    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 299))
    def test_program_with_builtIn_function_putString(self):
        input = """int main () {
	    putString("gg");
	    }"""
        expect = "successful"
        self.assertTrue(TestParser.checkParser(input, expect, 300))
